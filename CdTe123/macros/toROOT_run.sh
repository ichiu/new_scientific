./text2root ../data/J-PARC2023JanCdTe123/live_data_21MeV_yotsuya6-0226-1.mca
./text2root ../data/J-PARC2023JanCdTe123/live_data_23MeV_yotsuya6-0226-1.mca
./text2root ../data/J-PARC2023JanCdTe123/live_data_25MeV_yotsuya6-0226-1.mca
./text2root ../data/J-PARC2023JanCdTe123/live_data_25MeV_yotsuya6-0226-2.mca
./text2root ../data/J-PARC2023JanCdTe123/live_data_25MeV_yotsuya6-0226-3.mca
./text2root ../data/J-PARC2023JanCdTe123/live_data_30MeV_yotsuya6-0226-1.mca
python calib.py ../data/J-PARC2023JanCdTe123/live_data_21MeV_yotsuya6-0226-1.mca.root
python calib.py ../data/J-PARC2023JanCdTe123/live_data_23MeV_yotsuya6-0226-1.mca.root
python calib.py ../data/J-PARC2023JanCdTe123/live_data_25MeV_yotsuya6-0226-1.mca.root
python calib.py ../data/J-PARC2023JanCdTe123/live_data_25MeV_yotsuya6-0226-2.mca.root
python calib.py ../data/J-PARC2023JanCdTe123/live_data_25MeV_yotsuya6-0226-3.mca.root
python calib.py ../data/J-PARC2023JanCdTe123/live_data_30MeV_yotsuya6-0226-1.mca.root

#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_13Mev_June2022.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_15Mev_June2022.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_15Mev_sample_June2022_dsl15.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_15p5Mev_sample_June2022_dsl15.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16Mev_June2022.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16Mev_June2022_long.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16Mev_sample_June2022_dsl10.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16Mev_sample_June2022_dsl15.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16Mev_sample_June2022_test.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16Mev_sample_June2022_test2.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16p5Mev_imagingexp_June2022_dsl3_longrun.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16p5Mev_imagingexp_June2022_dsl5.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16p5Mev_sample_June2022_dsl15.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16p5Mev_sampleaddcolli_June2022_dsl15.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_Co57_June2022.mca
#./text2root ../data/J-PARC2022JuneCdTe123/live_data_JPARC_Eu152_June2022.mca
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_13Mev_June2022.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_15Mev_June2022.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_15Mev_sample_June2022_dsl15.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_15p5Mev_sample_June2022_dsl15.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16Mev_June2022.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16Mev_June2022_long.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16Mev_sample_June2022_dsl10.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16Mev_sample_June2022_dsl15.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16Mev_sample_June2022_test.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16Mev_sample_June2022_test2.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16p5Mev_imagingexp_June2022_dsl3_longrun.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16p5Mev_imagingexp_June2022_dsl5.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16p5Mev_sample_June2022_dsl15.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_16p5Mev_sampleaddcolli_June2022_dsl15.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_Co57_June2022.mca.root
#python calib.py ../data/J-PARC2022JuneCdTe123/live_data_JPARC_Eu152_June2022.mca.root
