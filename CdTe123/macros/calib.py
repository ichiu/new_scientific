#!/usr/bin/env python    
#-*- coding:utf-8 -*-   
"""
This module is for calibration script of CdTe123
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu@chem.sci.osaka-u.ac.jp"
__created__   = "2022-06-12"
__copyright__ = "Copyright 2022 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

# modules
import sys,os,random,math,ROOT,argparse,time,glob
from ROOT import TFile, TTree, gPad, TGraphAsymmErrors, TSpline3, gStyle, gErrorIgnoreLevel, gROOT, TSpectrum
from random import gauss
from array import array
ROOT.gROOT.SetBatch(1)
ROOT.gErrorIgnoreLevel = ROOT.kFatal

def cali():
    # === define ===
    #fint_co = ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/CdTe123/data/J-PARC2022JuneCdTe123/live_data_JPARC_Co57_June2022.mca.root","read")
    #fint_eu = ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/CdTe123/data/J-PARC2022JuneCdTe123/live_data_JPARC_Eu152_June2022.mca.root","read")
    #h_co = fint_co.Get("ADC")
    #h_eu = fint_eu.Get("ADC")
    #fit_range_co = [[1580,1610],[1770,1795]]#122.0614, 136.4743
    #fit_range_eu = [[520,545],[590,615],[1580,1610]]#39.82, 45.354, 121.7817
    #my_energy=[122.0614, 136.4743, 39.82, 45.354, 121.7817]
    #my_energy.sort()

    # === image ===
    fint_co = ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/CdTe123/data/J-PARC2023JanCdTe123/sum_cali.root","read")
    fint_eu = ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/CdTe123/data/J-PARC2023JanCdTe123/sum_cali.root","read")
    h_co = fint_co.Get("ADC")
    h_eu = fint_eu.Get("ADC")
    fit_range_co = [[1820,1860]]#O
    fit_range_eu = [[900,930],[1040,1070]]#Si, Al
    my_energy=[134.35, 65.87, 76.42]
    my_energy.sort()

    # === find peak ===
    line_co,line_eu,my_peaks=[],[],[] # channel == ibin
    for _r in fit_range_co:
       _sumw, _w, _maxbin_co = 0.,0.,-1.
       for ibin in range(_r[0], _r[1]+1):
          _sumw+=h_co.GetBinCenter(ibin)*h_co.GetBinContent(ibin)
          _w+=h_co.GetBinContent(ibin)
          if h_co.GetBinContent(ibin) > _maxbin_co: _maxbin_co = h_co.GetBinContent(ibin)
       if _w == 0: print("ERROR in _w !")
       line_co.append(_sumw/_w)
       my_peaks.append(_sumw/_w)

    _maxbin_eu=-1 
    for _r in fit_range_eu:
       _sumw, _w, _maxbin_eu = 0.,0.,-1.
       for ibin in range(_r[0], _r[1]+1):
          _sumw+=h_eu.GetBinCenter(ibin)*h_eu.GetBinContent(ibin)
          _w+=h_eu.GetBinContent(ibin)
          if h_eu.GetBinContent(ibin) > _maxbin_eu: _maxbin_eu = h_eu.GetBinContent(ibin)
       if _w == 0: print("ERROR in _w !")
       line_eu.append(_sumw/_w)
       my_peaks.append(_sumw/_w)
    my_peaks.sort()

    # === make line ===
    gr = ROOT.TGraph()
    _s = ROOT.TSpline3()
    gr.SetName("graph_calib")
    gr.SetPoint(0, 0, 0)
    for i in range(len(my_energy)):
       gr.SetPoint(i+1, my_peaks[i], my_energy[i])
       f_x, f_y, slope = 0,0,0
       if i == len(my_energy)-1:
          source_Epre=my_energy[i-1]
          fit_adcpre=my_peaks[i-1]
          source_E=my_energy[i]
          fit_adc=my_peaks[i]
          slope = (float(source_E) - float(source_Epre))/(fit_adc - fit_adcpre)
          f_x, f_y = fit_adc, float(source_E)
       gr.SetPoint(len(my_energy) + 1, 8192, (8912-f_x)*slope + f_y)
    _s = ROOT.TSpline3("spline_calib", gr)
    
    cv=ROOT.TCanvas("cv","cv",0,0,2400,800) 
    cv.Divide(3,1)
    cv.cd(1)
    gr.SetLineColor(1)
    gr.SetMarkerColor(1)
    gr.SetMarkerStyle(47)
    _s.SetLineColor(2)
    gr.Draw()
    cv.cd(2)
    h_co.Draw("hist")
    line_co,line_eu=[],[]
    for i in line_co: 
       l = ROOT.TLine(i,0,i,3000)
       l.SetLineColor(2)
       l.SetDirectory(0)
       line_co.append(l)
    for l in line_co:
       l.Draw("same")
    cv.cd(3)
    h_eu.Draw("hist")
    for i in line_eu: 
       l = ROOT.TLine(i,0,i,2500)
       l.SetLineColor(2)
       l.Draw("same")
       l.SetDirectory(0)
       line_eu.append(l)
    for l in line_eu:
       l.Draw("same")
    cv.Print("/Users/chiu.i-huan/Desktop/cv_cdte123_cali.pdf")
    return gr, _s

def main(args, gr):
    if args.inputfile == "None": print("key input file ! "); return 0
    fint = ROOT.TFile(args.inputfile, "update")
    tint = fint.Get("tree")
    hist=ROOT.TH1D("hist_e","hist_e",2000,0,500)
    _energy = array('d',[0])
    _treeadd = tint.Branch("energy", _energy, 'energy/D')
    for i in range(tint.GetEntries()):
       tint.GetEntry(i)
       pha=tint.channel+(random.random()-0.5)
       hist.Fill(gr.Eval(pha))
       _energy=gr.Eval(pha)
       _treeadd.Fill()
    hist.Write()
    gr.SetTitle(";ADC;Energy")
    gr.SetName("calibration")
    gr.Write()
    fint.Write("",TFile.kOverwrite)
    fint.Close()

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("inputfile", type=str, default="None", help="Input File Name")
    args = parser.parse_args()
    gr, _s = cali() 
    main( args , gr)
