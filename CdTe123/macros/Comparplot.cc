#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "THStack.h"
#include "TLegend.h"
//#include "Headers.h"
#include "TChain.h"
#include "TFile.h"
#include "TH2.h"
#include "TH2.h"
#include "TH3.h"
#include "THn.h"
#include "TCanvas.h"
#include "TLatex.h"

#include "TH1F.h"
#include "TDirectory.h"
#include "TDirectoryFile.h"
#include "TArrow.h"
#include "TRef.h"
#include "TApplication.h"
#include "TError.h"
#include "TMath.h"
#include "TAxis.h"
#include "TStyle.h"
#include "TLine.h"

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "ATLASStyle/AtlasStyle.C"
#include "ATLASStyle/AtlasLabels.C"
#include "ATLASStyle/AtlasUtils.C"


using namespace std;

void Comparplot(){

   #ifdef __CINT__
     gROOT->LoadMacro("AtlasLabels.C");
     gROOT->LoadMacro("AtlasUtils.C");
   #endif
   SetAtlasStyle();


  char name[50] = "";
  const double mz=91;
  const double rg=13;

  double r1=70;
  double r2=105;
  double scale=0;
  double rightmax=0;

//  TFile * outputTfile = new TFile ("histoutput.root","RECREATE");
  TCanvas *cs = new TCanvas("cs","cs",0,0,3200,3200);

  TPad *pad1 = new TPad("pad1","pad1",0,0.3,1,1);
  TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.3);
  TH1F* deno[70];
  TH1F* deno2[70];
  TH1 *F1_TL[70];
  TH1 *F2_TL[70];

//Load file
// TFile *f1=new TFile("../data/J-PARC2021Apri/live_data_BGD_27MeV_95ks.root","READ");//recon
// TFile *f2=new TFile("../data/J-PARC2021Apri/live_data0406_STD_27MeV_78ks.mca.root","READ");//recon
// double f1_scale=1/95.;
// double f2_scale=1/78.;
 TFile *f1=new TFile("/Users/chiu.i-huan/Desktop/new_scientific/CdTe123/data/J-PARC2023JanCdTe123/live_data_21MeV_yotsuya6-0226-1.mca.root","READ");//21 MeV
 TFile *f2=new TFile("/Users/chiu.i-huan/Desktop/new_scientific/CdTe123/data/J-PARC2023JanCdTe123/live_data_23MeV_yotsuya6-0226-1.mca.root","READ");//23 MeV
 TFile *f3=new TFile("/Users/chiu.i-huan/Desktop/new_scientific/CdTe123/data/J-PARC2023JanCdTe123/live_data_25MeV_sum.mca.root","READ");//25 MeV
 TFile *f4=new TFile("/Users/chiu.i-huan/Desktop/new_scientific/CdTe123/data/J-PARC2023JanCdTe123/live_data_30MeV_yotsuya6-0226-1.mca.root","READ");//30 MeV
 int nbin = 130*5; //for 20~150 keV
 double f1_scale=1/350.979441;
 double f2_scale=1/115.320942;
 double f3_scale=1/(17651.214970+37927.837787+3273.649514);
 double f4_scale=1/17159.580136;
 bool do_scale=true;
 TH1D* h1 = (TH1D*)f1->Get("hist_e");
 TH1D* h2 = (TH1D*)f2->Get("hist_e");
 TH1D* h3 = (TH1D*)f3->Get("hist_e");
 TH1D* h4 = (TH1D*)f4->Get("hist_e");
 h1->SetStats(0);
 h2->SetStats(0);
 h3->SetStats(0);
 h4->SetStats(0);

 h3->SetLineWidth(1);
 h3->GetXaxis()->SetRangeUser(20,150);
 h3->SetMaximum(h3->GetMaximum()*1.3);
 h3->SetTitle(";Energy [keV]; Counts [/0.2 keV/s]");
 
 h1->Scale(f1_scale);
 h2->Scale(f2_scale);
 h3->Scale(f3_scale);
 h4->Scale(f4_scale);

 h1->SetLineColorAlpha(1,0.75);
 h2->SetLineColorAlpha(3,0.75);
 h3->SetLineColorAlpha(2,0.9);
 h4->SetLineColorAlpha(4,0.75);

 TLegend* leg = new TLegend(0.8,0.8,0.88,0.9);
 leg->SetFillColorAlpha(0,0);
 leg->SetLineColor(0);
 leg->SetBorderSize( 0);
// leg->AddEntry(h1, "21 MeV"  , "L");
// leg->AddEntry(h2, "23 MeV"  , "L");
 leg->AddEntry(h3, "25 MeV"  , "L");
 leg->AddEntry(h4, "30 MeV"  , "L");
 leg->SetTextSize(0.038);

// h1->Draw("hist");
// h2->Draw("hist same");
 h3->Draw("hist");
 h4->Draw("hist same");
 leg->Draw("same");
 cs->Print("/Users/chiu.i-huan/Desktop/temp_output/cv_image_compar_jparc2023.pdf","");
  


//  pad1->SetBottomMargin(0);
//  pad2->SetTopMargin(0.01);
//  pad2->SetBottomMargin(0.4);
//  pad1->Draw();
//  pad2->Draw();
//
//
//  pad1->cd();
//  pad1->SetLogy();
//  h1->Draw();
//  h2->Draw("same");



//  TLatex *text2;
//   text2 = new TLatex(5.570061,23.08044,"^{152}Eu,100s");
//   text2->SetNDC();
//   text2->SetTextAlign(13);
//   text2->SetX(0.384);
//   text2->SetY(0.88);
//   text2->SetTextFont(42);
//   text2->SetTextSizePixels(24);
//   text2->Draw();
//
//
//   pad2->cd();
//
//   pad2->SetLogy();
//  h3 = (TH1D*)h2->Clone("h3");
//
//  h3->Divide(h1);
//  h3->SetStats(0);      // No statistics on lower plot
//  h3->SetLineWidth(2);
//  h3->SetLineColor(1);
//  h3->SetMinimum(0.01);  // Define Y ..
//  h3->SetMaximum(70); // .. range
//  h3->SetTitle("");
//
//   h3->GetYaxis()->SetTitle("Ratio (3CM/5CM)");
//   h3->GetYaxis()->SetNdivisions(505);
//   h3->GetYaxis()->SetTitleSize(25);
//   h3->GetYaxis()->SetTitleFont(43);
//   h3->GetYaxis()->SetTitleOffset(1.55);
//   h3->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
//   h3->GetYaxis()->SetLabelSize(20);
//
//   h3->GetXaxis()->SetTitle("Ge Detector Channel");
//   h3->GetXaxis()->SetTitleSize(35);
//   h3->GetXaxis()->SetTitleFont(43);
//   h3->GetXaxis()->SetTitleOffset(4.);
//   h3->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
//   h3->GetXaxis()->SetLabelSize(30);
//
//   h3->Draw("H");
//
//   double low =  h3->GetXaxis()->GetXmin();
//   double high = h3->GetXaxis()->GetXmax();
//   TLine *line = new TLine(low,1,high,1);
//   line->SetLineColorAlpha(kRed, 0.3);
//   line->SetLineWidth(2);
//   line->Draw("same");

   if(do_scale){
   cs->SaveAs("/Users/chiu.i-huan/Desktop/comparisonplots_scale.pdf");
   }else{
   cs->SaveAs("comparisonplots.pdf");
   cs->SaveAs("/Users/chiu.i-huan/Desktop/comparisonplots.pdf");
   }
}

