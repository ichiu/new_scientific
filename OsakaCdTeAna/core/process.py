import sys
sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/utils/')
from array import array
import ROOT
import os
import time
from time import localtime, asctime
from multiprocessing import Pool, cpu_count
from core.tran import tran_process
from logger import log, supports_color
from slimming import DisableBranch
from helpers import ProgressBar, GetTChain
from cuts import PreEventSelection, findx2yshift, findadccut
from printInfo import checkTree

class Processor():
      
      def __init__(self,ifilelist=None,ofile=None,addname=None,ncores=None,nevents=None,database=None): 
          # config
          self.ncores = ncores
          self.nevents = nevents
          self.ifilelist = ifilelist
          self.ofile = ofile
          self.addname = addname
          self.database = database
          
          # members
          self.ifilename = None
          self.tree = None
          self.ifile = None
          self.T = None
          self.skimmingtree = None
          self.drawables = dict()
          self.Nevents = None
          self.fout = None
          self.tout = None

      def register(self,ifile, drawables):
          """
          :param drawables: single or list of ROOT drawables or plots (collection of drawables)
          """
          if not isinstance(drawables, list):
              drawables = [drawables]
          self.drawables[ifile] = drawables

      def mainprocess(self):
          log().info("Starting Job: %s"%(asctime(localtime())))
          neventsum, selector_job_list = self.__get_selector__()
          self.__process__(neventsum, selector_job_list)
          self.__outputs__()

      def __get_selector__(self):
          log().info("Preparing jobs...")
          selectorjob_list = list()         
          sele,nfile = 0,0
          self.tout = ROOT.TTree('tree','tree') 
          self.tout.SetDirectory(0)
          self.tout.SetMaxTreeSize(100000000000)
          for self.ifilename in self.ifilelist:
             nfile+=1
             self.ifile = ROOT.TFile(self.ifilename)
             self.tree = self.ifile.Get("eventtree")  

             self.tree = DisableBranch(self.tree)
             if self.nevents:   self.Nevents = min(self.nevents, self.tree.GetEntries())
             else: self.Nevents = self.tree.GetEntries()
             self.skimmingtree, _cutname = PreEventSelection(self.ifilename, self.tree, self.Nevents) # skimmingtree is dic(#, ROOT.TEventList)
             if self.Nevents > 50000000: self.Nevents = 50000000 # TODO setlimit

             sele += self.skimmingtree.GetN()     
             printname = self.ifilename.split("/")[-1]
             log().info("Total passed events : %d / %d (%s, %d/%d)"%(self.skimmingtree.GetN(),self.Nevents,printname,nfile,len(self.ifilelist)))
             self.T = tran_process(tree=self.tree, event_list=self.skimmingtree,database=self.database,tout = self.tout)        
             self.register(self.ifilename, self.T.drawables) 

          selectorjob_list = [SelectorCfg(run_id=i, tran=self.T) for i in range(sele)]
          return sele, selectorjob_list 

      def __process__(self,neventsum, selectorjob_list):
          if not self.ncores:   ncores = min(2, cpu_count())
          else: ncores = min(self.ncores, cpu_count())

          log().info("Lighting up %d cores!!!"%(ncores))
          ti = time.time()
          prog = ProgressBar(ntotal=neventsum,text="Processing ntuple",init_t=ti)

          # single core
          nevproc=0
          for s in selectorjob_list:
             __process_selector__(s)
             nevproc+=1
             if prog: prog.update(nevproc)
          if prog: prog.finalize()

      def __outputs__(self):
          log().info('Printing output...')
          for ifile, drawobjects in self.drawables.items():
             filename=ifile.split("/")[-1]
             subProc = self.ofile+filename.split(".root")[0]+"_"+self.addname+".root"
             self.fout = ROOT.TFile( subProc, 'recreate' )
             __print_output__(self.fout, self.tout ,drawobjects)
          log().info('Finished Ntuples!')
          #checkTree(self.T.tout,self.Nevents)

def __process_selector__(sgel):
    sgel.tran.tran_adc2e(sgel.run_id)
    return sgel
       
def __print_output__(ofile, _tree, Drawables):
    ofile.cd()
    _tree.Write()
    for idraw in Drawables:
       idraw.Write()
    ofile.Close()

class SelectorCfg(object):
      def __init__(self,tree=None, run_id=None, tran=None):
          self.tree = tree          
          self.run_id = run_id
          self.tran = tran


