#!/usr/bin/env python    
#-*- coding:utf-8 -*-   
"""
This module provides the transformation from adc to energy.
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu.irs@osaka-u.ac.jp"
__created__   = "2022-06-09"
__copyright__ = "Copyright 2022 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

# modules
import sys,os,random,math,ROOT
sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/core/')
from ROOT import TFile, TTree, TCut, TChain, TSelector
from ROOT import gROOT, addressof
ROOT.gROOT.SetBatch(1)
import argparse
import math
from multiprocessing import Pool, cpu_count
import time
from time import localtime, asctime
from array import array
from random import gauss
import numpy as np
from utils.printInfo import checkTree
from utils.slimming import DisableBranch
from utils.countHit import Level1Hit, Level2Hit, matchLv2
from utils.cuts import PreEventSelection, findx2yshift, findadccut
from utils.helpers import ProgressBar

import enums

gROOT.ProcessLine(
"struct RootStruct {\
   unsigned int     trigger;\
   unsigned int     livetime;\
   unsigned int     unixtime;\
   unsigned int     nhit;\
   unsigned int     nsignalx_lv1;\
   unsigned int     nsignaly_lv1;\
   unsigned int     nsignalx_lv2;\
   unsigned int     nsignaly_lv2;\
   Double_t     energy[128];\
   Double_t  energy_pt[128];\
   Double_t  energy_al[128];\
   Double_t     adc_pt[128];\
   Double_t     adc_al[128];\
   Double_t     axis_x[128];\
   Double_t     axis_y[128];\
   Double_t       type[128];\
   Double_t   E_pt_lv1[128];\
   Double_t   E_al_lv1[128];\
   Double_t  Poi_x_lv1[128];\
   Double_t  Poi_y_lv1[128];\
   Double_t     E_pt_lv2[128];\
   Double_t     E_al_lv2[128];\
   Double_t        Poi_x[128];\
   Double_t        Poi_y[128];\
   Double_t    Poi_x_lv2[128];\
   Double_t    Poi_y_lv2[128];\
   Int_t n_strips_pt_lv2[128];\
   Int_t n_strips_al_lv2[128];\
   Int_t  stripid_pt_lv1[128];\
   Int_t  stripid_al_lv1[128];\
   Int_t  stripid_pt_lv2[128];\
   Int_t  stripid_al_lv2[128];\
};"
); 

from ROOT import RootStruct
struct = RootStruct()

def getlineEnergy(energyFile,channel): 
    return energyFile.Get('spline_%s'%(channel))

    # === minami cali. data ===
    if channel < 64: 
       new_ch = channel - 0*64;
       if new_ch < 10: cal_name = "gain_func_r000_000_" + "00" + str(new_ch) 
       else: cal_name = "gain_func_r000_000_" + "0" + str(new_ch) 
    elif channel < 128:  
       new_ch = channel - 1*64;
       if new_ch < 10: cal_name = "gain_func_r000_001_" + "00" + str(new_ch) 
       else: cal_name = "gain_func_r000_001_" + "0" + str(new_ch) 
    elif channel < 192:  
       new_ch = channel - 2*64;
       if new_ch < 10: cal_name = "gain_func_r000_002_" + "00" + str(new_ch) 
       else: cal_name = "gain_func_r000_002_" + "0" + str(new_ch) 
    else : 
       new_ch = channel - 3*64;
       if new_ch < 10: cal_name = "gain_func_r000_003_" + "00" + str(new_ch) 
       else: cal_name = "gain_func_r000_003_" + "0" + str(new_ch) 
    return energyFile.Get(cal_name)
 
def Getdatabase(database):
    eline, pha_cut, ep_map = list(), dict(), dict()
    db = ROOT.TFile(database, 'read')
    # *** get calibration line ***
    for ch in range(0, 256): 
       eline.append(getlineEnergy(db,ch))
    # *** get pha cut ***
    for ichips in range(4):
       hname = "h_pha_thre_chip"+str(ichips+1)
       _h=db.Get(hname)
       pha_cut.update({ichips:_h})
       pha_cut[ichips].SetDirectory(0)
    # *** get epi1 epi2 map ***
    n_init=-30; n_end=5; bin_range=0.2; nplots=int((n_end-(n_init))/bin_range); n_strip_limit=2 # default (check tools/mk_rep.py)
    for inx in range(n_strip_limit):
       for iny in range(n_strip_limit):
          for i in range(nplots):
             name = "Graph_nxny{0}{1}_map{2}".format(inx+1,iny+1,i)
             index=i+iny*nplots+inx*n_strip_limit*nplots
             ep_map.update({index:db.Get(name)})
    db.Close()
    return eline, pha_cut, ep_map
    
def makentuple(signal, hitx_lv2, hity_lv2, hitx_lv1, hity_lv1):
    struct.nsignalx_lv1 = len(hitx_lv1)
    struct.nsignaly_lv1 = len(hity_lv1)
    struct.nsignalx_lv2 = len(hitx_lv2)
    struct.nsignaly_lv2 = len(hity_lv2)
    struct.nhit = len(signal)
    for n in range(1,len(signal)+1):          
       struct.adc_pt[n-1]   = signal[n].adc_pt
       struct.adc_al[n-1]   = signal[n].adc_al
       struct.energy[n-1]   = signal[n].energy
       struct.energy_pt[n-1]= signal[n].energy_pt
       struct.energy_al[n-1]= signal[n].energy_al
       struct.axis_x[n-1]   = signal[n].x
       struct.axis_y[n-1]   = signal[n].y
       struct.type[n-1]     = signal[n].type
    for n in range(1,len(hitx_lv1)+1):          
       struct.E_pt_lv1[n-1]        = hitx_lv1[n].energy
       struct.stripid_pt_lv1[n-1]  = hitx_lv1[n].stripid
       struct.Poi_x_lv1[n-1]      = hitx_lv1[n].position
    for n in range(1,len(hity_lv1)+1):          
       struct.E_al_lv1[n-1]        = hity_lv1[n].energy
       struct.stripid_al_lv1[n-1]  = hity_lv1[n].stripid
       struct.Poi_y_lv1[n-1]      = hity_lv1[n].position
    for n in range(1,len(hitx_lv2)+1):          
       struct.E_pt_lv2[n-1]        = hitx_lv2[n].energy
       struct.n_strips_pt_lv2[n-1] = int(hitx_lv2[n].nstrips)
       struct.stripid_pt_lv2[n-1]  = hitx_lv2[n].stripid
       struct.Poi_x_lv2[n-1]      = hitx_lv2[n].position
    for n in range(1,len(hity_lv2)+1):          
       struct.E_al_lv2[n-1]        = hity_lv2[n].energy
       struct.n_strips_al_lv2[n-1] = int(hity_lv2[n].nstrips)
       struct.stripid_al_lv2[n-1]  = hity_lv2[n].stripid
       struct.Poi_y_lv2[n-1]      = hity_lv2[n].position

class tran_process():
      def __init__(self,
                   tree=None,
                   event_list=None,
                   database=None,
                   tout=None,
                   ):
          # ==================================
          # config 
          # ==================================
          self.tree       = tree
          self.event_list = event_list
          self.database   = database
          self.tout       = tout 
           
          # ==================================
          # members
          # ==================================
          self.hist_list = []
          self.tree_list = []
          self.h2_lv1 = ROOT.TH2D("hist_nhits_level1","level1 hit channel",20,0,20,20,0,20)
          self.h2_lv1.SetDirectory(0)
          self.h2_lv1.SetTitle("# of hits on each side;Pt-side;Al-side")
          self.h2_lv2 = ROOT.TH2D("hist_nhits_level2","level2 hit channel",20,0,20,20,0,20)
          self.h2_lv2.SetDirectory(0)
          self.h2_lv2.SetTitle("# of hits on each side;Pt-side;Al-side")
          self.h1_lv2_x_nstrips = ROOT.TH1D("ptside_n","pt-side nstrips of level2hit",10,0,10)
          self.h1_lv2_x_nstrips.SetDirectory(0)
          self.h1_lv2_y_nstrips = ROOT.TH1D("alside_n","al-side nstrips of level2hit",10,0,10)
          self.h1_lv2_y_nstrips.SetDirectory(0)

          # ==================================
          # tree
          # ==================================
          #  === Event content ===
          self.tout.Branch( 'trigger', addressof( struct, 'trigger' ),  'trigger/i' )
          self.tout.Branch( 'livetime', addressof( struct, 'livetime' ),  'livetime/i' )
          self.tout.Branch( 'unixtime', addressof( struct, 'unixtime' ),  'unixtime/i' )
          self.tout.Branch( 'nhit', addressof( struct, 'nhit' ),  'nhit/i' )
          self.tout.Branch( 'nsignalx_lv1', addressof( struct, 'nsignalx_lv1' ),  'nsignalx_lv1/i' )
          self.tout.Branch( 'nsignaly_lv1', addressof( struct, 'nsignaly_lv1' ),  'nsignaly_lv1/i' )
          self.tout.Branch( 'nsignalx_lv2', addressof( struct, 'nsignalx_lv2' ),  'nsignalx_lv2/i' )
          self.tout.Branch( 'nsignaly_lv2', addressof( struct, 'nsignaly_lv2' ),  'nsignaly_lv2/i' )

          #  === Lv1: calibration === 
          self.tout.Branch( 'E_pt_lv1', addressof( struct, 'E_pt_lv1' ),    'E_pt_lv1[nsignalx_lv1]/D' )
          self.tout.Branch( 'E_al_lv1', addressof( struct, 'E_al_lv1' ),    'E_al_lv1[nsignaly_lv1]/D' )
          #self.tout.Branch( 'Poi_x_lv1', addressof( struct, 'Poi_x_lv1' ),'Poi_x_lv1[nsignalx_lv1]/D' )
          #self.tout.Branch( 'Poi_y_lv1', addressof( struct, 'Poi_y_lv1' ),'Poi_y_lv1[nsignaly_lv1]/D' )
          self.tout.Branch( 'stripid_pt_lv1', addressof( struct, 'stripid_pt_lv1' ),'stripid_pt_lv1[nsignalx_lv1]/I' )
          self.tout.Branch( 'stripid_al_lv1', addressof( struct, 'stripid_al_lv1' ),'stripid_al_lv1[nsignaly_lv1]/I' )

          #  === Lv2: merged hit ===
          self.tout.Branch( 'E_pt_lv2', addressof( struct, 'E_pt_lv2' ),    'E_pt_lv2[nsignalx_lv2]/D' )
          self.tout.Branch( 'E_al_lv2', addressof( struct, 'E_al_lv2' ),    'E_al_lv2[nsignaly_lv2]/D' )
          self.tout.Branch( 'Poi_x_lv2', addressof( struct, 'Poi_x_lv2' ),'Poi_x_lv2[nsignalx_lv2]/D' )
          self.tout.Branch( 'Poi_y_lv2', addressof( struct, 'Poi_y_lv2' ),'Poi_y_lv2[nsignaly_lv2]/D' )
          self.tout.Branch( 'stripid_pt_lv2', addressof( struct, 'stripid_pt_lv2' ),'stripid_pt_lv2[nsignalx_lv2]/I' )
          self.tout.Branch( 'stripid_al_lv2', addressof( struct, 'stripid_al_lv2' ),'stripid_al_lv2[nsignaly_lv2]/I' )
          self.tout.Branch( 'n_strips_pt_lv2', addressof( struct, 'n_strips_pt_lv2' ),    'n_strips_pt_lv2[nsignalx_lv2]/I' )
          self.tout.Branch( 'n_strips_al_lv2', addressof( struct, 'n_strips_al_lv2' ),    'n_strips_al_lv2[nsignaly_lv2]/I' )

          #  === Lv3: match ===
          self.tout.Branch( 'energy', addressof( struct, 'energy' ),  'energy[nhit]/D' )
          self.tout.Branch( 'energy_pt', addressof( struct, 'energy_pt' ),  'energy_pt[nhit]/D' )
          self.tout.Branch( 'energy_al', addressof( struct, 'energy_al' ),  'energy_al[nhit]/D' )
          self.tout.Branch( 'x',        addressof( struct, 'axis_x' ),    'x[nhit]/D' )
          self.tout.Branch( 'y',        addressof( struct, 'axis_y' ),    'y[nhit]/D' )
          #self.tout.Branch( 'adc_pt',    addressof( struct, 'adc_pt' ),     'adc_pt[nhit]/D' )
          #self.tout.Branch( 'adc_al',    addressof( struct, 'adc_al' ),     'adc_al[nhit]/D' )
          #self.tout.Branch( 'type',        addressof( struct, 'type' ),   'type[nhit]/D' )


          # ==================================
          # auxfiles
          # ==================================
          self.eline, self.phacut, self.response = Getdatabase(self.database)

          # ==================================
          # Draw hist. & tree
          # ==================================
          #self.hist_list.append(self.h2_lv1)
          #self.hist_list.append(self.h2_lv2)
          #self.hist_list.append(self.h1_lv2_x_nstrips)
          #self.hist_list.append(self.h1_lv2_y_nstrips)
          #self.tree_list.append(self.tout)

          self.drawables = self.hist_list + self.tree_list

      def tran_adc2e(self,ie):
           
          # === get tree ===
          self.tree.GetEntry(self.event_list.GetEntry(ie))

          # === ana. ===
          hitx_lv1, hity_lv1 = Level1Hit(self.tree, self.eline, self.phacut, self.database)# position and energy calibration
          hitx_lv2, hity_lv2 = Level2Hit(hitx_lv1, hity_lv1) # merge adjacent signal
          hit_signal = matchLv2(hitx_lv2, hity_lv2, self.response) # match Pt & Al sides
          if len(hitx_lv1) == 0 or len(hity_lv1) == 0: return 0

          # === hist. for quick look ===
          #self.h2_lv1.Fill(len(hitx_lv1),len(hity_lv1))
          #self.h2_lv2.Fill(len(hitx_lv2),len(hity_lv2))
          #for _mx in hitx_lv2 : self.h1_lv2_x_nstrips.Fill(hitx_lv2[_mx].nstrips)
          #for _my in hity_lv2 : self.h1_lv2_y_nstrips.Fill(hity_lv2[_my].nstrips)

          # === parameters to ntuple ===
          struct.trigger  = self.tree.time_from_exttrigger
          struct.livetime  = self.tree.livetime
          struct.unixtime = self.tree.unixtime
          makentuple(hit_signal,hitx_lv2, hity_lv2,hitx_lv1, hity_lv1)
          self.tout.Fill()

