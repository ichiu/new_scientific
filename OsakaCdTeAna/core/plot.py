#!/usr/bin/env python    
#-*- coding:utf-8 -*-   
"""
This module provides the plots
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu.irs@osaka-u.ac.jp"
__created__   = "2022-06-01"
__copyright__ = "Copyright 2022 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

# modules
import sys,os,random,math,ROOT
from ROOT import TFile, TTree, gROOT, gStyle, TCut, gPad, gDirectory
ROOT.gROOT.SetBatch(1)
import argparse
sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/utils/')
#gROOT.ProcessLine("gErrorIgnoreLevel = kPrint, kInfo, kWarning, kError, kBreak, kSysError, kFatal;")
ROOT.gErrorIgnoreLevel = ROOT.kWarning

from logger import log, supports_color
from helpers import GetTChain, createRatioCanvas
from color import SetMyPalette

__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')

# === set cut ===
triggercut="1"
energycut="1"
#triggercut="(trigger > 0 && trigger < 2000)"
#energycut="(E_n_lv2 > 72 && E_p_lv2 < 78)"

class makecut():
      def __init__(self,basecut):
          self.base = basecut
      def add(self,cutname):
          self.base += "&&" + cutname
      def get(self):
          return TCut(self.base)

def image(tree, icut, position):
# =============== make image =============
    #tree.Draw("y:x >> h2(64,-16,16,64,-16,16)",icut,"colz")
    tree.Draw("Poi_y_lv2:Poi_x_lv2 >> h2(128,-16,16,128,-16,16)",icut,"colz")
    #TODO
    #tree.Draw("Poi_y_lv2:Poi_x_lv2 >> h2(128,0,128,128,0,128)",icut,"colz") # no axis
    h2 = gDirectory.Get("h2")
    h2.GetYaxis().SetTitle("Al side [mm]")
    h2.GetXaxis().SetTitle("Pt side [mm]")
    h2.SetDirectory(0)
#    gPad.SetLogz(1)
    return h2

def spectrum(tree, scut):
    #nbin,xR_up=700,350
    nbin,xR_up=200,200
    h1_s = ROOT.TH1F("Spectrum_allside","Spectrum allside", nbin,0,xR_up)
    h1_p = ROOT.TH1F("Spectrum_pside","Spectrum pside", nbin,0,xR_up)
    h1_n = ROOT.TH1F("Spectrum_nside","Spectrum nside", nbin,0,xR_up)

    tree.Draw("energy >> h1_s({},10,{})".format(nbin,xR_up),scut,"")
    #tree.Draw("energy_pt >> h1_p({},0,{})".format(nbin,xR_up),scut,"same")
    #tree.Draw("energy_al >> h1_n({},0,{})".format(nbin,xR_up),scut,"same")
    tree.Draw("E_pt_lv2 >> h1_p({},10,{})".format(nbin,xR_up),scut,"same")
    tree.Draw("E_al_lv2 >> h1_n({},10,{})".format(nbin,xR_up),scut,"same")
    h1_s=gDirectory.Get("h1_s")
    h1_p=gDirectory.Get("h1_p")
    h1_n=gDirectory.Get("h1_n")
    h1_s.SetStats(0)
    h1_p.SetStats(0)
    h1_n.SetStats(0)
    h1_s.GetXaxis().SetTitle("energy [keV]")
    h1_s.GetYaxis().SetTitle("Counts")
    h1_p.GetXaxis().SetTitle("energy [keV]")
    h1_p.GetYaxis().SetTitle("Counts")
    h1_n.GetXaxis().SetTitle("energy [keV]")
    h1_n.GetYaxis().SetTitle("Counts")

    return h1_s, h1_p, h1_n

class Baseplot():

      def __init__(self,infile=None,outname=None,initUT=None): 
          self.infile = infile
          self.outname = outname

      def plots(self):
          log().info("Plotting...")
          ROOT.SetAtlasStyle()
          if type(self.infile) == str : filename = self.infile
          else : filename = self.infile.GetName()          
          f = ROOT.TFile(filename)
          mytree   =  f.Get("tree")

          printname = "/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/figs/"+self.outname
          outf = ROOT.TFile( printname+".root", 'recreate' )
          outf.cd()

          cv  = createRatioCanvas("cv", 1800, 1600)
          cv.Divide(2,2)

          cv.cd(1)
          mytree.Draw("trigger >> h_trigger (500,1000,1500)","","")
          h_tri = gDirectory.Get("h_trigger")
          h_tri.SetTitle("trigger time")
          h_tri.SetStats(0)
          h_tri.GetXaxis().SetTitle("trigger")
          h_tri.GetYaxis().SetTitle("count")
          h_tri.GetXaxis().SetLabelSize(0.03)
          h_tri.Write()         
          Cut = makecut(basecut=triggercut)
          cut = Cut.get()

          cv.cd(2)
          gPad.SetRightMargin(0.15)   
          #gPad.SetLogz(1) 
          gStyle.SetPalette(56)
          mytree.Draw("nsignaly_lv2:nsignalx_lv2 >> hn2d(15,0,15,15,0,15)",cut,"colz")
          h_nhit = gDirectory.Get("hn2d")
          h_nhit.SetTitle("nhits of lv2")
          h_nhit.GetXaxis().SetTitle("nhits Xaxis")
          h_nhit.GetYaxis().SetTitle("nhits Yaxis")
          h_nhit.GetZaxis().SetLabelSize(0.03)
          h_nhit.Write()
 
          cv.cd(3)
          hist_spectrum_s, hist_spectrum_p, hist_spectrum_n = spectrum(mytree,cut)
          hist_spectrum_s.Write()
          hist_spectrum_p.Write()
          hist_spectrum_n.Write()
          hist_spectrum_s.SetLineColor(1)
          hist_spectrum_s.SetLineWidth(1)
          #hist_spectrum_p.SetLineColor(ROOT.kPink+9)
          hist_spectrum_p.SetLineColorAlpha(2,0.7)
          hist_spectrum_p.SetLineWidth(1)
          hist_spectrum_n.SetLineColorAlpha(ROOT.kAzure-1,0.7)
          hist_spectrum_n.SetLineWidth(1)
          hist_spectrum_n.GetYaxis().SetLabelSize(0.035)

          maxbin = hist_spectrum_n.GetMaximum()
          if  hist_spectrum_p.GetMaximum() > maxbin : maxbin = hist_spectrum_p.GetMaximum()
          if  hist_spectrum_s.GetMaximum() > maxbin : maxbin = hist_spectrum_s.GetMaximum()
          hist_spectrum_n.SetMaximum(maxbin*1.15)

          leg = ROOT.TLegend(.55,.78,.90,.90)
          hist_spectrum_n.Draw("hist")
          hist_spectrum_p.Draw("hist same")
          #hist_spectrum_s.Draw("same")
          #leg.AddEntry(hist_spectrum_s,  "E #gamma", "l")
          leg.AddEntry(hist_spectrum_p,  "Pt side (Cathode)", "l")
          leg.AddEntry(hist_spectrum_n,  "Al side (Anode)", "l")
          leg.Draw("same")
          
 
          cv.cd(4)
          gPad.SetLeftMargin(0.15)
          gPad.SetBottomMargin(0.15)
          Cut.add(energycut)
          Cut.add("(nsignalx_lv2 == 1 && nsignaly_lv2 == 1)")
          cut = Cut.get()
          hist_image = image(mytree,cut,"all")
          hist_image.Write()
          gPad.SetLogz(0) 
          gStyle.SetPalette(56)
          gPad.SetRightMargin(0.15)
          hist_image.RebinX(1)
          hist_image.RebinY(1)
          hist_image.Draw("colz")

          outf.Write()
          cv.Print(printname+".pdf")
          log().info("Show plot : {}".format(printname.replace("../run/", "./")+".pdf"))

