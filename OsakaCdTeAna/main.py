#!/usr/bin/env python    
#-*- coding:utf-8 -*-   
"""
This module shows the main functions.
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu.irs@osaka-u.ac.jp"
__created__   = "2022-06-09"
__copyright__ = "Copyright 2022 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

# modules
import sys,os,random,math,ROOT
from ROOT import TFile, TTree, TCut, TChain, TSelector
from ROOT import gROOT, AddressOf
ROOT.gROOT.SetBatch(1)
import argparse, math, time, logging
from time import localtime, asctime
from multiprocessing import Pool, cpu_count
from array import array
from utils.helpers import GetInputList, GetInitUnixTime 
from core.process import Processor
from core.plot import Baseplot

def main(args):
    
    ilist = GetInputList(args.inputFolder)

    outname = "/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/"
    p = Processor(ifilelist = ilist, ofile=outname, addname=args.output, ncores=args.ncores, nevents=args.nevents, database=args.database)
    p.mainprocess()

    if p.fout is not None:
       b = Baseplot(infile=p.fout,outname=p.fout.GetName().split("/")[-1].split(".root")[0])
       b.plots()

    if os.path.isfile(outname+"latest"):
       os.system("cd %s"%(outname))
       os.system("rm -f latest")
    os.system("cd %s"%(outname))
    os.system("ln -sf %s latest"%(p.fout.GetName()))
    os.system("cd /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/")
    exit(0)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("inputFolder", type=str, default="/Users/chiu.i-huan/Desktop/new_scientific/imageAna/data/testinput/", help="Input File Name")
    parser.add_argument( "-o", "--output", type=str, default="tranadc_dsd", help="Output File Name")
    parser.add_argument( "-db", "--database", type=str, default=None, help="Database file Name")
    parser.add_argument( "-cpu", "--ncores", dest="ncores", type=int, default = 1, help="number of CPU")
    parser.add_argument( "-n", "--nevents", dest="nevents", type=int, default = None, help="Number of processing events." )
    args = parser.parse_args()

    main( args)

