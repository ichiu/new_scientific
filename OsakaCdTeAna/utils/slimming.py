#!/usr/bin/env python    
#-*- coding:utf-8 -*-   
"""
This module provides the transformation from adc to energy.
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu.irs@osaka-u.ac.jp"
__created__   = "2022-06-09"
__copyright__ = "Copyright 2022 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

# modules
import sys,os,random,math,ROOT
from ROOT import TFile, TTree, gROOT
ROOT.gROOT.SetBatch(1)
import argparse
from array import array
from random import gauss

def DisableBranch(tree):
    tree.SetBranchStatus("*",1)
    return tree

    
