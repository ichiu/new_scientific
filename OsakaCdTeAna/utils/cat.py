#!/usr/bin/env python    
#-*- coding:utf-8 -*-   
"""
This module provides definition of categories.
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu.irs@osaka-u.ac.jp"
__created__   = "2022-06-09"
__copyright__ = "Copyright 2022 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

# modules
import sys,os,random,math,ROOT
sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/core/')
from ROOT import TFile, TTree, TCut, TChain, TSelector
from ROOT import gROOT, AddressOf
import enums 
from hits import hitphoton

class EventCategory():
      def __init__(self, hitx=None, hity=None, response=None):
          # category for single cluster
          self.hitx = hitx 
          self.hity = hity
          self.DeltaEnergy = enums.DeltaEnergy
          self.response_dic = response # list, [gr_1d,gr_2d]
          self.case1, self.case2, self.case3, self.case4, self.case5 = None, None, None, None, None

          self.GetCategory(self.hitx, self.hity)
          self.photon_list = self.SumCategories()

      def GetCategory(self, _xlist, _ylist):         
          if len(_xlist) == 1 and len(_ylist) == 1:
             self.case1 = self.get1and1(_xlist[1], _ylist[1])
          elif len(_xlist) == 1 and len(_ylist) == 2:
             self.case2 = self.get1and2(_xlist[1], _ylist[1], _ylist[2])
          elif len(_xlist) == 2 and len(_ylist) == 1:
             self.case3 = self.get2and1(_xlist[1], _xlist[2], _ylist[1])
          elif len(_xlist) == 2 and len(_ylist) == 2:
             self.case4 = self.get2and2(_xlist[1], _xlist[2], _ylist[1], _ylist[2])
          elif len(_xlist) > 2 or len(_ylist) > 2:
             self.case5 = self.getother(_xlist,_ylist)

      def SumCategories(self):
          _dic = {}
          _n = 0
          if self.case1:
             for _i in self.case1:
                _n += 1
                _dic.update({_n:self.case1[_i]})
          elif self.case2:
             for _i in self.case2:
                _n += 1
                _dic.update({_n:self.case2[_i]})
          elif self.case3:
             for _i in self.case3:
                _n += 1
                _dic.update({_n:self.case3[_i]})
          elif self.case4:
             for _i in self.case4:
                _n += 1
                _dic.update({_n:self.case4[_i]})
          elif self.case5:
             for _i in self.case5:
                _n += 1
                _dic.update({_n:self.case5[_i]})
          return _dic

      def energy_correction1d(self, _hitx, _hity):
          # *** default ***
          n_init=-30; n_end=5; bin_range=0.2; nplots=int((n_end-(n_init))/bin_range)
          n_strip_limit=2

          # *** set limit ***
          epi1=(_hitx.energy+_hity.energy)/2.
          epi2=(_hitx.energy-_hity.energy)/2.
          nstt_x=_hitx.nstrips
          nstt_y=_hity.nstrips
          if epi2 < n_init: epi2 = n_init+0.01
          if epi2 > n_end: epi2 = n_end-0.01
          if nstt_x > n_strip_limit : nstt_x=n_strip_limit
          if nstt_y > n_strip_limit : nstt_y=n_strip_limit                   

          # *** find energy ***
          _index=int((epi2-n_init)/bin_range)
          index=_index+(nstt_x-1)*nplots+(nstt_y-1)*n_strip_limit*nplots# index of n_strips is from zero
          _response=self.response_dic[index]
          _e_corr=_response.Eval(epi1)
          return _e_corr          
 
      #def energy_correction2d(self, _hitx, _hity): #NOTE: to be fixed
      #    if _hitx.nstrips > 4: _hitx.nstrips = 4
      #    if _hity.nstrips > 3: _hity.nstrips = 3
      #    _name="p"+str(_hitx.nstrips)+"n"+str(_hity.nstrips)
      #    if not self.response_dic[1].get(_name): _name="p"+str(4)+"n"+str(3)
      #    _response=self.response_dic[1][_name]
      #    epi1=(_hitx.energy+_hity.energy)/2 # === epi1 is (e_pt+e_al)/2 ===
      #    epi2=(_hitx.energy-_hity.energy)/2 # === epi2 is (e_pt-e_al)/2 ===
      #    _e_corr=_response.Interpolate(epi1,epi2) #watanabe method
      #    return _e_corr

      def get1and1(self, _x0, _y0, _type=None):
          if _type is None: _t = 1
          else: _t = _type
          _d, _n={}, 0
          #_energy=(_x0.energy+_y0.energy)*0.5 # avg.
          _energy=self.energy_correction1d(_x0,_y0)# correction; TGraph1D
          
          _p = setphoton(_energy, _x0.energy,_y0.energy,_x0.adc,_y0.adc,_x0.position,_y0.position,_t)
          _n+=1
          _d.update({_n:_p})
          return _d

      def get1and2(self, _x0, _y0, _y1, _type=None):
          if _type is None: _t = 2
          else: _t = _type
          _d, _n={}, 0
          Ex0, Ey0, Ey1 = _x0.energy, _y0.energy, _y1.energy
          if( math.fabs(Ex0 - (Ey0+Ey1)) <= self.DeltaEnergy): # Two photons
             _p = setphoton(Ey0, Ex0*Ey0/(Ey0+Ey1), Ey0, _x0.adc*Ey0/(Ey0+Ey1), _y0.adc, _x0.position, _y0.position, _t)
             _n+=1
             _d.update({_n:_p})
             _p = setphoton(Ey1, Ex0*Ey1/(Ey0+Ey1), Ey1, _x0.adc*Ey1/(Ey0+Ey1), _y1.adc, _x0.position, _y1.position, _t)
             _n+=1
             _d.update({_n:_p})               
          else: # One noise
             if(math.fabs(Ex0 - Ey0) < math.fabs(Ex0 - Ey1)) and (math.fabs(Ex0 - Ey0) <= self.DeltaEnergy):
                _d = self.get1and1(_x0, _y0, 2)
             if (math.fabs(Ex0 - Ey0) > math.fabs(Ex0 - Ey1)) and (math.fabs(Ex0 - Ey1) <= self.DeltaEnergy):
                _d = self.get1and1(_x0, _y1, 2)
          return _d
                        
      def get2and1(self, _x0, _x1, _y0, _type=None):
          if _type is None: _t = 3
          else: _t = _type
          _d, _n={}, 0
          Ex0, Ex1, Ey0 = _x0.energy, _x1.energy, _y0.energy
          if( math.fabs(Ey0 - (Ex0+Ex1)) <= self.DeltaEnergy):
             _p = setphoton(Ex0, Ex0, Ey0*Ex0/(Ex0+Ex1), _x0.adc, _y0.adc*Ex0/(Ex0+Ex1), _x0.position, _y0.position, _t) 
             _n+=1
             _d.update({_n:_p})

             _p = setphoton(Ex1, Ex1, Ey0*Ex1/(Ex0+Ex1), _x1.adc, _y0.adc*Ex1/(Ex0+Ex1), _x1.position, _y0.position, _t) 
             _n+=1
             _d.update({_n:_p})               
          else: 
             if(math.fabs(Ey0 - Ex0) < math.fabs(Ey0 - Ex1)) and (math.fabs(Ey0 - Ex0) <= self.DeltaEnergy):
                _d = self.get1and1(_x0, _y0, 3)
             if (math.fabs(Ey0 - Ex0) > math.fabs(Ey0 - Ex1)) and (math.fabs(Ey0 - Ex1) <= self.DeltaEnergy):
                _d = self.get1and1(_x1, _y0, 3)
          return _d

      def get2and2(self, _x0, _x1, _y0, _y1, _type=None):
          if _type is None: _t = 4
          else: _t = _type
          _d, _n={}, 0
          Ex0, Ex1, Ey0, Ey1 = _x0.energy, _x1.energy, _y0.energy, _y1.energy
          if(math.fabs(Ex0+Ex1-Ey0-Ey1)  <= self.DeltaEnergy ):#two photons
             if(math.fabs(Ex0-Ey0) <= self.DeltaEnergy) and (math.fabs(Ex1-Ey1) <= self.DeltaEnergy):
                _p = setphoton(_x0.energy, _x0.energy, _y0.energy, _x0.adc, _y0.adc, _x0.position, _y0.position, _t)
                _n+=1
                _d.update({_n:_p})
                _p = setphoton(_x1.energy, _x1.energy, _y1.energy, _x1.adc, _y1.adc, _x1.position, _y1.position, _t)
                _n+=1
                _d.update({_n:_p})
             elif (math.fabs(Ex0-Ey1) <= self.DeltaEnergy) and (math.fabs(Ex1-Ey0) <= self.DeltaEnergy):
                _p = setphoton(_x0.energy, _x0.energy, _y1.energy, _x0.adc, _y1.adc, _x0.position, _y1.position, _t)
                _n+=1
                _d.update({_n:_p})
                _p = setphoton(_x1.energy, _x1.energy, _y0.energy, _x1.adc, _y0.adc, _x1.position, _y0.position, _t)
                _n+=1
                _d.update({_n:_p})
          elif ((Ey0+Ey1) > (Ex0+Ex1)):# one noise in y-side -> return case3 (2*1)
             if  (((Ex0+Ex1) - Ey0) <= self.DeltaEnergy):                   
                _d = self.get2and1(_x0,_x1,_y0, 4)
             elif (((Ex0+Ex1) - Ey1) <= self.DeltaEnergy):
                _d = self.get2and1(_x0,_x1,_y1, 4)
          else:# one noise in x-side -> return case2 (1*2)
             if  (((Ey0+Ey1) - Ex0) <= self.DeltaEnergy):
                _d = self.get1and2(_x0,_y0,_y1, 4)
             elif (((Ey0+Ey1) - Ex1) <= self.DeltaEnergy):
                _d = self.get1and2(_x1,_y0,_y1, 4)
          return _d

      def getother(self, _xlist, _ylist, _type=None):
          if _type is None: _t = 5
          else: _t = _type
          _d, _n, point={}, 0, 0
          maxpoint=min(len(_xlist),len(_ylist))
          xElist, yElist = setEnergyindex(_xlist), setEnergyindex(_ylist)
          for i in range(maxpoint):
             xi, Ex = xElist[i]
             yi, Ey = yElist[i]
             xhit, yhit = _xlist[xi], _ylist[yi]
             if math.fabs(Ex-Ey) <= self.DeltaEnergy:
                _p = setphoton((xhit.energy+yhit.energy)*0.5, xhit.energy, yhit.energy, xhit.adc, yhit.adc, xhit.position, yhit.position, _t)
                _n+=1
                _d.update({_n:_p})
          return _d

def setEnergyindex(hitlv2):
    lv2Elist={}
    for _hit in hitlv2:
       lv2Elist.update({_hit:hitlv2[_hit].energy})
    return sorted(lv2Elist.items(), key=lambda d: d[1], reverse=True)

def setphoton(_e,ep,en,adcp,adcn,x,y,case):
    _p = hitphoton()
    _p.energy  = _e
    _p.energy_pt  = ep
    _p.energy_al  = en
    _p.adc_pt     = adcp
    _p.adc_al     = adcn
    _p.x         = x
    _p.y         = y
    _p.type      = case
    return _p
          
