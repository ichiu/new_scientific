#!/usr/bin/env python    
#-*- coding:utf-8 -*-   
"""
This module provides the hit selection.
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu.irs@osaka-u.ac.jp"
__created__   = "2022-07-06"
__copyright__ = "Copyright 2022 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

# modules
import sys,os,random,math,ROOT
sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/core/')
from ROOT import TFile, TTree, gROOT
ROOT.gROOT.SetBatch(1)
import argparse
import math
from multiprocessing import Pool, cpu_count
from array import array
from random import gauss
import numpy as np
sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/utils/')
from hits import hitchannel
from cat import EventCategory 
import heapq 
import enums
import time
from scipy.special import comb

"""
Level-1 : pha threshold & Calibration
"""
def SetHitInfo(index, adc, energy, poi, strip, asic):
    _hit = hitchannel()
    _hit.index = index
    _hit.adc = adc
    _hit.energy = energy
    _hit.position = poi
    _hit.stripid = strip
    _hit.asic = asic
    return _hit    

def getpha(iasic, istrip, tree_adc, tree_cmn, dbname):
    stripid, _pha=int(istrip+iasic*64), tree_adc[istrip] - tree_cmn
    #NOTE: remove the dead channels during the experiment at Jan. 2023
    if stripid == 0 or stripid == 127  or stripid == 128  or stripid == 255: _pha = 0
    if "_201" in dbname:
       if stripid == 220: _pha = 0
    if "_202" in dbname:
       # iasic = 2; istrip = 22, 23, 24, 28, 29, 31, 32, 33
       if stripid == 150 or stripid == 151 or stripid == 152 or stripid == 156 or stripid == 157 or stripid == 159 or stripid == 160 or stripid == 161: _pha = 0
    if "_203" in dbname:
       if stripid == 187 or stripid == 191 or stripid == 195 or stripid == 202 or stripid == 205 or stripid == 207 or stripid == 209: _pha = 0
    return _pha, stripid 

def Level1Hit(tree, eline, phathre, dbname):
    n_hit_x, n_hit_y, istrip = 0, 0, 0
    signalx, signaly = {}, {}
    EnergyCut = enums.EnergyCut
    tree_adc, tree_cmn, tree_index, tree_hitnum = 0,0,0,0
    _hit = hitchannel()
    nasic,nstrip = 4, 64
    # ========= find signal region (p-side and n-side) ============
    for iasic in range(nasic): # number of ASIC, 0~3
       adc_name, cmn_name, index_name, hitnum_name = "adc"+str(iasic), "cmn"+str(iasic), "index"+str(iasic), "hitnum"+str(iasic) 
       # adc0[hitnum], cmn for all adc same  a chip, index in a chip, hit channel in a chip
       # NOTE: if hitnum is 64, means all channels be stored when any channel has signal, then istrip = ich; if hitnum is < 64 means that we need to find which channel has signal by tree_index
       tree_adc, tree_cmn, tree_index, tree_hitnum = getattr(tree,adc_name), getattr(tree,cmn_name), getattr(tree,index_name), getattr(tree,hitnum_name)
       for ich in range(tree_hitnum): #loop number of hit channels

          # === get pha ===
          # NOTE: index0 for hitnum0 is for adc0 -> ex: hitnum0 = 2, adc0[0] and adc0[1] for value
          if tree_adc[ich] == enums.ADCUpperBound: continue 
          istrip = tree_index[ich] # find ID in each chips -> istrip is in the range of 0~63 
          _pha, stripid = getpha(iasic, istrip, tree_adc, tree_cmn, dbname)
          _pha_thre=phathre[iasic].GetBinContent(istrip+1) # get pha cut for each istrip
          if _pha < _pha_thre*5: continue # 5 sigma threshold

          # === pha to energy ===
          pha = _pha+random.uniform(-0.5,0.5) # get random
          energy = eline[stripid].Eval(pha) # use tspline
          if energy < EnergyCut: continue # L1 energy cut

          # === store Lv1 hit ===
          if(iasic < nasic/2):
             n_hit_x += 1 #one hit
             poi=-16+stripid*0.250+0.125 # stripid to poi : 0 ~ 127 -> -16 ~ 16
             poi=random.gauss(poi,(0.25/2)/3.) # set gaus poi, 3sigma = 0.125 mm
             #poi=stripid # no axis
             signal_hitx = SetHitInfo(n_hit_x, pha, energy, poi, stripid, iasic)
             signalx.update({n_hit_x:signal_hitx})
          else:
             n_hit_y += 1
             poi=-16+(stripid-128)*0.25+0.125 # stripid to poi : 128 ~ 255 -> -16 ~ 16
             poi=random.gauss(poi,(0.25/2)/3.) # set gaus poi, 3sigma = 0.125 mm
             #poi=stripid-128 # no axis
             signal_hity = SetHitInfo(n_hit_y, pha, energy, poi, stripid, iasic)
             signaly.update({n_hit_y:signal_hity})               
    return signalx, signaly

        
"""
Level-2 : Merge
"""
def isAdjacent(check_index, _hit):
    if check_index == 1: return False # first hit (no pre. hit)
    current_chid=_hit[check_index].stripid
    pre_chid=_hit[check_index-1].stripid
    if math.fabs(current_chid-pre_chid) == 1: return True
    else: return False

def reset(index, _lv1hit, nad, _mhit):
    # === find energy and hit position of merge hit === 
    energy, adc = 0., 0.
    dic = {}
    for i in range(nad+1):
       new_index = index-i 
       energy += _lv1hit[new_index].energy
       adc += _lv1hit[new_index].adc
       dic.update({_lv1hit[new_index].energy:new_index})
    _maxEnergy = heapq.nlargest(2,dic)# get leading two energy, _maxEnergy is list
    _index = dic[_maxEnergy[0]]# find index of Level1 hits with max. energy

    _mhit.energy, _mhit.adc = energy, adc
    _mhit.stripid, _mhit.position = _lv1hit[_index].stripid, _lv1hit[_index].position # get stripid, poi with max energy
    _mhit.nstrips = nad+1
    _stripID1, _stripID2 = dic[_maxEnergy[0]], dic[_maxEnergy[1]] # find two leading index
    _mhit.position = (_lv1hit[_stripID1].position*_lv1hit[_stripID2].energy + _lv1hit[_stripID2].position*_lv1hit[_stripID1].energy)/(_lv1hit[_stripID1].energy + _lv1hit[_stripID2].energy)
    return _mhit

def mk_lv2hit(hist_dic):
    lv2hits={}
    for ih in hist_dic:
       newhit=hitchannel()
       newhit.nstrips=hist_dic[ih].GetEntries()
       newhit.energy=hist_dic[ih].Integral()
       if newhit.energy == 0 : continue
       #newhit.position=hist_dic[ih].newhit.GetXaxis().GetBinCenter(hist_dic[ih].GetMaximumBin()) #Old method
       weight=0;
       for i in range(hist_dic[ih].GetNbinsX()): weight+=hist_dic[ih].GetXaxis().GetBinCenter(i)*hist_dic[ih].GetBinContent(i) # poi*e (Furukawa method)
       newhit.position=weight/newhit.energy
       lv2hits.update({ih:newhit})
    return lv2hits
          
def Level2Hit(_hitx, _hity): 
    """
    === merge adjacent hits ===
      1. loop lv.1 hits : index of ch. ID is 0~127 for Pt, 128~255 for Al
      2. check adj. ch. : check hit-1 vs. hit-2; if yes-> check hit-2 vs. hit-3
      3. make lv.2 hits : find poi and energy
    """
     # === NOTE : Utokyo's method to get the same result, but take longer time with pyROOT ===
#    merge_xhit,merge_yhit={},{}
#    h_pe=ROOT.TH1D("h_pe","h_pe",128,-16.,16.)
#
#    n_lv2hit,h_dic=0,{} #n_lv2hit is lv.2 ID (from 1)
#    for ihit in _hitx:#Pt side
#       if(isAdjacent(ihit, _hitx)): # adj. hit
#          h_pe.Fill(_hitx[ihit].position,_hitx[ihit].energy)
#       else: # new hit (include 1st hit) 
#          h_pe.Reset()
#          h_pe.Fill(_hitx[ihit].position,_hitx[ihit].energy)
#          n_lv2hit += 1
#       h_dic.update({n_lv2hit:h_pe})
#    merge_xhit=mk_lv2hit(h_dic)
#
#    n_lv2hit,h_dic=0,{}
#    for ihit in _hity:
#       if(isAdjacent(ihit, _hity)): 
#          h_pe.Fill(_hity[ihit].position,_hity[ihit].energy)
#       else: 
#          h_pe.Reset()
#          h_pe.Fill(_hity[ihit].position,_hity[ihit].energy)
#          n_lv2hit += 1
#       h_dic.update({n_lv2hit:h_pe})
#    merge_yhit=mk_lv2hit(h_dic)
#    return merge_xhit, merge_yhit


    # === merge adjacent hit === 
    merge_xhit, merge_yhit = {}, {}
    merge_nx, merge_ny = {}, {}
    m_nx, m_ny = 0, 0  #id 
    n_adx, n_ady = 0, 0 #nstrips

    for ix in range(1, 1+len(_hitx)):
       if(isAdjacent(ix, _hitx)):
          n_adx += 1
          _newmergehit = hitchannel()
          _newmergehit = reset(ix, _hitx, n_adx, _newmergehit)
          merge_xhit.update({m_nx:_newmergehit})
       else:
          n_adx = 0
          m_nx += 1
          _hitx[ix].nstrips = n_adx+1
          merge_xhit.update({m_nx:_hitx[ix]}) 

    for iy in range(1, 1+len(_hity)):
       if(isAdjacent(iy, _hity)):
          n_ady += 1
          _newmergehit = hitchannel()
          _newmergehit = reset(iy, _hity, n_ady, _newmergehit)
          merge_yhit.update({m_ny:_newmergehit})
       else:
          n_ady = 0
          m_ny += 1
          _hity[iy].nstrips = n_ady+1
          merge_yhit.update({m_ny:_hity[iy]})
    return merge_xhit, merge_yhit


"""
Level-3 : Match & Energy Correction
"""
def matchLv2(_hitx, _hity, _r):
    # === matching Lv2 hit with energy info. & apply epi1-epi2 correction method ===
    c = EventCategory(hitx=_hitx, hity=_hity, response=_r)
    return c.photon_list
