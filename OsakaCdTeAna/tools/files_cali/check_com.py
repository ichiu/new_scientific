import sys,os,random,math,ROOT
ROOT.gROOT.SetBatch(1)
import argparse

def main(args):

    f=ROOT.TFile(args.inputFolder,"read")
    c1 = ROOT.TCanvas("c1","c1",1200,800)
    c1.Print("compare_spline.pdf"+"[","pdf")
    for i in range(256):
       l=f.Get("spline_{}".format(i))
       if i == 0 or i == 64 or i == 128 or i == 192: l.Draw()
       else: l.Draw("same")
       if i == 63 or i == 127 or i == 191 or i == 255: c1.Print("compare_spline.pdf","pdf")
    c1.Print("compare_spline.pdf"+"]","pdf")

    c2 = ROOT.TCanvas("c2","c2",1200,800)
    c2.cd()
    for i in range(256):
       l=f.Get("spline_{}".format(i))
       if i <64: l.SetLineColor(1)
       elif i <128: l.SetLineColor(2)
       elif i <192: l.SetLineColor(3)
       else: l.SetLineColor(4)
       if i == 0:l.Draw()
       else: l.Draw("same")
    c2.Print("compare_spline_all.pdf","pdf")

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("inputFolder", type=str, default="./spline_calibration_cdte202_Co.root", help="Input File Name")
    args = parser.parse_args()

    main( args)
