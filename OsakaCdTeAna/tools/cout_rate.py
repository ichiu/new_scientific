import sys,os,random,math,ROOT
ROOT.gROOT.SetBatch(1)
import argparse

def main(args):
    f=ROOT.TFile(args.inputFolder,"read")
    t=f.Get("eventtree")
    integral_time=0
    ti,tend=0,0
    for i in range(t.GetEntries()):
       t.GetEntry(i)
       if i == 0 : ti = t.unixtime
       if i == t.GetEntries()-1: tend = t.unixtime
       integral_time+=t.livetime

    print("measurement time: \033[1;35m {0} \033[0m sec.".format((tend-ti))) # 1 clock = 10 ns (100 MHz)
    print("integral livetime: \033[1;33m {:.1f} \033[0m sec.".format(integral_time/100000000)) # 1 clock = 10 ns (100 MHz)
    print("rate: \033[1;33m {:.1f} \033[0m cnt./sec.".format(t.GetEntries()/(tend-ti)))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("inputFolder", type=str, default="/Users/chiu.i-huan/Desktop/new_scientific/imageAna/data/testinput/", help="Input File Name")
    args = parser.parse_args()
    main( args)
