"""
This module makes the paper plots.
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu@rirc.osaka-u.ac.jp"
__created__   = "2023-02-11"
__copyright__ = "Copyright 2023 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

import sys,os,random,math,ROOT,argparse,yaml
sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/utils/')
from logger import log
import numpy as np
from root_numpy import hist2array, array2hist, tree2array
ROOT.gErrorIgnoreLevel = ROOT.kFatal
ROOT.gROOT.LoadMacro('./AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

hvcut="(!((1674382232 < unixtime && unixtime < 1674383510)||(1674389404 < unixtime && unixtime < 1674390632)||(1674398400 < unixtime && unixtime < 1674399616)||(1674408175 < unixtime && unixtime < 1674409885)||(1674416770 < unixtime && unixtime < 1674418095)||(1674424805 < unixtime && unixtime < 1674426121)||(1674433947 < unixtime && unixtime < 1674435622)||(1674441925 < unixtime && unixtime < 1674443217)||(1674450000 < unixtime && unixtime < 1674451235)||(1674458400 < unixtime && unixtime < 1674459726)||(1674466800 < unixtime && unixtime < 1674468028)||(1674475200 < unixtime && unixtime < 1674477748)||(1674486012 < unixtime && unixtime < 1674487295)||(1674494419 < unixtime && unixtime < 1674495740)||(1674502861 < unixtime && unixtime < 1674504172)||(1674511215 < unixtime && unixtime < 1674512621)||(1674519998 < unixtime && unixtime < 1674521213)||(1674528000 < unixtime && unixtime < 1674529290)||(1674536400 < unixtime && unixtime < 1674537560)||(1674544800 < unixtime && unixtime < 1674545958)||(1674553200 < unixtime && unixtime < 1674554522)||(1674560730 < unixtime && unixtime < 1674562244)||(1674568770 < unixtime && unixtime < 1674570048)||(1674577324 < unixtime && unixtime < 1674578679)||(1674586205 < unixtime && unixtime < 1674587846)))"
# === order need to be fixed ===
detector_list=["201","202","203","204"]
momentum_list = ["30MeV", "25MeV"]
rotation_list=["0","+30","-30"]
rtcut_list=["(1674352500 < unixtime && unixtime < 1674409000)", "(1674409000 < unixtime && unixtime < 1674441925)", "(1674441925 < unixtime && unixtime < 1674475200)", "(1674511215 < unixtime && unixtime < 1674553669)", "(1674553669 < unixtime && unixtime < 1674592200)", "(1674481320 < unixtime && unixtime < 1674511215)"]
rt_entries=[[812080, 524400, 545200], [597200, 257040, 438880]] # [30 MeV, 25 MeV], based on cdte201, for no beam time and beam power
# === check trigger distribution ===
triggercut="((trigger > 1025 && trigger < 1055) || (trigger > 1080 && trigger < 1120))"
#Atom_name,energy_cut="","(E_al_lv2 > 125 && E_al_lv2 < 140)"
#Atom_name,energy_cut="Si","(E_al_lv2 > 72 && E_al_lv2 < 82)"
Atom_name,energy_cut="Cu","(E_al_lv2 > 105 && E_al_lv2 < 125)"
#add_cut="(n_strips_al_lv2 == 1 && n_strips_pt_lv2 == 1)"
add_cut="(1)"

class makecut():
      def __init__(self,basecut):
          self.base = basecut
      def add(self,cutname):
          self.base += "&&" + cutname
      def get(self):
          return ROOT.TCut(self.base)

def getLatex():
    _t = ROOT.TLatex()
    _t.SetNDC()
    _t.SetTextFont( 62 )
    _t.SetTextColor( 36 )
    _t.SetTextSize( 0.08 )
    _t.SetTextAlign( 12 )
    return _t

def mk_es(args):
    inputfile = args.inputFolder + "data_cdtenumber_momentum_sum.root"

    Cut = makecut(basecut=hvcut)
    Cut.add(triggercut)
    Cut.add(add_cut)
    cut = Cut.get()
    signal_cut = ["((-5 < x && x < 0) && (-13 < y && y < 13))", "((-8 < x && x < -2) && (-7 < y && y < 15))", "((-8 < x && x < -2) && (-13 < y && y < 13))", "((-3 < x && x < 5) && (-15 < y && y < 15))"]
    bkg_cut = ["((-15 < x && x < -10) && (-13 < y && y < 13))", "((-15 < x && x < -9) && (-7 < y && y < 15))", "((9 < x && x < 15) && (-13 < y && y < 13))", "((-15 < x && x < -7) && (-15 < y && y < 15))"]
    print(cut)

    leg = ROOT.TLegend(.7,.7,.95,.95)
    leg.SetFillColorAlpha(0,0)
    leg.SetLineColorAlpha(0,0)
    leg.SetBorderSize(0)
    leg2 = ROOT.TLegend(.7,.7,.95,.95)
    leg2.SetFillColorAlpha(0,0)
    leg2.SetLineColorAlpha(0,0)
    leg2.SetBorderSize(0)
    latex = getLatex()


    for i, mom in enumerate(momentum_list):
       c1 = ROOT.TCanvas("c{}".format(i),"c{}".format(i),1400,1600)
       c1.Divide(2,2)

       for j, det in enumerate(detector_list):
          c1.cd(j+1)
          _iname = inputfile.replace("momentum",mom)
          iname = _iname.replace("number",det)
          ifile=ROOT.TFile(iname,"read")
          tree=ifile.Get("tree")
          tree.Draw("E_pt_lv2 >> h1pt(900,20,200)",cut,"colz")
          tree.Draw("E_al_lv2 >> h1al(900,20,200)",cut,"colz")
          tree.Draw("energy >> h1(900,20,200)",cut,"colz")
          h1=ROOT.gDirectory.Get("h1");h1al=ROOT.gDirectory.Get("h1al");h1pt=ROOT.gDirectory.Get("h1pt"); 
          h1.SetDirectory(0); h1al.SetDirectory(0); h1pt.SetDirectory(0); 
          h1.SetLineColor(1)
          h1pt.SetLineColor(4)
          h1al.SetLineColor(2)
          h1.SetLineWidth(1)
          h1pt.SetLineWidth(1)
          h1al.SetLineWidth(1)
          if j == 0 and i == 0:
             leg.AddEntry(h1pt, "pt", "l")
             leg.AddEntry(h1al, "al", "l")
             leg.AddEntry(h1, "sum", "l")
          h1.SetStats(0); h1pt.SetStats(0); h1al.SetStats(0);
          h1pt.SetTitle(";Energy [keV]; Counts [/0.2 keV]")
          h1.GetXaxis().CenterTitle(); h1.GetYaxis().CenterTitle();
          h1pt.SetMaximum(h1pt.GetMaximum()*1.5); h1pt.SetMinimum(0); h1pt.Draw("hist");
          h1pt.Draw("hist same"); h1al.Draw("hist same"); h1.Draw("hist same");
          if j == 0: leg.Draw("same")
          latex_name = "cdte {}".format(det)
          latex.DrawLatex(0.5,0.85,latex_name)

          cc = ROOT.TCanvas("cc_{}_{}".format(i,j),"cc_{}_{}".format(i,j),1200,1000)
          cc.cd()
          Cuts,Cutb = makecut(basecut=hvcut), makecut(basecut=hvcut)
          Cuts.add(triggercut); Cutb.add(triggercut);
          Cuts.add(add_cut); Cutb.add(add_cut);
          Cuts.add(signal_cut[j]); Cutb.add(bkg_cut[j]);
          cuts,cutb = Cuts.get(), Cutb.get()
          tree.Draw("energy >>  h1a(360,20,200)",cut,"colz")
          tree.Draw("energy >> h1_s(360,20,200)",cuts,"colz")
          tree.Draw("energy >> h1_b(360,20,200)",cutb,"colz")
          h1a=ROOT.gDirectory.Get("h1a"); h1_s=ROOT.gDirectory.Get("h1_s"); h1_b=ROOT.gDirectory.Get("h1_b");
          h1_s.SetDirectory(0);  h1_b.SetDirectory(0);
          h1_minus = h1_s.Clone()
          h1_minus.Add(h1_b,-1);
          h1a.SetMaximum((h1a.GetMaximum())*1.2);
          h1a.SetMinimum(0)
          h1a.SetTitle(";Energy [keV]; Counts [/0.5 keV]")
          h1a.GetXaxis().CenterTitle()
          h1a.GetYaxis().CenterTitle()
          h1a.SetLineColor(1)
          h1_s.SetLineColor(ROOT.kTeal+9)
          h1_b.SetLineColor(4)
          h1_minus.SetLineColor(2)
          h1a.Draw("hist");
          h1_s.Draw("hist same");
          h1_b.Draw("hist same");
          h1_minus.Draw("hist same");
          if j == 0 and i == 0:
             leg2.AddEntry(h1a, "All", "l")
             leg2.AddEntry(h1_s, "Signal", "l")
             leg2.AddEntry(h1_b, "Bkg.", "l")
             leg2.AddEntry(h1_minus, "Signal - Bkg.", "l")
          leg2.Draw("same")
          cc.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/cv_jparc_es_sb_{}_{}.pdf".format(det,mom));

       c1.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/cv_jparc_es_{}.pdf".format(mom))
       print("outfigs : /Users/chiu.i-huan/Desktop/temp_output/cv_jparc_es_{}.pdf".format(mom))


def mk_es_sum(args):
    inputfile = args.inputFolder + "data_cdtenumber_momentum_sum.root"
    # === cdte 201 -> 204 ===
    signal_cut_x = ["(-5 < x && x < 0)", "(-10 < x && x < 0)", "(-8 < x && x < -1)", "(-3 < x && x < 3)"]
    signal_cut_y = ["(-13 < y && y < 13)","(-13 < y && y < 13)","(-13 < y && y < 13)","(-13 < y && y < 13)"]
    bkg_cut_x = ["(10 < x && x < 15)","(5 < x && x < 15)","(8 < x && x < 15)","(9 < x && x < 15)"]
    bkg_cut_y = ["(-13 < y && y < 13)","(-13 < y && y < 13)","(-13 < y && y < 13)","(-13 < y && y < 13)"]

    leg = ROOT.TLegend(.4,.6,.75,.95)
    leg.SetFillColorAlpha(0,0)
    leg.SetLineColorAlpha(0,0)
    leg.SetBorderSize(0)

    for i, mom in enumerate(momentum_list):
       c1_es = ROOT.TCanvas("ces{}".format(i),"ces{}".format(i),1400,1400)
       h1a_sum, h1_s_sum, h1_b_sum, h1_minus_sum = ROOT.TH1D(), ROOT.TH1D(), ROOT.TH1D(), ROOT.TH1D()
       c1_es.cd()
       for j, det in enumerate(detector_list):
          _iname = inputfile.replace("momentum",mom)
          iname = _iname.replace("number",det)
          ifile=ROOT.TFile(iname,"read")
          tree=ifile.Get("tree")
          Cuts,Cutb = makecut(basecut=hvcut), makecut(basecut=hvcut)
          Cuts.add(triggercut); Cutb.add(triggercut);
          Cuts.add(add_cut); Cutb.add(add_cut);
          Cuts.add(signal_cut_x[j]); 
          Cuts.add(signal_cut_y[j]); 
          Cutb.add(bkg_cut_x[j]);
          Cutb.add(bkg_cut_y[j]);
          cuts,cutb = Cuts.get(), Cutb.get()

          tree.Draw("energy >> h1_s(300,30,180)",cuts,"colz")
          tree.Draw("energy >> h1_b(300,30,180)",cutb,"colz")
          h1_s=ROOT.gDirectory.Get("h1_s"); h1_b=ROOT.gDirectory.Get("h1_b");
          h1_s.SetDirectory(0);  h1_b.SetDirectory(0);
          h1a = h1_s.Clone()
          h1_minus = h1_s.Clone()
          h1a.Add(h1_b,1)
          h1_minus.Add(h1_b,-1)
          h1a.SetDirectory(0);  h1_minus.SetDirectory(0);

          if j == 0 and i == 0:
             leg.AddEntry(h1a, "Sum", "l")
             leg.AddEntry(h1_s, "Signal", "l")
             leg.AddEntry(h1_b, "Bkg.", "l")
             leg.AddEntry(h1_minus, "Signal - Bkg.", "l")
          if j == 0:
             h1a_sum=h1a
             h1_s_sum=h1_s
             h1_b_sum=h1_b
             h1_minus_sum=h1_minus
          else: 
             h1a_sum.Add(h1a,1)
             h1_s_sum.Add(h1_s,1)
             h1_b_sum.Add(h1_b,1)
             h1_minus_sum.Add(h1_minus,1)
       h1a_sum.SetMaximum((h1a_sum.GetMaximum())*1.2);
       h1a_sum.SetMinimum(0)
       h1a_sum.SetTitle(";Energy [keV]; Counts [/0.5 keV]")
       h1a_sum.GetXaxis().CenterTitle()
       h1a_sum.GetYaxis().CenterTitle()
       h1a_sum.SetLineColor(1)
       h1_s_sum.SetLineColor(ROOT.kTeal+9)
       h1_b_sum.SetLineColor(4)
       h1_minus_sum.SetLineColor(2)
       h1a_sum.Draw("hist");
       h1_s_sum.Draw("hist same");
       h1_b_sum.Draw("hist same");
       h1_minus_sum.Draw("hist same");
       leg.Draw("same")
       c1_es.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/cv_jparc_es_sum_{}.pdf".format(mom));


       c1_im = ROOT.TCanvas("cim{}".format(i),"cim{}".format(i),1400,1400)
       h2sum = ROOT.TH2D()
       Cut = makecut(basecut=hvcut)
       Cut.add(triggercut)
       Cut.add(energy_cut)
       Cut.add(add_cut)
       cut = Cut.get()
       for j, det in enumerate(detector_list):
          _iname = inputfile.replace("momentum",mom)
          iname = _iname.replace("number",det)
          ifile=ROOT.TFile(iname,"read")
          tree=ifile.Get("tree")
          tree.Draw("x:y >> h2(128,-16,16, 128,-16,16)",cut,"colz")
          h2=ROOT.gDirectory.Get("h2");
          h2.SetDirectory(0);
          h2.SetTitle("; Al side [mm]; Pt side [mm]")
          h2.GetXaxis().CenterTitle(1)
          h2.GetYaxis().CenterTitle(1)
          h2.SetStats(0)
          if j == 0 : h2sum=h2
          else: h2sum.Add(h2,1)
       c1_im.cd()
       _h2=h2sum.Clone()
       _array=hist2array(h2sum)
       _array[np.where(_array == 0)]=0.0000000000001
       array2hist(_array,_h2)
       #_h2.GetZaxis().SetRangeUser(0.1, 600)
       _h2.Draw("colz") 
       ROOT.gPad.SetLogz(0)
       ROOT.gStyle.SetPalette(56)
       ROOT.gPad.SetRightMargin(0.15)
       c1_im.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/cv_jparc_image_sum_{}.pdf".format(mom));
         
def correction(i,_array):
    # NOTE: for rutubo image
    # _array[:,24] -> get [xbin] at ybin = 24
    if i == "202": 
       dead_chs = [150,151,152,156,157,159,160,161]; dead_chs= [i-128 for i in dead_chs]
       #for i, ich in enumerate(dead_chs): _array[:,ich]=(_array[:,ich-1]+_array[:,ich+1])/2. 
       for i, ich in enumerate(dead_chs): _array[ich,:]=(_array[ich-1,:]+_array[ich+1,:])/2.
    if i == "203": 
       dead_chs = [187,191,195,202,205,207,209]; dead_chs= [i-128 for i in dead_chs]
       #for ich in dead_chs: _array[:,ich]=(_array[:,ich-1]+_array[:,ich+1])/2.
       for i, ich in enumerate(dead_chs): _array[ich,:]=(_array[ich-1,:]+_array[ich+1,:])/2.
    if i == "201": 
       dead_chs = [220]; dead_chs= [i-128 for i in dead_chs]
       #for ich in dead_chs: _array[:,ich]=(_array[:,ich-1]+_array[:,ich+1])/2.
       for i, ich in enumerate(dead_chs): _array[ich,:]=(_array[ich-1,:]+_array[ich+1,:])/2.
    return _array

def mk_image(args):
    inputfile = args.inputFolder + "data_cdtenumber_momentum_sum.root"
    leg = ROOT.TLegend(.7,.7,.95,.95)
    leg.SetFillColorAlpha(0,0)
    leg.SetLineColorAlpha(0,0)
    leg.SetBorderSize(0)
    latex = getLatex()

    for i, mom in enumerate(momentum_list):
       outfile_name = "/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_rutsubo/rotation_image_{}_{}.root".format(mom,Atom_name)
       fout = ROOT.TFile(outfile_name,"recreate")
       for k, rot in enumerate(rotation_list):
          c1 = ROOT.TCanvas("c{}{}".format(i,k),"c{}{}".format(i,k),1400,1600)
          c1.Divide(2,2)

          Cut = makecut(basecut=hvcut)
          Cut.add(triggercut)
          Cut.add(rtcut_list[i*3+k])
          Cut.add(energy_cut)
          Cut.add(add_cut)
          cut = Cut.get()

          print("{}, {}".format(mom, rot))

          for j, det in enumerate(detector_list):
             c1.cd(j+1)
             _iname = inputfile.replace("momentum",mom)
             iname = _iname.replace("number",det)
             ifile=ROOT.TFile(iname,"read")
             tree=ifile.Get("tree")
             #tree.Draw("y:x >> h2(128,-16,16, 128,-16,16)",cut,"colz")
             tree.Draw("x:y >> h2(128,-16,16, 128,-16,16)",cut,"colz")
             h2=ROOT.gDirectory.Get("h2");
             h2.SetDirectory(0);
             #h2.SetTitle("; Pt side X [mm]; Al side Y [mm]")
             h2.SetTitle("; Al side [mm]; Pt side Y [mm]")
             h2.GetXaxis().CenterTitle(1)
             h2.GetYaxis().CenterTitle(1)
             h2.SetStats(0)

             # === set angle ===      
             if det == "202" and rot == "0":   iorder = 6
             if det == "202" and rot == "+30": iorder = 7
             if det == "203" and rot == "-30": iorder = 8 
             if det == "203" and rot == "0":   iorder = 9
             if det == "203" and rot == "+30": iorder = 10
             if det == "204" and rot == "-30": iorder = 11
             if det == "204" and rot == "0":   iorder = 0
             if det == "204" and rot == "+30": iorder = 1
             if det == "201" and rot == "-30": iorder = 2
             if det == "201" and rot == "0":   iorder = 3
             if det == "201" and rot == "+30": iorder = 4
             if det == "202" and rot == "-30": iorder = 5

             # === correction ===
             h2_clone=h2.Clone()
             _array=hist2array(h2)
             _array=correction(det,_array)
             array2hist(_array,h2_clone)
             h2_clone.SetDirectory(0)

             # === reweight ===
             min_entry = min(rt_entries[i])
             print(min_entry/rt_entries[i][k])
             h2_clone.Scale(min_entry/rt_entries[i][k])
             h2.Scale(min_entry/rt_entries[i][k])

             # === plot ===
             ROOT.gPad.SetLogz(0)
             ROOT.gStyle.SetPalette(56)
             ROOT.gPad.SetRightMargin(0.15)
             h2.Draw("colz") 
             latex_name = "cdte {}".format(det)
             latex.DrawLatex(0.5,0.85,latex_name)

             # === store ===
             h2_clone.SetName("h{}".format(iorder))
             h2.SetName("h{}_ori".format(iorder))
             fout.cd()
             h2_clone.Write()
             h2.Write()

             # === all plot ===
             Zrange = 10
             if mom == "25MeV": Zrange = 22
             else: Zrange = 26
             Zrange = 12
             c2 = ROOT.TCanvas("c2{}{}".format(i,k),"c2{}{}".format(i,k),1400,1400)
             h2_clone2=h2.Clone()
             _array=hist2array(h2)
             _array[np.where(_array == 0)]=0.0000000000001
             array2hist(_array,h2_clone2)
             h2_clone2.SetDirectory(0)
             h2_clone2.Scale(min_entry/rt_entries[i][k])
             ROOT.gPad.SetLogz(0)
             ROOT.gStyle.SetPalette(56)
             ROOT.gPad.SetRightMargin(0.15)
             h2_clone2.Rebin2D(4,4)
             h2_clone2.GetZaxis().SetRangeUser(0, Zrange)
             h2_clone2.Draw("colz")
             c2.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/cv_jparc_image_cdte{}_{}_{}_{}.pdf".format(det,mom,rot,Atom_name))
          c1.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/cv_jparc_image_{}_{}_{}.pdf".format(mom,rot,Atom_name))
       fout.Close()

    latex = getLatex()
    Zrange, _rbin = 6, 1
    for imon in momentum_list:
       inputfile_name = "/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_rutsubo/rotation_image_{}_{}.root".format(imon,Atom_name)
       fint = ROOT.TFile(inputfile_name,"read")
       cv_name="/Users/chiu.i-huan/Desktop/temp_output/fig_rotation_"+imon+Atom_name+".pdf"
       c1=ROOT.TCanvas("cv_mkimage_{}".format(imon),"cv_mkimage_{}".format(imon),1200,1000)
       c1.Print(cv_name + "[", "pdf")
       for i in range(12):
          angle=0+30*i
          _h=fint.Get("h{}".format(i))
          _h.SetDirectory(0)
          #_h.SetTitle("; Pt side X [mm]; Al side Y [mm]")
          _h.SetTitle("; Al side [mm]; Pt side [mm]")
          _h.GetXaxis().CenterTitle(1)
          _h.GetYaxis().CenterTitle(1)
          _h.SetStats(0)
          _h.Rebin2D(_rbin,_rbin)
          _h.GetZaxis().SetRangeUser(0, Zrange)
          ROOT.gPad.SetLogz(0)
          ROOT.gStyle.SetPalette(56)
          ROOT.gPad.SetRightMargin(0.15)
          _h.Draw("colz")
          latex_name = "angle:{}".format(angle)
          latex.DrawLatex(0.6,0.85,latex_name)
          c1.Print(cv_name, "pdf")
       c1.Print(cv_name + "]", "pdf")
    fint.Close()

def mk_image_Co(args):

    inputfile = "/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_Co/data_cdtenumber_Co_sum.root"
    leg = ROOT.TLegend(.7,.7,.95,.95)
    leg.SetFillColorAlpha(0,0)
    leg.SetLineColorAlpha(0,0)
    leg.SetBorderSize(0)
    latex = getLatex()

    Cut = makecut(basecut="(1)")
    Cut.add("(energy > 118 && energy < 126)")
    cut = Cut.get()
    h2_list=[ROOT.TH2D(), ROOT.TH2D(), ROOT.TH2D(), ROOT.TH2D()]
    for j, det in enumerate(detector_list):
        c1 = ROOT.TCanvas("c{}".format(j),"c{}".format(j),1200,1200)
        c1.cd()
        iname = inputfile.replace("number",det)
        ifile=ROOT.TFile(iname,"read")
        tree=ifile.Get("tree")
        tree.Draw("y:x >> h2(128,-16,16, 128,-16,16)",cut,"colz")
        #tree.Draw("x:y >> h2(128,-16,16, 128,-16,16)",cut,"colz")
        h2_list[j]=ROOT.gDirectory.Get("h2");
        h2_list[j].SetDirectory(0);
        h2_list[j].SetTitle("; Pt side X [mm]; Al side Y [mm]")
        #h2_list[j].SetTitle("; Al side [mm]; Pt side [mm]")
        h2_list[j].GetXaxis().CenterTitle(1)
        h2_list[j].GetYaxis().CenterTitle(1)
        h2_list[j].SetStats(0)
        _h2=h2_list[j].Clone()
        _array=hist2array(h2_list[j])
        #_array=correction(det,_array)
        _array[np.where(_array == 0)]=0.0000000000001
        array2hist(_array,_h2)
        _h2.GetZaxis().SetRangeUser(0, 600)
        _h2.Draw("colz") 
        ROOT.gPad.SetLogz(0)
        ROOT.gStyle.SetPalette(56)
        ROOT.gPad.SetRightMargin(0.15)
        latex_name = "cdte {}".format(det)
        latex.DrawLatex(0.5,0.35,latex_name)
        c1.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/cv_jparc_image_Co_{}.pdf".format(det))

    outfile_name = "/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_Co/rotation_image_Co.root"
    fout = ROOT.TFile(outfile_name,"recreate")
    fout.cd()
    for i, h2 in enumerate(h2_list):
       if i == 0:   iorder = 1 #201
       if i == 1:   iorder = 2 #202
       if i == 2:   iorder = 3 #203
       if i == 3:   iorder = 0 #204
       h2.SetName("h{}".format(iorder))
       h2.Write()
    fout.Close()

def get_image_sim(args):

    inputfile = "/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/root/Output_sample_pointX5Y5Z5.root"
    #inputfile = "/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/root/Output_sample_line.root"
    #inputfile = "/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/root/Output_sample_circle.root"
    _sname="point"# point, line, or circle
    leg = ROOT.TLegend(.7,.7,.95,.95)
    leg.SetFillColorAlpha(0,0)
    leg.SetLineColorAlpha(0,0)
    leg.SetBorderSize(0)
    latex = getLatex()

    h2_list=[ROOT.TH2D(), ROOT.TH2D(), ROOT.TH2D(), ROOT.TH2D()]
    ifile=ROOT.TFile(inputfile,"read")
    for j, det in enumerate(detector_list):
        c1 = ROOT.TCanvas("c{}".format(j),"c{}".format(j),1200,1200)
        c1.cd()
        h2=ifile.Get("image_CdTe{}".format(j+1))
        h2.SetDirectory(0)
        h2_list[j]=h2
        h2_list[j].SetTitle("; Pt side X [mm]; Al side Y [mm]")
        h2_list[j].GetXaxis().CenterTitle(1)
        h2_list[j].GetYaxis().CenterTitle(1)
        h2_list[j].SetStats(0)
        _h2=h2_list[j].Clone()
        _array=hist2array(h2_list[j])
        _array[np.where(_array == 0)]=0.0000000000001
        array2hist(_array,_h2)
        _h2.GetZaxis().SetRangeUser(0, 20)
        _h2.Draw("colz") 
        ROOT.gPad.SetLogz(0)
        ROOT.gStyle.SetPalette(56)
        ROOT.gPad.SetRightMargin(0.15)
        latex_name = "cdte {}".format(det)
        latex.DrawLatex(0.5,0.85,latex_name)
        c1.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/cv_jparc_image_sim_{}_{}.pdf".format(_sname,det))

    outfile_name = "/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/root/rotation_image_sim_{}.root".format(_sname)
    fout = ROOT.TFile(outfile_name,"recreate")
    fout.cd()
    for i, h2 in enumerate(h2_list):
       if i == 0:   iorder = 1 #201
       if i == 1:   iorder = 2 #202
       if i == 2:   iorder = 3 #203
       if i == 3:   iorder = 0 #204
       h2.SetName("h{}".format(iorder))
       h2.Write()
    fout.Close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("--inputFolder", dest="inputFolder", type=str, default="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_rutsubo/", help="Input File Name")
    parser.add_argument("-o","--output", dest="output", type=str, default="test", help="Output File Name")
    args = parser.parse_args()

    #mk_es(args)
    #mk_es_sum(args)
    mk_image(args)
    #mk_image_Co(args)
    #get_image_sim(args)
