import argparse,sys,os,random,math,ROOT,glob

def GetInputList(inputFolder):
    inputDict = list()
    if ".root" in inputFolder :
       inputDict.append(inputFolder)
       return inputDict

    subFolders = glob.glob(inputFolder+"/*.root")
    for subFolder in subFolders:
       inputDict.append(subFolder)
    return inputDict

def GetTChain(inputFolder,treename):
    _list=GetInputList(inputFolder)
    chain = ROOT.TChain(treename)
    for _l in _list:
       chain.AddFile(_l)
    return chain

def plt(args):
    mychain=GetTChain(args.inputFolder,"tree")
    energy_range, bin_size=[], 0.1
    if args.output == "Ba": energy_range=[70, 360]
    elif args.output == "Co": energy_range=[10, 150]
    else: 
      print("output is Ba or Co")
      return 0
    nbin=(energy_range[1]-energy_range[0])/bin_size

    cv_name="/Users/chiu.i-huan/Desktop/temp_output/check_cali_{}.pdf".format(args.output)
    c0 = ROOT.TCanvas(cv_name,"",0,0,1200,800)
    c0.Print(cv_name + "[", "pdf")
    for i in range(128,256): # only check al side
       mychain.Draw("E_al_lv2 >> h0({0},{1},{2})".format(nbin,energy_range[0],energy_range[1]),"stripid_al_lv2 == {}".format(i),"")
       h0 = ROOT.gDirectory.Get("h0")
       height=h0.GetEntries()
       line1=ROOT.TLine(356.017,0,356.017,height)
       line2=ROOT.TLine(81,0,81,height)
       line3=ROOT.TLine(122.06,0,122.06,height)
       h0.Draw()
       line1.Draw()
       line2.Draw()
       line3.Draw()
       c0.Print(cv_name, "pdf")
    c0.Print(cv_name+"]", "pdf")
       
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("inputFolder", type=str, default="/Users/chiu.i-huan/Desktop/new_scientific/imageAna/data/testinput/", help="Input File Name")
    parser.add_argument("-o","--output", dest="output", type=str, default="Source", help="Output File Name")
    args = parser.parse_args()

    plt(args)
