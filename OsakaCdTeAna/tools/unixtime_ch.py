"""
2023 January Image Exp.
unittime of HV down-up
→ not used those events in the analysis
"""
import time 

def datetime_timestamp(dt):
    time.strptime(dt, '%Y-%m-%d %H:%M:%S')
    s = time.mktime(time.strptime(dt, '%Y-%m-%d %H:%M:%S'))
    return int(s)


rot_time=[
["2023-01-22 10:55:00", "2023-01-23 02:36:40", "30 MeV, 0"],
["2023-01-23 02:36:40", "2023-01-23 11:45:25", "30 MeV, +30"],
["2023-01-23 11:45:25", "2023-01-23 21:00:00", "30 MeV, -30"],
["2023-01-24 07:00:15", "2023-01-24 18:47:49", "25 MeV, 0"],
["2023-01-24 18:47:49", "2023-01-25 05:30:00", "25 MeV, +30"],
["2023-01-23 22:42:00", "2023-01-24 07:00:15", "25 MeV, -30"],
]

HV_time=[
["2023-01-22 19:10:32","2023-01-22 19:31:50"], 
["2023-01-22 21:10:04","2023-01-22 21:30:32"],
["2023-01-22 23:40:00","2023-01-23 00:00:16"],
["2023-01-23 02:22:55","2023-01-23 02:51:25"],
["2023-01-23 04:46:10","2023-01-23 05:08:15"],
["2023-01-23 07:00:05","2023-01-23 07:22:01"],
["2023-01-23 09:32:27","2023-01-23 10:00:22"],
["2023-01-23 11:45:25","2023-01-23 12:06:57"],
["2023-01-23 14:00:00","2023-01-23 14:20:35"],
["2023-01-23 16:20:00","2023-01-23 16:42:06"],
["2023-01-23 18:40:00","2023-01-23 19:00:28"],
["2023-01-23 21:00:00","2023-01-23 21:42:28"],
["2023-01-24 00:00:12","2023-01-24 00:21:35"],
["2023-01-24 02:20:19","2023-01-24 02:42:20"],
["2023-01-24 04:41:01","2023-01-24 05:02:52"],
["2023-01-24 07:00:15","2023-01-24 07:23:41"],
["2023-01-24 09:26:38","2023-01-24 09:46:53"],
["2023-01-24 11:40:00","2023-01-24 12:01:30"],
["2023-01-24 14:00:00","2023-01-24 14:19:20"],
["2023-01-24 16:20:00","2023-01-24 16:39:18"],
["2023-01-24 18:40:00","2023-01-24 19:02:02"],
["2023-01-24 20:45:30","2023-01-24 21:10:44"],
["2023-01-24 22:59:30","2023-01-24 23:20:48"],
["2023-01-25 01:22:04","2023-01-25 01:44:39"],
["2023-01-25 03:50:05","2023-01-25 04:17:26"]]

print(HV_time)
print("   ")
print("========= HV cut ============")
print("   ")

cut_list = "("
for i, hvtime in enumerate(HV_time):
   down_time, up_time = datetime_timestamp(hvtime[0]), datetime_timestamp(hvtime[1])
   hv_cut = "({} < unixtime && unixtime < {})".format(down_time,up_time)
   print(hv_cut)
   cut_list += hv_cut 
   if i == (len(HV_time)-1): continue
   cut_list += "||"
cut_list += ")"
print("   ")
print(cut_list)

print("   ")
print("========= Rotation selection ============")
print("   ")

for i, rttime in enumerate(rot_time):
   down_time, up_time = datetime_timestamp(rttime[0]), datetime_timestamp(rttime[1])
   rot_cut = "{} : ({} < unixtime && unixtime < {})".format(rttime[2],down_time,up_time)
   print(rot_cut)
