#include "mk_map.h"
#include "make_EPI1vsEPI2.h"

void mk_map(int map_det_ID){

  c0 = new TCanvas("new temp. canvas","new temp. canvas",10,10,800,800);
  fint = new TFile(Form("/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/response/Eavg_plots/Eavg_forResp_range%.1fkeV_cdte%d.root",range,map_det_ID),"READ");
  fout = new TFile("/Users/chiu.i-huan/Desktop/temp_output/ep1_ep2_map.root","RECREATE");

  EPI2_range = (EPI2_high-(EPI2_low))/range;
  n_Epeaks = 1+2; // only Co 122 peak + sero & final point
  g = new TGraph2D(n_Epeaks*EPI2_range);
  
  fout->cd();
  c0->Print(Form("/Users/chiu.i-huan/Desktop/temp_output/map_fitting_cdte%d.pdf[",map_det_ID), "pdf");
  int index = 0;
  Double_t x_1d[n_Epeaks], y_1d[n_Epeaks];
  //TODO : Set hist. range, set source
  for(int i = 0; i < EPI2_range; i++){
     EPI2_loop=EPI2_low+range*i;
     h1 = (TH1D*)fint->Get(Form("h%d_nx%d_ny%d",i,1,1));
     h1_temp=(TH1D*)h1->Clone();
     h1_temp->GetXaxis()->SetRangeUser(122.06-50, 122.06+10);
     s = new TSpectrum(1);
     s->SetResolution(1);
     nfound = s->Search(h1_temp,0.01,"",0.005);
     xpeaks = s->GetPositionX();
     xp = xpeaks[0];//find peak
     h1_temp->Draw("");
     c0->Print(Form("/Users/chiu.i-huan/Desktop/temp_output/map_fitting_cdte%d.pdf",map_det_ID));
     std::cout << "index : " << i  << Form("  ,  %.1f < E < %.1f keV , Found peak at : %.1f keV", EPI2_loop, EPI2_loop+range, xp) << std::endl;

     for (int y = 0; y < n_Epeaks; y++){//energy point 

        if (y==0){ yp = 0; zp= 0; x_1d[0] = 0; y_1d[0]=0;}
        if (y==1){ yp=xp; zp = 122.06 + ((double) rand() / (RAND_MAX + 1.0))*0.1; x_1d[1] = xp; y_1d[1]=122.06;}
        if (y==2){ yp=((xp-0)/(122.06+ ((double) rand() / (RAND_MAX + 1.0))*0.1 -0))*500; zp = 500; x_1d[2] = ((xp-0)/(122.06-0))*500; y_1d[2]=500;}
        g->SetPoint(index,yp,EPI2_loop+0.5*range,zp);
        index++;
     }

     gr = new TGraph(n_Epeaks,x_1d,y_1d);
     gr->SetName(Form("Graph_map%d",i));
     gr->Write();     
  }
  c0->Print(Form("/Users/chiu.i-huan/Desktop/temp_output/map_fitting_cdte%d.pdf]",map_det_ID), "pdf");
  g->SetName(Form("graph2d_%d_%d",1,1));
  g->SetTitle(";E_{avg.};#DeltaE;E_{exp.}");
  g->Write();
  std::cout << Form(" Check peak : /Users/chiu.i-huan/Desktop/temp_output/map_fitting_cdte%d.pdf",map_det_ID) << std::endl; 
     
 }

