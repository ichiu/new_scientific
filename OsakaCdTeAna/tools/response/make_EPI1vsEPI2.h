#ifndef make_EPI1vsEPI2_H
#define make_EPI1vsEPI2_H

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <TGraph.h>

    char inputfile;
    char source;
    int det_ID;
    char outname[256];

    TCanvas *c0 = new TCanvas("temp canvas","temp canvas",10,10,800,800);
    TCanvas *c1 = new TCanvas("c1","Energy Spectum comparison",10,10,1000,800);
    TCanvas *c2 = new TCanvas("c2 test","Energy Spectum",10,10,1000,800);
    TCanvas *c3 = new TCanvas("c3","C3 Energy Spectum",10,10,1000,800);

    TH2D *ha_com;
    TFile* fint, *fout;
    TTree* tree;
    TH1D *h1, *h2, *h3;

    TH2D* hmap;
    TH2D *hmap_temp;
    int h_index;
    double EPI2_loop;
    const double EPI1_low = 72;
    const double EPI1_high = 132;
    const double EPI2_low = -30;
    const double EPI2_high = 5;
    const double range = 1;
    const int nstrip_limit = 2;
    TH1D* hepoint[nstrip_limit][nstrip_limit];
    Double_t epi1_avg=0;
    Double_t epi2_sub=0;
    Int_t eloop;
    double e_points = 0;

    unsigned int nsignalx_lv2;  
    unsigned int nsignaly_lv2; 
    Double_t   E_pt_lv2[128]; 
    Double_t   E_al_lv2[128];
    Int_t n_strips_pt_lv2[128];
    Int_t n_strips_al_lv2[128];

#endif
