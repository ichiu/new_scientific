#ifndef mk_map_H
#define mk_map_H 1

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <TGraph.h>


   char map_source;
   int map_det_ID;
   double x,y,z;
   int EPI2_range;// delta range
   int n_Epeaks;// # of peaks
   Double_t Epeaks_table;

   TH1D* h1_temp;
   TFile* f_hist;
   TGraph2D *g;
   TGraph* gr;
   TSpectrum *s;
   Int_t nfound;
   Double_t *xpeaks;
   Double_t xp;
   Double_t yp;
   Double_t zp;
   
#endif
