import sys,os,random,math,ROOT,glob
ROOT.gROOT.SetBatch(1)
ROOT.gErrorIgnoreLevel = ROOT.kWarning
import argparse

def GetInputList(inputFolder):
    inputDict = list()
    if ".root" in inputFolder :
       inputDict.append(inputFolder)
       return inputDict

    subFolders = glob.glob(inputFolder+"/*.root")
    for subFolder in subFolders:
       inputDict.append(subFolder)
    return inputDict

def GetTChain(inputFolder,treename):
    _list=GetInputList(inputFolder)
    chain = ROOT.TChain(treename)
    for _l in _list:
       chain.AddFile(_l)
    return chain

def plt(sigma_list): 
    """
    make hist. for ped. cut
    """
    hout_1 = ROOT.TH1D("h_pha_thre_chip1","h_pha_thre_chip1",64,0,64)
    hout_2 = ROOT.TH1D("h_pha_thre_chip2","h_pha_thre_chip2",64,0,64)
    hout_3 = ROOT.TH1D("h_pha_thre_chip3","h_pha_thre_chip3",64,0,64)
    hout_4 = ROOT.TH1D("h_pha_thre_chip4","h_pha_thre_chip4",64,0,64)
    for ichip in range(4):
       for ich in range(64):
          _sigma=sigma_list[ichip*64+ich]
          if ichip == 0: hout_1.Fill(ich,_sigma)
          if ichip == 1: hout_2.Fill(ich,_sigma)
          if ichip == 2: hout_3.Fill(ich,_sigma)
          if ichip == 3: hout_4.Fill(ich,_sigma)
    fout=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/auxfile/ped_thre_"+args.output+"_cmn.root","recreate")
    fout.cd()
    hout_1.Write()
    hout_2.Write()
    hout_3.Write()
    hout_4.Write()
    fout.Close()

def find_ped(args):
    flist=GetInputList(args.inputFolder)
    fit_range=[0,200] # range to check ped. position and sigma
    list_sigma=[]
    c1=ROOT.TCanvas("cv_findped","cv_findped",1200,1000)
    cv_name="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/figs/find_ped_"+args.output+".pdf"
    c1.Print(cv_name + "[", "pdf")
    for i in range(64*4): #4 chips (64 chs for each chips)
       if i < 10: h_name= "hist00"+str(i)
       elif i < 100: h_name= "hist0"+str(i)
       else: h_name= "hist"+str(i)
       htemp=ROOT.TH1D(h_name+"_sum",h_name+"_sum",1024,-0.5,1023.5)
       for f_index in flist:
          fint=ROOT.TFile(f_index,"read")
          htemp.Add(fint.Get(h_name),1)
          fint.Close()
       htemp.GetXaxis().SetRangeUser(fit_range[0],fit_range[1]) # ped. range

       maxbin=htemp.GetXaxis().GetBinCenter(htemp.GetMaximumBin())
       maxhigh=htemp.GetBinContent(htemp.GetMaximumBin())
       func = ROOT.TF1("func","[0]*TMath::Exp(-0.5*pow(((x-[1])/[2]),2))",fit_range[0],fit_range[1])
       func.SetParLimits(0,0,maxhigh)
       func.FixParameter(1,maxbin)
       func.SetParLimits(2,0,20)
       
       htemp.Fit(func,"Q")
       list_sigma.append(func.GetParameter(2))
       htemp.Draw()
       c1.Print(cv_name, "pdf")
    c1.Print(cv_name + "]", "pdf")
    print("fitting ped. plots: {}".format(cv_name))
    return list_sigma

def find_ped_cmn(args):
    flist=GetInputList(args.inputFolder)
    fit_range=[-15,15] # range to check ped. position and sigma
    list_sigma=[]
    c1=ROOT.TCanvas("cv_findped","cv_findped",1200,1000)
    cv_name="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/figs/find_ped_"+args.output+"_cmn.pdf"
    c1.Print(cv_name + "[", "pdf")
    for i in range(64*4): #4 chips (64 chs for each chips)
       if i < 10: h_name= "hist_cmn00"+str(i)
       elif i < 100: h_name= "hist_cmn0"+str(i)
       else: h_name= "hist_cmn"+str(i)
       htemp=ROOT.TH1D(h_name+"_sum",h_name+"_sum",1024,-50.5,973.5)
       for f_index in flist:
          fint=ROOT.TFile(f_index,"read")
          htemp.Add(fint.Get(h_name),1)
          fint.Close()
       htemp.GetXaxis().SetRangeUser(fit_range[0],fit_range[1]) # ped. range
       list_sigma.append(htemp.GetRMS())
       htemp.Draw()
       c1.Print(cv_name, "pdf")
    c1.Print(cv_name + "]", "pdf")
    print("fitting ped. plots: {}".format(cv_name))
    return list_sigma

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("inputFolder", type=str, default="/Users/chiu.i-huan/Desktop/new_scientific/imageAna/data/testinput/", help="Input File Name")
    parser.add_argument("-o","--output", dest="output", type=str, default="machine2", help="Output File Name")
    args = parser.parse_args()

    #_sigma=find_ped(args)
    _sigma=find_ped_cmn(args)
    plt(_sigma)
