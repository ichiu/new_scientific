"""
This module is energy correction for 2 mm CdTe
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu.irs@osaka-u.ac.jp"
__created__   = "2022-06-09"
__copyright__ = "Copyright 2022 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

import sys,os,random,math,ROOT,argparse,yaml
sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/utils/')
from logger import log
import numpy as np
ROOT.gErrorIgnoreLevel = ROOT.kFatal
#ROOT.gROOT.SetBatch(1)

__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')

def get_yaml_conf(yamlfile):
    if not os.path.isfile(yamlfile):
        raise Exception("File {} not found. Please supply valid yaml input file.".format(yamlfile))
    with open(yamlfile) as f:
       data = yaml.load(f, Loader=yaml.FullLoader)
    return data

def gettree(inputFolder,_limit):
    fint = ROOT.TFile(inputFolder,"read")
    tint = fint.Get("tree")
    cut = "(nsignalx_lv2 == 1 && nsignaly_lv2 ==1)"
    cut += "&& (n_strips_pt_lv2[0] <= {0} && n_strips_al_lv2[0] <= {0})".format(_limit)
    cut += "&& (Entry$ < {})".format(20000000) #TODO short test (took ~ 40 mins)
    tout =  tint.CopyTree(cut)
    tout.SetDirectory(0)# leave from read-only file 
    return tout

def find_EPI1(epi2,E):
    EPI1_range=[]
    sigma=3 # pm 3 keV range
    if epi2 == 0: EPI1_range = [E-sigma, E+sigma]
    if epi2 < 0: EPI1_range = [(1.0858*epi2+E)-sigma, (1.0858*epi2+E)+sigma] # slopes of 2D plot
    if epi2 > 0: EPI1_range = [(-1.132*epi2+E)-sigma, (-1.132*epi2+E)+sigma]
    return EPI1_range

def mk_gr(args):
    log().info("Making Graph...")
    fout_name="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/auxfile/"+args.output+"_"+args.cdteid+".root"
    with open('./response/Eavg_plots/graph_x_{}.npy'.format(args.cdteid), 'rb') as f:
       _x=np.load(f)
    with open('./response/Eavg_plots/graph_y_{}.npy'.format(args.cdteid), 'rb') as f:
       _y=np.load(f)
    n_strips,n_epi2=len(_x), len(_x[0][0])
    fout = ROOT.TFile(fout_name,"recreate") 
    fout.cd()
    c1=ROOT.TCanvas("cv_mkresp","cv_mkresp",1200,1000)
    cv_name="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/figs/fig_response_"+args.cdteid+".pdf"
    c1.Print(cv_name + "[", "pdf")
    for ix in range(n_strips):
       for iy in range(n_strips):
          for iepi2 in range(n_epi2):
            not_zero=_x[ix][iy][iepi2]!=0 # find not empty
            x = _x[ix][iy][iepi2][not_zero]
            y = _y[not_zero]
            x = np.insert(x, 0, 0); y = np.insert(y, 0, 0); # return (0,0) point
            npoints=len(y)
            gr=ROOT.TGraph(npoints, x, y)
            gr.SetName("Graph_nxny{0}{1}_map{2}".format(ix+1,iy+1,iepi2))
            gr.SetTitle("Graph_nxny{0}{1}_map{2};epi1;energy".format(ix+1,iy+1,iepi2))
            gr.Write()
            gr.Draw("ALP")
            c1.Print(cv_name, "pdf")
            del gr
    c1.Print(cv_name + "]", "pdf")
    fout.Write()
    fout.Close()
    log().info("Output Path : {}".format(fout_name))
    log().info("Fig. Path : {}".format(cv_name))

def main(args):
    npeaks, nstrips_limit, epi2_bin, EPI2_R = 0, 2, 0.2, [-30,5]# default
    conf = get_yaml_conf(args.yamlfile)
    for prop, propv in conf.items():
       npeaks+=len(propv["Peaks"])
    log().info("Total %s Peaks"%(npeaks))
          
    weight=np.zeros((nstrips_limit,nstrips_limit,int((EPI2_R[1]-EPI2_R[0])/epi2_bin),npeaks+1),dtype=float)
    entry=np.zeros((nstrips_limit,nstrips_limit,int((EPI2_R[1]-EPI2_R[0])/epi2_bin),npeaks+1),dtype=float)
    graph_y_e=np.zeros(npeaks+1,dtype=float)
    for prop, propv in conf.items():
    
        log().info("Preparing {} File".format(prop))
        inputFolder = propv['Path'].replace("number",args.cdteid)
        peaks_list = propv['Peaks']
        tree=gettree(inputFolder,nstrips_limit)

        for i in range(tree.GetEntries()): 
           if i%50000 == 0: log().info("Events : %s / %s "%(i,tree.GetEntries()))
           tree.GetEntry(i)
           epi1, epi2 = (tree.E_pt_lv2[0]+tree.E_al_lv2[0])/2.,(tree.E_pt_lv2[0]-tree.E_al_lv2[0])/2.
           n_pt, n_al = tree.n_strips_pt_lv2[0]-1, tree.n_strips_al_lv2[0]-1 # index from zero for np.array
           if not (EPI2_R[0] < epi2 and epi2 < EPI2_R[1]): continue
           epi2_id=int((epi2-EPI2_R[0])/epi2_bin)
           for p_items in peaks_list:
              order = p_items["order"]
              mean_erengy = p_items["energy"]
              EPI1_R = find_EPI1(epi2,mean_erengy)
              if not (EPI1_R[0] < epi1 and epi1 < EPI1_R[1]): continue
              weight[n_pt][n_al][epi2_id][order]+=epi1
              entry[n_pt][n_al][epi2_id][order]+=1
              graph_y_e[order] = mean_erengy
    entry[entry==0]=1.#avoid zero divide
    graph_x_epi1=weight/entry
    with open('./response/Eavg_plots/graph_x_{}.npy'.format(args.cdteid), 'wb') as f:
         np.save(f, graph_x_epi1)
    with open('./response/Eavg_plots/graph_y_{}.npy'.format(args.cdteid), 'wb') as f:
         np.save(f, graph_y_e)
     
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("-o","--output", dest="output", type=str, default="epi1_epi2_map", help="Output File Name")
    parser.add_argument("-y", "--yamlfile", type=str, default="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/response/configs/energy_table.yaml", help="Yaml File Name")
    parser.add_argument("-i", "--cdteid", type=str, default="201", help="CdTe ID (201,202,203)")
    args = parser.parse_args()
    
#    main(args) # step-1
    mk_gr(args) # step-2
