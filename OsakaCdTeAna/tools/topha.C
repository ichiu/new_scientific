#include <iostream>
#include <fstream>
#include <sstream>

void topha(){
     
     TString filename="spectrum_sci.pha"; 
     //TFile* fint = new TFile("/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/20220611a_64-71_001_500n30.root","read");
     //TFile* fint = new TFile("/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/sum_jparc_500n30.root","read");
     TFile* fint = new TFile("/Users/chiu.i-huan/Desktop/OsakaCdTe_JPARC_June2022/nimo/SCI/sum_all.root","read");
     TTree* tint = (TTree*)fint->Get("tree");
     //tint->Draw("E_n_lv2 >> h1(5000,0,500)");
     TCut cut1 = "(((unixtime > 1583663336 && unixtime < 1583663640) || (unixtime > 1583665785 && unixtime < 1583668072) || (unixtime > 1583670126 && unixtime < 1583728926) || (unixtime > 1583797615 && unixtime < 1583807420) || (unixtime > 1583808902 && unixtime < 1583823904) || (unixtime > 1583825103 && unixtime < 1583837643) || (unixtime > 1583838416 && unixtime < 1583846500) || (unixtime > 1583847476 && unixtime < 1583872201))&&(((unixtime > 1583670785 && unixtime < 1583727074) && (int(((unixtime-1583670785)/1800)%31)==2))||((unixtime > 1583736900 && unixtime < 1583758670) && (int(((unixtime-1583736900)/1800)%31)==2))||((unixtime > 1583758670 && unixtime < 1583793160) && (int(((unixtime-1583758670)/1800)%31)==-8) && (2>=10))||((unixtime > 1583797637 && unixtime < 1583798147) && (2==0))||((unixtime > 1583798147 && unixtime < 1583802651) && (2==1))||((unixtime > 1583802651) && (int(((unixtime-1583802651)/4500)%15.0)==0.5) && (2%2==1) && (2>=3)))) || (((unixtime > 1583663336 && unixtime < 1583663640) || (unixtime > 1583665785 && unixtime < 1583668072) || (unixtime > 1583670126 && unixtime < 1583728926) || (unixtime > 1583797615 && unixtime < 1583807420) || (unixtime > 1583808902 && unixtime < 1583823904) || (unixtime > 1583825103 && unixtime < 1583837643) || (unixtime > 1583838416 && unixtime < 1583846500) || (unixtime > 1583847476 && unixtime < 1583872201))&&(((unixtime > 1583670785 && unixtime < 1583727074) && (int(((unixtime-1583670785)/1800)%31)==29))||((unixtime > 1583736900 && unixtime < 1583758670) && (int(((unixtime-1583736900)/1800)%31)==29))||((unixtime > 1583758670 && unixtime < 1583793160) && (int(((unixtime-1583758670)/1800)%31)==19) && (29>=10))||((unixtime > 1583797637 && unixtime < 1583798147) && (29==0))||((unixtime > 1583798147 && unixtime < 1583802651) && (29==1))||((unixtime > 1583802651) && (int(((unixtime-1583802651)/4500)%15.0)==14.0) && (29%2==1) && (29>=3))))";
     tint->Draw("E_n_lv2 >> h1(5000,0,500)",cut1,"");
     TH1D* h1 = (TH1D*)gDirectory->Get("h1");
     
     ofstream phaDataFile(filename.Data());
     TDatime now;
     phaDataFile << "# PHA DATA for J-PARC MUSE-2022-June created by I-Huan Chiu" << endl;
     phaDataFile << "# Date: " << now.AsSQLString() << endl;
     phaDataFile << "# Root file: " << filename.Data() << endl;
     phaDataFile << "# Time range (s): " << "10" << endl;
     phaDataFile << "# PHA data format: binID, energy(keV) at the bin center, content" << endl;
     Int_t nbin = h1->GetNbinsX();
     for (Int_t i = 1; i < nbin + 1; i++)
     {
         Double_t binCenter = h1->GetBinCenter(i);
         Double_t binContent = h1->GetBinContent(i);
         phaDataFile << i << ", " << binCenter << ", " << binContent << endl;
     }
     phaDataFile.close();

}

