import argparse,sys,os,random,math,ROOT,glob

def plt(args):
    f1=ROOT.TFile(args.input1,"read")
    f2=ROOT.TFile(args.input2,"read")
    t1=f1.Get("eventtree")
    t2=f2.Get("eventtree")
    finalentry = t2.GetEntries()
    t1.GetEntry(1)
    t2.GetEntry(finalentry-1)
    t1_start, t2_end = t1.unixtime, t2.unixtime
    print(t1_start,t2_end)
    time_range,chip_name=300,"adc3"
    t1.Draw("{} >> h1(1024,0,1024)".format(chip_name),"unixtime > {}&& unixtime < {}".format(t1_start,t1_start+time_range))
    t2.Draw("{} >> h2(1024,0,1024)".format(chip_name),"unixtime > {}&& unixtime < {}".format(t2_end-time_range,t2_end))
    h1,h2 = ROOT.gDirectory.Get("h1"), ROOT.gDirectory.Get("h2")
    h1.SetStats(0)
    h2.SetStats(0)
    h1.SetLineColor(2)
    h2.SetLineColor(4)

    c0 = ROOT.TCanvas("c0","c0",0,0,1200,800)
    ROOT.gPad.SetLogy(1) 
    h1.Draw("hist")
    h1.SetTitle("polarization; adc0; counts/5 mins") 
    h2.Draw("hist same")
    leg = ROOT.TLegend(.65,.58,.88,.88)
    leg.SetFillColor(0)
    leg.SetLineColor(0)
    leg.SetBorderSize(0)
    leg.AddEntry(h1,  "start", "l")
    leg.AddEntry(h2,  "end", "l")
    leg.Draw("same") 
    c0.Print("/Users/chiu.i-huan/Desktop/temp_output/check_polarization_{}.pdf".format(chip_name))
       
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("-i","--input1", dest="input1", type=str, default="test1", help="Input File Name")
    parser.add_argument("-f","--input2", dest="input2", type=str, default="test2", help="Input File Name")
    args = parser.parse_args()

    plt(args)
