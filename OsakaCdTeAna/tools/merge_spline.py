#!/usr/bin/env python    
#-*- coding:utf-8 -*-   
"""
This module provides the transformation from adc to energy.
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu.irs@osaka-u.ac.jp"
__created__   = "2022-07-06"
__copyright__ = "Copyright 2022 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"


import sys,os,random,math,ROOT,argparse
from ROOT import TFile, TTree, gPad, TGraphAsymmErrors, TSpline, TSpline3, TSpline5, gStyle, gErrorIgnoreLevel, gROOT
import numpy as np
ROOT.gROOT.SetBatch(1)
ROOT.gErrorIgnoreLevel = ROOT.kWarning
#kPrint, kInfo, kWarning, kError, kBreak, kSysError, kFatal;
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')

def getLatex(ch, x = 0.85, y = 0.85):
    _t = ROOT.TLatex()
    _t.SetNDC()
    _t.SetTextFont( 62 )
    _t.SetTextColor( 36 )
    _t.SetTextSize( 0.08 )
    _t.SetTextAlign( 12 )
    return _t

def compare(_outputname, graph, fline, spline):   
    ROOT.SetAtlasStyle()
    c0name="/Users/chiu.i-huan/Desktop/temp_output/"+_outputname+"/comparison_all.pdf" 
    c0 = ROOT.TCanvas(c0name,"",0,0,1600,800)
    c0.Divide(2,1)
    c0.Print(c0name + "[", "pdf")
    for ich in range(128):
       for side in range(2):
          c0.cd(side+1)
          i=ich+side*128
          linename = "fline_"+str(i) 
          graph[i].SetLineColorAlpha(4,0.7) 
          graph[i].SetMarkerColor(4) 
          fline[i].SetLineColorAlpha(2,0.95) 
          fline[i].SetLineWidth(1) 
          spline[i].SetLineColorAlpha(8,0.95) 
          spline[i].SetLineWidth(1) 
          fline[i].Draw()
          spline[i].Draw("same")
          graph[i].Draw("* same")

          leg = ROOT.TLegend(.55,.18,.75,.40)
          leg.SetFillColor(0)
          leg.SetLineColor(0)
          leg.SetBorderSize(0)
          leg.AddEntry(graph[i],  "graph", "p")
          leg.AddEntry(spline[i],  "spline", "l")
          leg.AddEntry(fline[i],  "fline", "l")
          leg.Draw("same")

          latex = getLatex(i,400,8000) 
          Latex_name="Ch : {}".format(i)
          latex.DrawLatex(0.25,0.85,Latex_name)
       c0.Print(c0name, "pdf")
    c0.Print(c0name + "]", "pdf")
    print("comparison : ", c0name)

def merge(args):
    graph_list, spline_list, fline_list=[],[],[]
    useCoHight = True
    fa, fb, fc, fe = ROOT.TFile(), ROOT.TFile(), ROOT.TFile(), ROOT.TFile()
    fout=ROOT.TFile("./files_cali/spline_calibration_"+args.outname+"_merge.root","recreate")
    if args.outname == "cdte201" or args.outname == "cdte202" or args.outname == "cdte203":
       #fa=ROOT.TFile("files_cali/spline_calibration_"+args.outname+"_Am.root","read")
       fb=ROOT.TFile("files_cali/spline_calibration_"+args.outname+"_Ba.root","read")
       fc=ROOT.TFile("files_cali/spline_calibration_"+args.outname+"_Co.root","read")
    if args.outname == "cdte204":
       fa=ROOT.TFile("files_cali/spline_calibration_"+args.outname+"_Am.root","read")
       fe=ROOT.TFile("files_cali/spline_calibration_"+args.outname+"_Eu.root","read")
    
    fout.cd()
    for i in range(256):
       graph_name="graph_"+str(i)
       _gb=fb.Get(graph_name)
       _gc=fc.Get(graph_name)
       _ga=fa.Get(graph_name)
       _ge=fe.Get(graph_name)
       # === NOTE check fitting plots & adc range (adc, energy) ===     
       # === check if there is zero point in _gb and _gc  === 
       # === check if the energy peaks are exist in energy table or not === 
       # === please focus on the number of peaks you used. (Am,Ba,Co)=(5,3,2) for p-side, (3,2,3) for n-side === 

       if args.outname == "cdte201" or args.outname == "cdte202" or args.outname == "cdte203":
          if i < 128:#p-side
             #xp = np.array([0,_gc.GetPointX(1),_gb.GetPointX(1),_gb.GetPointX(2),_gb.GetPointX(3),_gc.GetPointX(2),_gb.GetPointX(4)]) # Co14, Ba30, Ba35, Ba81, Co122, Ba356
             #yp = np.array([0, _gc.GetPointY(1),_gb.GetPointY(1), _gb.GetPointY(2), _gb.GetPointY(3), _gc.GetPointY(2),_gb.GetPointY(4)])
             xp = np.array([0,_gb.GetPointX(1),_gb.GetPointX(2),_gb.GetPointX(3),_gc.GetPointX(2),_gb.GetPointX(4)]) # Ba30, Ba35, Ba81, Co122, Ba356
             yp = np.array([0,_gb.GetPointY(1), _gb.GetPointY(2), _gb.GetPointY(3), _gc.GetPointY(2),_gb.GetPointY(4)])
          else:#n-side
             #xp = np.array([0,_gb.GetPointX(1),_gb.GetPointX(2),_gb.GetPointX(3),_gc.GetPointX(1),_gc.GetPointX(2),_gb.GetPointX(4),_gb.GetPointX(5)]) # Ba30, Ba35, Ba81, Co122, Co136, Ba303, Ba356
             #yp = np.array([0,_gb.GetPointY(1),_gb.GetPointY(2),_gb.GetPointY(3),_gc.GetPointY(1),_gc.GetPointY(2),_gb.GetPointY(4),_gb.GetPointY(5)])
             xp = np.array([0,_gb.GetPointX(1),_gb.GetPointX(2),_gb.GetPointX(3),_gc.GetPointX(1),_gc.GetPointX(2)]) # Ba30, Ba35, Ba81, Co122, Co136
             yp = np.array([0,_gb.GetPointY(1),_gb.GetPointY(2),_gb.GetPointY(3),_gc.GetPointY(1),_gc.GetPointY(2)])
       if args.outname == "cdte204":
          xp = np.array([0,_ga.GetPointX(1),_ge.GetPointX(1),_ga.GetPointX(2),_ge.GetPointX(2)]) # Am18, Eu40, Am60, Eu122
          yp = np.array([0,_ga.GetPointY(1),_ge.GetPointY(1),_ga.GetPointY(2),_ge.GetPointY(2)])
       
       npoints=len(xp)
       _g = ROOT.TGraph(npoints,xp,yp)
       _g.SetName(graph_name)

       #reset Zero and Final points
       x1,y1,x2,y2 = _g.GetPointX(1), _g.GetPointY(1), _g.GetPointX(2), _g.GetPointY(2)
       xf1,yf1,xf2,yf2 = _g.GetPointX(npoints-2), _g.GetPointY(npoints-2), _g.GetPointX(npoints-1), _g.GetPointY(npoints-1)
       slope_z,slope_f=(y2-y1)/(x2-x1), (yf2-yf1)/(xf2-xf1)
       _g.SetPoint(0, 0, y1-(x1-0)*slope_z)
       _g.SetPoint(npoints, 1024,  (1024-xf2)*slope_f + yf2)

       _s = ROOT.TSpline3("spline_"+str(i), _g)
       _s.SetName("spline_"+str(i))
       _f = ROOT.TF1("fline_"+str(i),"pol1",0,1024)
       _g.Fit("fline_"+str(i),"qn")# get pol1 function passed (0,0) point

       _g.Write();_f.Write();_s.Write()
       graph_list.append(_g)
       fline_list.append(_f)
       spline_list.append(_s)
       del _g,_f,_s
    fout.Write()
    fout.Close()
    print("outfile : ./files_cali/spline_calibration_"+args.outname+"_merge.root")
    compare(args.outname,graph_list,fline_list,spline_list)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("-o","--outname", dest="outname", type=str, default="cdte202", help="Name of Output File from Calibration")
    args = parser.parse_args()
    merge(args)
