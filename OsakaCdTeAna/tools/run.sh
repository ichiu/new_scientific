# ===================
# = Peds. Threshold =
# ===================
#python find_ped_thre.py /Volumes/HD-PCFSU3-A/OsakaCdTe_calidata_June2022/cdte201/Co -o cdte201
#python find_ped_thre.py /Volumes/HD-PCFSU3-A/OsakaCdTe_calidata_June2022/cdte202/Ba -o cdte202
#python find_ped_thre.py /Volumes/HD-PCFSU3-A/OsakaCdTe_calidata_June2022/cdte203/Co -o cdte203
#python find_ped_thre.py /Volumes/HD-PCFSU3-A/OsakaCdTe_RICenter_calidata_Dec2022/cdte204/Eu -o cdte204

#python find_ped_thre.py /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/data/OsakaCdTe_JPARC_Jan2023/cdte201/data20230121_cdte201_3_00047_001.root -o cdte201
#python find_ped_thre.py /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/data/OsakaCdTe_JPARC_Jan2023/cdte202/data20230121_cdte202_3_00047_001.root -o cdte202
#python find_ped_thre.py /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/data/OsakaCdTe_JPARC_Jan2023/cdte203/data20230121_cdte203_3_00047_001.root -o cdte203
#python find_ped_thre.py /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/data/OsakaCdTe_JPARC_Jan2023/cdte204/data20230121_cdte204_3_00047_001.root -o cdte204


# ===================
# === Calibration ===
# ===================
#python calib.py -i /Volumes/HD-PCFSU3-A/OsakaCdTe_calidata_June2022 -d cdte201 -s Co
#python calib.py -i /Volumes/HD-PCFSU3-A/OsakaCdTe_calidata_June2022 -d cdte202 -s Co
#python calib.py -i /Volumes/HD-PCFSU3-A/OsakaCdTe_calidata_June2022 -d cdte203 -s Co
#python calib.py -i /Volumes/HD-PCFSU3-A/OsakaCdTe_calidata_June2022 -d cdte201 -s Ba 
#python calib.py -i /Volumes/HD-PCFSU3-A/OsakaCdTe_calidata_June2022 -d cdte202 -s Ba 
#python calib.py -i /Volumes/HD-PCFSU3-A/OsakaCdTe_calidata_June2022 -d cdte203 -s Ba 
#python calib.py -i /Volumes/HD-PCFSU3-A/OsakaCdTe_RICenter_calidata_Dec2022 -d cdte204 -s Eu 
#python calib.py -i /Volumes/HD-PCFSU3-A/OsakaCdTe_RICenter_calidata_Dec2022 -d cdte204 -s Am 
#python merge_spline.py -o cdte201
#python merge_spline.py -o cdte202
#python merge_spline.py -o cdte203
#python merge_spline.py -o cdte204 

# 1. make epimap file 
# 2. check /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/response/configs/energy_table.yaml
# ===================
# === mk Response ===
# ===================
#python mk_rep.py -i 201 
#python mk_rep.py -i 202 
#python mk_rep.py -i 203 
#python mk_rep.py -i 204 


# ===================
# ===    merge    ===
# ===================
cd /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/auxfile
rm database_201.root database_202.root database_203.root database_204.root
hadd database_201.root epi1_epi2_map_201.root ped_thre_cdte201_cmn.root spline_calibration_cdte201_merge.root
hadd database_202.root epi1_epi2_map_202.root ped_thre_cdte202_cmn.root spline_calibration_cdte202_merge.root
hadd database_203.root epi1_epi2_map_203.root ped_thre_cdte203_cmn.root spline_calibration_cdte203_merge.root
hadd database_204.root epi1_epi2_map_204.root ped_thre_cdte204_cmn.root spline_calibration_cdte204_merge.root
cd -
