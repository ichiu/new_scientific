# === example for running ===

# *** make matrix ***
#python3 mksr.py --imageinput /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_rutsubo/rotation_image_30MeV.root -o mkmatrix -i root/Output_4.35M_resp3.root -m "redo" 

# *** sim. test ***
#python3 mksr.py --imageinput ./root/rotation_image_sim_point.root -o Sim_point_0303 -n 4 -l 50 -sub 1 -t osem -m "matrix_response_sim.npy"
#python3 mksr.py --imageinput ./root/rotation_image_sim_line.root -o Sim_line_0303 -n 4 -l 50 -sub 1 -t osem -m "matrix_response_sim.npy"
#python3 mksr.py --imageinput ./root/rotation_image_sim_circle.root -o Sim_circle_0303 -n 4 -l 50 -sub 1 -t osem -m "matrix_response_sim.npy"

# *** Co data ***
#python3 mksr.py --imageinput /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_Co/rotation_image_Co.root -n 4 -o Co_0308_all_move2_mlem -l 30 -sub 1 -t mlem 
#python3 mksr.py --imageinput /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_Co/rotation_image_Co.root -n 4 -o Co_0308_all_move2_osem -l 30 -sub 1 -t osem -sub 1 

# *** image data ***
#python3 mksr.py --imageinput /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_rutsubo/rotation_image_30MeV.root -n 12 -o O_30MeV_0309_resp3_move_mlem_cut_all -l 50 -t mlem
#python3 mksr.py --imageinput /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_rutsubo/rotation_image_30MeV_Si.root -n 12 -o Si_30MeV_0309_resp3_move_mlem_cut_all -l 50 -t mlem
#python3 mksr.py --imageinput /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_rutsubo/rotation_image_25MeV.root -n 12 -o O_25MeV_0309_resp3_move_mlem_cut_all -l 50 -t mlem
#python3 mksr.py --imageinput /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_rutsubo/rotation_image_25MeV_Si.root -n 12 -o Si_25MeV_0309_resp3_move_mlem_cut_all -l 50 -t mlem

#python3 mksr.py --imageinput /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_rutsubo/rotation_image_sum.root -n 12 -o O_sum_0315_remove78 -l 50 -t mlem -m "matrix_response_rebin4.npy"
python3 mksr.py --imageinput /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_rutsubo/rotation_image_sum_Si.root -n 12 -o Si_sum_0315_remove78 -l 50 -t mlem -m "matrix_response_rebin4.npy"

#python3 mksr.py --imageinput /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/root/image_rutsubo/rotation_image_30MeV.root -n 12 -o O_30MeV_0309_resp3_move_osem_nocut -l 10 -t osem -sub 1
