import sys,os,ROOT,time
sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/imageAna/macro/utils/')
sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/imageAna/macro/')
from ROOT import gSystem, gPad, gDirectory, gStyle
from helpers import createRatioCanvas, ProgressBar
from logger import log
from root_numpy import hist2array, array2hist, tree2array
import numpy as np
import vtk
from vtk.util import numpy_support
import itkwidgets
from vedo import *
from vedo.applications import IsosurfaceBrowser
from vedo.applications import SlicerPlotter
from scipy import ndimage, misc
ROOT.gErrorIgnoreLevel = ROOT.kWarning
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))

#final paper used
inputfile, outvideoname="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/output/myMLEMoutput_Co_0308_all_move2_mlem_iteration30.root", "Co2"
#inputfile, outvideoname="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/output/myMLEMoutput_Sim_point_0303_iteration50.root", "point"
#inputfile, outvideoname="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/output/myMLEMoutput_Sim_line_0303_iteration50.root", "line"
#inputfile, outvideoname="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/output/myMLEMoutput_Sim_circle_0303_iteration50.root", "circle"

#inputfile, outvideoname="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/output/myMLEMoutput_O_30MeV_0309_resp2_move_mlem_nocut_iteration10.root", "rutubo"
#inputfile, outvideoname="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/output/myMLEMoutput_O_30MeV_0309_resp2_move_mlem_cut_iteration10.root", "rutubo"
#inputfile, outvideoname="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/output/myMLEMoutput_O_30MeV_0309_resp3_move_mlem_cut_all_iteration20.root", "rutubo"
#inputfile, outvideoname="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/output/myMLEMoutput_O_30MeV_0309_resp3_move_mlem_nocut_all_iteration20.root", "rutubo"
#inputfile, outvideoname="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/output/myMLEMoutput_O_30MeV_0309_resp3_move_mlem_cut_all_iteration50.root", "rutubo"
#inputfile, outvideoname="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/output/myMLEMoutput_Si_30MeV_0309_resp3_move_mlem_cut_all_iteration50.root", "rutubo"
#inputfile, outvideoname="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/output/myMLEMoutput_O_25MeV_0309_resp3_move_mlem_cut_all_iteration50.root", "rutubo"
#inputfile, outvideoname="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/output/myMLEMoutput_O_30MeV_0311_resp3_move_mlem_cut_all_iteration50.root", "rutubo"
#inputfile, outvideoname="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/output/myMLEMoutput_O_sum_0315_remove78_iteration50.root", "rutubo_O"
#inputfile, outvideoname="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/tools/mlem/output/myMLEMoutput_Si_sum_0315_remove78_iteration50.root", "rutubo_Si"
plotname="MLEM_3Dimage"
#plotname="MLEM_3Dimage_iteration30"# paper used

#MC used
#inputfile="./root/Output_sample_line.root"
#inputfile="./root/Output_sample_circle.root"
#plotname="h3_sample"# paper used

if __name__=="__main__":
   # === NOTE: TH3D (X,Y,Z) -> numpy -> VEDO (Z,Y,X) ===
   f_mlem=ROOT.TFile(inputfile,"read")
   h3=f_mlem.Get(plotname)
   matrix=hist2array(h3)
   with open('3Dplot_mlem.npy', 'wb') as f:
      np.save(f, matrix)
   matrix = np.transpose(matrix, axes=[2, 1, 0])

   # === cut matrix ===
   #_where=np.where(matrix>80)
   #matrix[_where]=0
   #_where2=np.where(matrix<0.1)
   #matrix[_where2]=0

   # === Rotation (Paper) ===
   #_angle=180
   #matrix=ndimage.rotate(matrix,_angle,axes=(1,2),reshape=False)   
   # === Rotation (MC) ===
   #_angle=180
   #matrix=ndimage.rotate(matrix,_angle,axes=(1,2),reshape=False)   

   # === VEDO ===
   # *** Samples ***
   init_X=20
   init_Y=20
   init_Z=12
   diff_Big=5
   shift_small=0.2
   #setting
   #poi_big1  =(init_X+7.07,init_Y+7.07,init_Z+12.7-diff_Big)
   #poi_big2  =(init_X-7.07,init_Y-7.07,init_Z)
   #poi_small1=(init_X+7.07,init_Y-7.07,init_Z+6.35-diff_Big+6.35-6.35/2)
   #poi_small2=(init_X-7.07,init_Y+7.07,init_Z+12.7+6.35-diff_Big-6.35/2)
   # from photo
   poi_big1  =(init_X+7.07,init_Y+7.07,init_Z+12.7-diff_Big)
   poi_big2  =(init_X-7.07,init_Y-7.07,init_Z)
   poi_small1=(init_X+7.07,init_Y-7.07,init_Z+6.35-diff_Big+6.35-6.35/2)
   poi_small2=(init_X-7.07,init_Y+7.07,init_Z+12.7+6.35-diff_Big-6.35/2-3)

   s1 = Sphere(c="white",pos=poi_big1, r=12.7/2,alpha=0.5, res=12).wireframe()
   s2 = Sphere(c="white",pos=poi_big2, r=12.7/2,alpha=0.5, res=12).wireframe()
   s3 = Sphere(c="white",pos=poi_small1, r=12.7/4,alpha=0.5, res=12).wireframe()
   s4 = Sphere(c="white",pos=poi_small2, r=12.7/4,alpha=0.5, res=12).wireframe()

   # *** color volume ***
   #vol = Volume(matrix).alpha([0.0, 0.0, 0.2, 0.25, 0.4, 0.4]).c('blue')# paper used 
   #vol = Volume(matrix).alpha([0.0, 0.0, 0.2, 0.2, 0.4, 0.4]).c('blue')# MC test
   #vol = Volume(matrix).alpha([0.0, 0.1, 0.2 ,0.3, 0.38, 0.55, 0.8]).c('lime')# default
   #vol = Volume(matrix).alpha([0.0, 0.45,0.5, 0.51, 0.52 ,0.6, 0.65, 0.7, 0.75]).c('lime')# default
   vol = Volume(matrix).alpha([0.0, 0.43, 0.45,0.5, 0.55, 0.60 ,0.65, 0.70, 0.75, 0.80]).c(['white','b','g','r'])# default
   # *** color volume ***
   #vol = Volume(matrix, c=['white','b','g','r'])

   vol.addScalarBar3D()

   #  === Co ===
   slices = []
   for i in range(1,2):
       sl = vol.slicePlane(origin=[10,18+i*2,10], normal=(0,1,0))
       slices.append(sl)
   amap = [0.1, 0.8, 1, 1, 1]  # hide low value points giving them alpha 0

   #  === Rutubo ===
   slices = []
   for i in range(1,2):
       sl = vol.slicePlane(origin=[10,18+i*2,10], normal=(0,1,0))
       slices.append(sl)
   amap = [0.1, 0.8, 1, 1, 1]  # hide low value points giving them alpha 0

   mslices = merge(slices) # merge all slices into a single Mesh
   mslices.cmap("CMRmap", alpha=amap).lighting('off').addScalarBar(title='Slice',pos=(0.65, 0.05),size=(100,350))# or gist_ncar_r
#   mslices.cmap("jet_r", alpha=amap).lighting('off').addScalarBar(title='Slice',pos=(0.65, 0.05),size=(100,350))# or gist_ncar_r
#   mslices.cmap("Spectral", alpha=amap).lighting('off').addScalarBar(title='Slice',pos=(0.65, 0.05),size=(100,350))# or gist_ncar_r

   # == check ==
   #plt = SlicerPlotter( vol, bg='white', bg2='lightblue', cmaps=("gist_ncar_r","jet_r","Spectral_r","hot_r","bone_r"),useSlider3D=False,)
   #plt = IsosurfaceBrowser(vol, c='gold') # Plotter instance
   #show(vol,mslices,__doc__, axes=1)
   show(vol,__doc__, axes=1)

   # == mk paper plot ==
#   cam = dict(pos=(-60, 80, 100),
#           focalPoint=(15, 15, 15),
#           distance=100.94)
#   cam2 = dict(pos=(10, 120, 25),
#           focalPoint=(15, 15, 15),
#           distance=100)
#   plt=show(s1,s2,s3,s4, vol, mslices, __doc__, axes=1,camera=cam,interactive=False)#no interactive
#   plt=show(s1,s2,s3,s4, vol, __doc__, axes=1,camera=cam,interactive=False)#no interactive
#   plt=show(vol, __doc__, axes=1,camera=cam,interactive=False)#no interactive
#   io.screenshot(filename="./output/vedo3Dplot_1.png")
#   plt2=show(s1,s2,s3,s4, vol,mslices, __doc__, axes=1,camera=cam2,interactive=False)#no interactive
#   io.screenshot(filename="/Users/chiu.i-huan/Desktop/vedo3Dplot_2.png")

   # == mk video ==
   plt = Plotter(axes=1, offscreen=True)
   video = Video("video_rot_{}.mov".format(outvideoname), duration=24,backend='opencv')
   _angle=2
   _inver,to_angle=-1,0
   for i in range(int(360/_angle)):
      plt.camera.Azimuth(-_angle)
      #plt.show(vol,mslices)
      plt.show(vol)
      video.addFrame()
   for i in range(int((170)/_angle)):
      #to_angle+=_angle
      if i >= int((170/2.)/_angle): _inver=1
      plt.camera.Elevation(_inver*_angle*(-1))
      #plt.show(vol,mslices)
      plt.show(vol)
      video.addFrame()
   for i in range(int((170)/_angle)):
      #to_angle+=_angle
      if i >= int((170/2.)/_angle): _inver=-1
      plt.camera.Elevation(_inver*_angle*(-1))
      #plt.show(vol,mslices)
      plt.show(vol)
      video.addFrame()
   video.close()

