"""
This module makes the 2d plots for mlem.
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu@rirc.osaka-u.ac.jp"
__created__   = "2023-02-11"
__copyright__ = "Copyright 2023 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

import sys,os,random,math,ROOT,argparse,yaml
ROOT.gErrorIgnoreLevel = ROOT.kFatal
ROOT.gROOT.SetBatch(1)
def getLatex():
    _t = ROOT.TLatex()
    _t.SetNDC()
    _t.SetTextFont( 62 )
    _t.SetTextColor( 36 )
    _t.SetTextSize( 0.08 )
    _t.SetTextAlign( 12 )
    return _t

def check_2d_low():
#   fout = ROOT.TFile("./root/output_response_2dplots.root","recreate")
#   tree = fint.Get("tree")
#   fout.cd()
   for iy in range(npoints): 
      for ix in range(npoints): 
         for iz in range(npoints):
            #xcut, ycut, zcut = -15 + 5*ix, -15 + 5*iy, -15 + 5*iz
            #tree.Draw("Hit_Start_X:Hit_Start_Z >> h2(128,-16,16,128,-16,16)","pInitX == {} && pInitY == {} && pInitZ == {}".format(xcut, ycut, zcut),"colz") 
            #h2=ROOT.gDirectory.Get("h2")
            #h2.SetDirectory(0)
            #h2.SetTitle(";X [mm]; Y [mm]")
            #h2.SetName("imafe_pos{}".format(index))
            #h2.Write()
            print("aa")

def check_2d():
   fint = ROOT.TFile("./root/Output_1.9M_point.root","read")
   npoints = 31
   index=0
   cv_name="/Users/chiu.i-huan/Desktop/temp_output/fig_mlem_response_all2d.pdf" 
   cv = ROOT.TCanvas("test","test",1200,1200)
   cv.Print(cv_name + "[", "pdf")
   h3 = ROOT.TH3D("h3","h3",32,0,32,32,0,32,32,0,32)
   for iy in range(npoints): 
      for ix in range(npoints): 
         for iz in range(npoints):
            print("index : {}".format(index))
            latex = getLatex()
            h1 = fint.Get("image_X{}_Y{}_Z{}".format(ix,iy,iz))
            h1.SetDirectory(0)
            h1.SetStats(0)
            h1.SetTitle(";X [mm]; Y [mm]")
            h1.Draw("colz")
            latex.DrawLatex(0.5,0.25,"X{}, Y{}, Z{}".format(ix,iy,iz))
            if h1.GetEntries() == 0: 
               #print("(X, Y, Z) = ({}, {}, {})".format(ix,iy,iz))
               h3.Fill(ix,iy,iz)
            index += 1
            cv.Print(cv_name, "pdf")
   cv.Print(cv_name + "]", "pdf")
   fout = ROOT.TFile("/Users/chiu.i-huan/Desktop/temp_output/output_h3_hit.root","recreate")
   fout.cd()
   h3.Write()

if __name__ == "__main__":
   check_2d() 
