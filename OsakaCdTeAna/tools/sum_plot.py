#!/usr/bin/env python    
#-*- coding:utf-8 -*-   
"""
This module make sum plots for 4 CdTe-DSD
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu@rirc.osaka-u.ac.jp"
__created__   = "2023-01-16"
__copyright__ = "Copyright 2023 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

# modules
import sys,os,random,math,ROOT
from ROOT import TFile, TTree, gROOT, gStyle, TCut, gPad, gDirectory
ROOT.gROOT.SetBatch(1)
import argparse
sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/imageAna/macro/utils/')
#gROOT.ProcessLine("gErrorIgnoreLevel = kPrint, kInfo, kWarning, kError, kBreak, kSysError, kFatal;")
ROOT.gErrorIgnoreLevel = ROOT.kWarning

from logger import log, supports_color
from helpers import GetTChain, createRatioCanvas
from color import SetMyPalette
import enums

__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
#ROOT.SetAtlasStyle()

#from scipy.spatial.transform import Rotation as R
import numpy as np

def mk_image(args):
    det_list = ["201","202","203","204"]
    cv  = createRatioCanvas("cv", 1800, 1600)
    cv.Divide(2,2)
    for i, _det in enumerate(det_list):
       c0  = createRatioCanvas("c0", 1800, 1600)
       _tree = GetTChain(args.inputFolder,"tree")
       _tree.Draw("y:x >> h2(128,-16,16, 128,-16,16,)","","colz")
       h2=ROOT.gDirectory.Get("h2")

       h2.SetTitle("; Pt side X [mm]; Al side Y [mm]")
       h2.GetXaxis().CenterTitle(1)
       h2.GetYaxis().CenterTitle(1)
       h2.SetStats(0)
       h2.SetDirectory(0)

       cv.cd(i)
       ROOT.gPad.SetLogz(0)
       ROOT.gStyle.SetPalette(56)
       ROOT.gPad.SetRightMargin(0.15)
       h2.Draw("colz")
    cv.Print("/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/figs/sum_plots_image.pdf") 
    print("Output images: /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/run/figs/sum_plots_image.pdf")
       
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("-i", "--inputFolder", type=str, default="/Users/chiu.i-huan/Desktop/new_scientific/imageAna/run/root/CdTe_root/", help="Input Ntuple Name")
    args = parser.parse_args()
     
    mk_image( args )
