#!/usr/bin/env python    
#-*- coding:utf-8 -*-   
"""
This module provides the transformation from adc to energy.
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu.irs@osaka-u.ac.jp"
__created__   = "2022-06-09"
__copyright__ = "Copyright 2022 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

# modules
import sys,os,random,math,ROOT,argparse,time,glob
from ROOT import TFile, TTree, gPad, TGraphAsymmErrors, TSpline3, gStyle, gErrorIgnoreLevel, gROOT
ROOT.gROOT.SetBatch(1)
from array import array
#from logger import log
from random import gauss
import linecache

def getLatex(ch, x = 0.85, y = 0.85):
    _t = ROOT.TLatex()
    _t.SetNDC()
    _t.SetTextFont( 62 )
    _t.SetTextColor( 36 )
    _t.SetTextSize( 0.08 )
    _t.SetTextAlign( 12 )
    return _t


class Calibration():
      def __init__(self,filename=None,output=None,Etable=None,source=None):
          self.filename=filename
          self.output=output
          self.Etable=Etable
          self.source=source
          self.hist_list,self.name_list=self.gethist()
          self.list_chip_1,self.list_chip_2,self.list_chip_3,self.list_chip_4=self.getrange()
          self.fit_array=[]
          self.fit_result=self.fit("weight")
          #self.fit_result=self.fit("gaus")
          self.graph_list,self.spline_list,self.fline_list=self.mkTSpline()

      def gethist(self):
          inputDict = list()
          hist_list,name_list=[],[]
          if ".root" in self.filename:     
             inputDict.append(self.filename)
          else:
             subFolders = glob.glob(self.filename+"/*.root")
             for subFolder in subFolders:
                inputDict.append(subFolder)

          for _ifname in range(len(inputDict)):
             f = ROOT.TFile(inputDict[_ifname]) 
             for i in range(256):
                if i < 10: hist_name = "hist_cmn" + "00" + str(i) 
                elif i < 100:  hist_name = "hist_cmn" + "0" + str(i) 
                else : hist_name = "hist_cmn" + str(i)
                _h = f.Get(hist_name).Clone()
                _h.SetDirectory(0) 
                if _ifname == 0:
                   hist_list.append(_h)
                   name_list.append(hist_name)            
                else:
                   hist_list[i].Add(_h)
          return hist_list, name_list

      def getrange(self):
          list_chip_1, list_chip_2, list_chip_3, list_chip_4 = [],[],[],[]
          table_1,table_2,table_3,table_4=self.Etable.replace(".txt","_chip1.txt"), self.Etable.replace(".txt","_chip2.txt"), self.Etable.replace(".txt","_chip3.txt"), self.Etable.replace(".txt","_chip4.txt")
          _l1=linecache.getlines(table_1) 
          _l2=linecache.getlines(table_2) 
          _l3=linecache.getlines(table_3) 
          _l4=linecache.getlines(table_4) 
          for _il in range(len(_l1)):
             _e=_l1[_il].strip().split(' ')
             if "#" in _e[0]: continue
             if _e[0] == self.source: list_chip_1.append(_e)  
          for _il in range(len(_l2)):
             _e=_l2[_il].strip().split(' ')
             if "#" in _e[0]: continue
             if _e[0] == self.source: list_chip_2.append(_e)  
          for _il in range(len(_l3)):
             _e=_l3[_il].strip().split(' ')
             if "#" in _e[0]: continue
             if _e[0] == self.source: list_chip_3.append(_e)  
          for _il in range(len(_l4)):
             _e=_l4[_il].strip().split(' ')
             if "#" in _e[0]: continue
             if _e[0] == self.source: list_chip_4.append(_e)  
          return list_chip_1,list_chip_2,list_chip_3,list_chip_4

      def fit(self,fittype):
          fit_result=[]
          ich=0
          for ih in self.hist_list:
             if ich < 64: element_list=self.list_chip_1
             elif ich < 128: element_list=self.list_chip_2
             elif ich < 192: element_list=self.list_chip_3
             elif ich < 256: element_list=self.list_chip_4
             dic,arrow,n_fit={},[],0
             for ifit in element_list:
                harrow = ROOT.TArrow()
                #harrow.SetDirectory(0)
                E_down, E_up=float(ifit[2]), float(ifit[3])
                E_down, E_up=E_down+50, E_up+50 # Energy -> bin number (Minimum bin number 0 is energy = -50)
                _poix, _sumw, _w, _maxbin = 0.,0.,0.,-1.
                if fittype == "gaus": 
                   for ibin in range(int(E_down),int(E_up+1)):
                      if ih.GetBinContent(ibin) > _maxbin: _maxbin = ih.GetBinContent(ibin)
                   g1=ROOT.TF1("g1","gaus",E_down,E_up)
                   g1.SetLineColor(ROOT.kRed)
                   #g1.SetLineColorAlpha(0,0)
                   if n_fit == 0: ih.Fit("g1","QR")
                   else: ih.Fit("g1","QR+")
                   _poix=g1.GetParameter(1)
                   harrow.SetX1(_poix); harrow.SetX2(_poix); 
                   harrow.SetY1(_maxbin); harrow.SetY2(0); harrow.SetDrawOption(">"); harrow.SetArrowSize(0.95)
                   arrow.append(harrow)                   
                   dic.update({ifit[1]:g1.GetParameter(1)})                
                   n_fit+=1   
                else:
                   for ibin in range(int(E_down),int(E_up+1)):
                      _sumw+=ih.GetBinCenter(ibin)*ih.GetBinContent(ibin)
                      _w+=ih.GetBinContent(ibin)
                      if ih.GetBinContent(ibin) > _maxbin: _maxbin = ih.GetBinContent(ibin)
                   if _w == 0: _poix = (E_down+E_up)/2. # no count
                   else: _poix=_sumw/_w   
                   harrow.SetX1(_poix); harrow.SetX2(_poix); 
                   harrow.SetY1(_maxbin); harrow.SetY2(0); harrow.SetDrawOption(">"); harrow.SetArrowSize(0.95)
                   arrow.append(harrow)                   
                   dic.update({ifit[1]:_poix})
             self.fit_array.append(arrow)
             fit_result.append(dic)
             ich+=1
          return fit_result # includes list of dic[energy, adc]

      def mkTSpline(self):
          graph_list,spline_list,fline_list=[],[],[]
          for i in range(len(self.hist_list)):     
             _g = ROOT.TGraph()
             _s = ROOT.TSpline3()
             _ln=ROOT.TF1("func_{}".format(i),"pol1",0,1024)
             #_ln.FixParameter(0,0)
             _ln.SetLineWidth(1)

             _g.SetName("graph_"+self.name_list[i])
             if i < 64: element_list=self.list_chip_1
             elif i < 128: element_list=self.list_chip_2
             elif i < 192: element_list=self.list_chip_3
             elif i < 256: element_list=self.list_chip_4
             for ifit in range(len(element_list)):
                 source_E=element_list[ifit][1]
                 fit_adc=self.fit_result[i][source_E]
                 _g.SetPoint(ifit+1, fit_adc, float(source_E))
                 #_g.SetPoint(ifit, fit_adc, float(source_E))# NOTE: If no Zero point
                 if ifit == 0:
                    source_Eaft=element_list[1][1]
                    fit_adcaft=self.fit_result[i][source_Eaft]
                    slope_1 = (float(source_Eaft) - float(source_E))/(fit_adcaft - fit_adc)
                    f_x_1, f_y_1 = fit_adc, float(source_E)
                 if ifit == len(element_list)-1:
                    source_Epre=element_list[ifit-1][1]
                    fit_adcpre=self.fit_result[i][source_Epre]
                    if fit_adc == fit_adcpre: fit_adc +=1
                    slope = (float(source_E) - float(source_Epre))/(fit_adc - fit_adcpre)
                    f_x, f_y = fit_adc, float(source_E)
             _g.SetPoint(0, 0, f_y_1-(f_x_1-0)*slope_1) # Set Zero point
             _g.SetPoint(len(element_list) + 1, 1024, (1024-f_x)*slope + f_y)# Set final point 
             graph_list.append(_g)

             _s = ROOT.TSpline3("spline_"+str(i), _g)
             spline_list.append(_s)

             _g.Fit(_ln,"QR")
             fline_list.append(_ln)
             
             del _g,_s, _ln
          return graph_list,spline_list,fline_list

      def plot(self):
          check_range=[20,800]#NOTE:change here for plotting range
          islog = 1
          __location__ = os.path.realpath(
                  os.path.join(os.getcwd(), os.path.dirname(__file__)))
          ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
          #ROOT.SetAtlasStyle()
          plots_path="/Users/chiu.i-huan/Desktop/temp_output/"+args.detector
          if not os.path.isdir(plots_path):
             os.system("mkdir {}".format(plots_path))
          c0name=plots_path+"/hist_"+self.source+"_all.pdf"
          c1name=plots_path+"/hist_"+self.source+"_fit.pdf"
          c2name=plots_path+"/hist_"+self.source+"_gr.pdf"
          c0 = ROOT.TCanvas(c0name,"",0,0,2400,800)
          c1 = ROOT.TCanvas(c1name,"",0,0,1200,600)
          c2 = ROOT.TCanvas(c2name,"",0,0,800,800)
          c0.Divide(3,1)
          c0.Print(c0name + "[", "pdf")
          c1.Print(c1name + "[", "pdf")
          c2.Print(c2name + "[", "pdf")
          for i in range(256):
             latex = getLatex(i,400,8000)
             if i < 10: temp_name = "hist_cmn" + "00" + str(i) 
             elif i < 100:  temp_name = "hist_cmn" + "0" + str(i) 
             else : temp_name = "hist_cmn" + str(i)
             new_name=self.name_list[i].replace(temp_name,"ch : {}".format(i))
             gROOT.ProcessLine("gErrorIgnoreLevel = kWarning;")
             self.hist_list[i].SetLineColor(1)
             for ja in self.fit_array[i]: 
                ja.SetLineColorAlpha(ROOT.kRed,0.7)
                ja.SetLineWidth(1)

             self.hist_list[i].GetXaxis().SetRangeUser(check_range[0],check_range[1])
             if islog == 0:
                _maxbincon = -1.
                for ibin in range(int(check_range[0]+50),int(check_range[1]+50)):
                   if self.hist_list[i].GetBinContent(ibin) > _maxbincon: _maxbincon = self.hist_list[i].GetBinContent(ibin)
                self.hist_list[i].SetMinimum(0)
                self.hist_list[i].SetMaximum(_maxbincon*1.2)

             c0.cd(1)
             gPad.SetLogy(islog)
             self.hist_list[i].Draw()
             for ja in self.fit_array[i]: ja.Draw("SAME")
             latex.DrawLatex(0.5,0.85,new_name)
             c1.cd()
             gPad.SetLogy(islog)
             gPad.SetGridx(1)
             self.hist_list[i].GetXaxis().SetNdivisions(80, 1, 5, True)
             self.hist_list[i].GetXaxis().SetLabelSize(0.01)
             self.hist_list[i].Draw()
             for ja in self.fit_array[i]: ja.Draw("SAME")
             latex.DrawLatex(0.5,0.85,new_name)

             c0.cd(2)
             graph=self.graph_list[i]
             graph.SetMarkerColor(4)
             graph.SetMarkerStyle(21)
             graph.Draw("ALP")
             latex.DrawLatex(0.5,0.85,new_name)         
             c2.cd()
             graph.Draw("ALP")
             latex.DrawLatex(0.5,0.85,new_name)         

             c0.cd(3)
             p=self.spline_list[i]
             p.SetLineColor(1)
             p.Draw("L")
             _p=self.fline_list[i]
             _p.SetLineColor(2)
             #_p.Draw("SAME")
             latex.DrawLatex(0.5,0.85,new_name)         
             c0.Print(c0name, "pdf")
             c1.Print(c1name, "pdf")
             c2.Print(c2name, "pdf")
          c0.Print(c0name + "]", "pdf")
          c1.Print(c1name + "]", "pdf")
          c2.Print(c2name + "]", "pdf")

      def Printout(self):
          self.output=self.output.replace(".root","_"+self.source+".root")
          fout = ROOT.TFile( self.output, 'recreate' )
          fout.cd()
          for i in range(256):
             graph=self.graph_list[i]
             graph.SetName("graph_"+str(i))
             spline=self.spline_list[i]
             spline.SetName("spline_"+str(i))
             fline=self.fline_list[i]
             fline.SetName("fline_"+str(i))
             graph.Write()    
             spline.Write()    
             fline.Write()    
          fout.Write()          

        
def run(args):
    if ".root" not in args.input:
       _input=args.input+"/"+args.detector+"/"+args.source+"/" 
    _outputname="./files_cali/spline_calibration_"+args.detector+".root"
    _table=args.table+args.detector+"_table"+"/energy_table.txt"
    Cal=Calibration(filename=_input, output=_outputname,Etable=_table,source=args.source)
    Cal.plot()
    Cal.Printout()
    exit(0)
 
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("-i","--input", dest="input", type=str, default="/Users/chiu.i-huan/Desktop/new_scientific/imageAna/data/minami_data", help="Input File Name")
    parser.add_argument("-d","--detector", dest="detector", type=str, default="cdte201", help="Output File Name")
    parser.add_argument("-s","--source", dest="source", type=str, default="Am", help="Am or Co or Ba")
    parser.add_argument("--table", type=str, default="./energy_table/", help="energy table used for 2mm cdte")
    args = parser.parse_args()
    
    run(args)# for CdTe

