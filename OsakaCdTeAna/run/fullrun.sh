DIRDATA="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/data/OsakaCdTe_JPARC_Jan2023"
MACRODIR="/Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna"
NCORES=8
OUTNAME="0208"

# =====
# ================== test run ====================
# =====
#python3 main.py /Users/chiu.i-huan/Desktop/new_scientific/OsakaCdTeAna/data/OsakaCdTe_JPARC_Jan2023/cdte202/data20230121_cdte202_3_00060_001.root -o ${OUTNAME} -db ${MACRODIR}/auxfile/database_202.root

#python3 main.py /Volumes/HD-PCFSU3-A/OsakaCdTe_RICenter_calidata_Dec2022/cdte203/Eu/test20221219_cdte203_00009_001.root -o ri_eu -db ${MACRODIR}/auxfile/database_203.root

# =====
# ================== Para. Run ====================
# =====
#parallel --eta -j ${NCORES} python3 main.py ${DIRDATA}/cdte{2}/data20230121_cdte{2}_3_000{1}_001.root -o ${OUTNAME} -db ${MACRODIR}/auxfile/database_{2}.root ::: 08 09 10 11 12 13 14 15 16 17 18 19 20 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60  ::: 201 202 203 204
#parallel --eta -j ${NCORES} python3 main.py ${DIRDATA}/cdte{2}/data20230123_cdte{2}_3_000{1}_001.root -o ${OUTNAME} -db ${MACRODIR}/auxfile/database_{2}.root ::: 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 31 32 33 34 35 37 38 ::: 201 202 203 204
parallel --eta -j ${NCORES} python3 main.py ${DIRDATA}/cdte{2}/data20230121_cdte{2}_3_000{1}_001.root -o ${OUTNAME} -db ${MACRODIR}/auxfile/database_{2}.root ::: 03  ::: 201 202 203 204

