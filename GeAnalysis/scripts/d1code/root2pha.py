"""
This module shows the main functions.
"""
__author__    = "I-Huan CHIU"
__email__     = "ichiu@rirc.osaka-u.ac.jp"
__created__   = "2023-02-02"
__copyright__ = "Copyright 2023 I-Huan CHIU"
__license__   = "GPL http://www.gnu.org/licenses/gpl.html"

import os,sys,ROOT,argparse
from datetime import date
today = date.today()

def main(args):
    hname_list=["forward","backward"]
    fint = ROOT.TFile(args.inputFolder,"read")
    for hname in hname_list:
       h = fint.Get(hname)
   
       print_latex = []
       print_latex.append("# PHA DATA for MUSE D1")
       print_latex.append("# Date: {}".format(today.strftime("%B %d, %Y")))
       print_latex.append("# Root file: {}".format(fint.GetName()))
       print_latex.append("# Histogram: {}".format(hname))
       print_latex.append("# PHA data format: binID, time at the bin center, content")
   
       Nbin=h.GetXaxis().GetNbins()
       for i in range(Nbin):
          _center=h.GetBinCenter(i+1)
          _content=h.GetBinContent(i+1)
          print_latex.append("{0}, {1}, {2}".format(i+1, _center, _content))
   
       outpha=args.inputFolder.replace(".root","_{}.pha".format(hname))
       with open(outpha, 'w') as f:
          f.write("\n".join(print_latex))
       print("Output file : {}".format(outpha)) 

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("inputFolder", type=str, default="/Users/chiu.i-huan/Desktop/new_scientific/imageAna/data/testinput/", help="Input File Name")
    args = parser.parse_args()

    main(args)
