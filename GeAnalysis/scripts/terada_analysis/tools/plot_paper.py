import sys, os, ROOT, math
from array import array
import numpy as np
ROOT.gErrorIgnoreLevel = ROOT.kFatal
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()
#ROOT.gROOT.SetBatch(1)

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(False)
ROOT.gStyle.SetEndErrorSize(0)
ROOT.gStyle.SetFrameFillColor(0)
ROOT.gStyle.SetCanvasColor(0)
ROOT.gStyle.SetPadBorderSize(0)

def plot_spec():
    #fint=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/White/203084_beam.root","read"); time_s = 43368; sample_name="white"
    fint=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/DEW12007_bar/203089_beam.root","read"); time_s = 44846; sample_name="dewbar"
    fint_bkg=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Blank_dsl20/203092_beam.root","read"); time_b = 10853
    h=fint.Get("Energy")
    hb=fint_bkg.Get("Energy")
    h.Scale(1./time_s)
    hb.Scale(1./time_b)

    h.SetTitle(";Energy [keV];Counts/sec./25 eV")
    h.SetLineColorAlpha(2,0.9)
    h.SetLineWidth(1)
    h_low=h.Clone()
    h_high=h.Clone()

    hb.SetTitle(";Energy [keV];Counts/sec./25 eV")
    hb.SetLineColorAlpha(4,0.9)
    hb.SetLineWidth(1)
    hb_low=hb.Clone()
    hb_high=hb.Clone()

    leg = ROOT.TLegend(.8,.75,.90,.90)
    leg.SetFillColor(0)
    leg.SetLineColor(0)
    leg.SetBorderSize(0)
    leg.AddEntry(h,"sample","l") 
    leg.AddEntry(hb,"blank","l") 

    c1=ROOT.TCanvas("c1","c1",2400,1000)
    c1.cd()
    h.Draw("hist")
    hb.Draw("hist same")
    leg.Draw("same")
    c1.Print("/Users/chiu.i-huan/Desktop/temp_output/paper_long_es_{}.pdf".format(sample_name))

    leg2 = ROOT.TLegend(.75,.7,.90,.90)
    leg2.SetFillColor(0)
    leg2.SetLineColor(0)
    leg2.SetBorderSize(0)
    leg2.AddEntry(h,"sample","l") 
    leg2.AddEntry(hb,"blank","l") 

    c2=ROOT.TCanvas("c1","c1",1200,1000)
    c2.cd()
    ROOT.gPad.SetLogy(1)
    h_low.GetXaxis().SetRangeUser(50,100)
    h_low.Draw("hist")
    hb_low.Draw("hist same")
    leg2.Draw("same")
    c2.Print("/Users/chiu.i-huan/Desktop/temp_output/paper_low_es_{}.pdf".format(sample_name))

    c3=ROOT.TCanvas("c1","c1",1200,1000)
    c3.cd()
    ROOT.gPad.SetLogy(1)
    h_high.GetXaxis().SetRangeUser(130,170)
    h_high.Draw("hist")
    hb_high.Draw("hist same")
    leg2.Draw("same")
    c3.Print("/Users/chiu.i-huan/Desktop/temp_output/paper_high_es_{}.pdf".format(sample_name))

def plot_eff():
    # NWA482
    #Si:21.034575
    #Al:15.87753
    #Ca:12.149747
    #Mg:2.412144
    #Fe:3.10922
    #O:45.416784

    # took from paper
    name_list=["Al(3-2)","Fe(5-4)","Ca(4-3)","Mg(3-2)","Al(4-2)","Fe(4-3)","O(2-1)","Ca(3-2)","O(3-1)","O(4-1)"] 
    muRate=[0.70,0.11,0.76,0.16,0.14,0.14,4.73,0.61,1.37,0.83]
    muRate_e_st=[0.02,0.01,0.01,0.01,0.01,0.01,0.05,0.02,0.02,0.02]# statistic
    muRate_e_sy=[0.194, 0.009, 0.033, 0.019, 0.012, 0.009, 0.323, 0.032, 0.070, 0.035]#systematic
    muRate_e=[]
    print("Error for rate")
    for i, est in enumerate(muRate_e_st):
       _error=math.sqrt(est*est + muRate_e_sy[i]*muRate_e_sy[i])
       muRate_e.append(_error)
       print("{0:.2f}".format(_error))
    Al_mr, Fe_mr, Ca_mr, Mg_mr, O_mr=15.87753/21.034575,3.10922/21.034575, 12.149747/21.034575,2.412144/21.034575,45.416784/21.034575
    massRate=[Al_mr,Fe_mr,Ca_mr,Mg_mr,Al_mr,Fe_mr,O_mr,Ca_mr,O_mr,O_mr]
    #gr=ROOT.TGraphAsymmErrors()
    gr=ROOT.TH1D("gr","gr",len(name_list),0,len(name_list))
    gr_e=ROOT.TH1D("gr","gr",len(name_list),0,len(name_list))
    c2=ROOT.TCanvas("c2","c2",1200,1000)
    for i, name in enumerate(name_list):
       gr.GetXaxis().SetBinLabel(i+1,name_list[i])
       error=(massRate[i]/muRate[i])*(muRate_e[i]/muRate[i])
       #gr.SetPoint(i,i,massRate[i]/muRate[i])
       #gr.SetPointError(i,0,0,error,error)
       gr.SetBinContent(i+1,massRate[i]/muRate[i])
       gr_e.SetBinContent(i+1,massRate[i]/muRate[i])
       gr_e.SetBinError(i+1,error)
       print("{0}, {1:.2f} $\pm$ {2:.2f}".format(name, massRate[i]/muRate[i], error))
    c2.cd()
    ROOT.gPad.SetLogy(1)
    gr.SetTitle(";X;f_{X/Si(3-2)}")
    gr.GetYaxis().SetLabelSize(0.055)
    gr.GetYaxis().SetTitleSize(0.06)
    gr.GetXaxis().SetTitleSize(0.06)
    gr.SetLineColor(1)
    gr.Draw(" hist L")
    gr_e.Draw("ep same")
    c2.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_paper_mass_eff.pdf")


def _fit(X_array,Y_array):
    fit_points=[np.array([0,X_array[0],X_array[1]]),np.array([0,Y_array[0],Y_array[1]])]
    _temp_g=ROOT.TGraph(3,fit_points[0],fit_points[1])
    _fit_g=_temp_g.Clone()
    fline=ROOT.TF1("fline","pol1",-0.1,1)
    fline.FixParameter(0,0)#TODO fix zero point
    _fit_g.Fit("fline","qn")
    return [fline,_temp_g]# return TF1 and TGraph

def plot_curve():
    fint=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/terada_analysis/auxfile/intensity_sum.root","read")
    tree=fint.Get("tree")
    peak_list=["Fegamma","Si32", "Al43", "Al42", "Fe54", "Fe43", "Ca43", "Ca32", "Mg32", "Cu43", "O21", "O31", "O41"]
    n_peaks=len(peak_list)
    _temp_count_final, _temp_error=np.zeros((6,2,n_peaks)), np.zeros((6,2,n_peaks)) # only black & white

    for _i in range(tree.GetEntries()):
       tree.GetEntry(_i)
       if (tree.sample_ID > 1): continue
       _temp_count_final[tree.ch-1][tree.sample_ID][tree.peak_ID]=tree.count_final
       _temp_error[tree.ch-1][tree.sample_ID][tree.peak_ID]=tree.error_final

    # *** dispersion of detectors ***
    dis_error=_temp_count_final - np.average(_temp_count_final,axis=0)
    total_error=np.sqrt(np.square(_temp_error)+np.square(dis_error))
    
    sum_count = np.sum(_temp_count_final, axis=0)
    error = np.sqrt(np.sum(np.square(total_error), axis=0))

    # *** set array ***
    Nsample=sum_count[:,1].size
    Si32_array,Si32_e=sum_count[:,1],error[:,1]
    Al43_array,Al43_e=sum_count[:,2],error[:,2]
    Al42_array,Al42_e=sum_count[:,3],error[:,3]
    Fe54_array,Fe54_e=sum_count[:,4],error[:,4]
    Fe43_array,Fe43_e=sum_count[:,5],error[:,5]
    Ca43_array,Ca43_e=sum_count[:,6],error[:,6]
    Ca32_array,Ca32_e=sum_count[:,7],error[:,7]
    Mg32_array,Mg32_e=sum_count[:,8],error[:,8]
    O21_array,O21_e=sum_count[:,10],error[:,10]
    O31_array,O31_e=sum_count[:,11],error[:,11]
    O41_array,O41_e=sum_count[:,12],error[:,12]    
    Al_ref=np.array([0.22138347760312999,0.7598975203768764,0.44751891335024385,0.44751891335024385,0.44751891335024385])
    Fe_ref=np.array([0.8556488650061658,0.13513692742839553,0.45254817705772626,0.45254817705772626,0.45254817705772626])
    Ca_ref=np.array([0.3728378706921842,0.5970949816409656,0.4260001612484084,0.4260001612484084,0.4260001612484084])
    Mg_ref=np.array([0.24387843829585934,0.11357561582036725,0.2265341984728784,0.2265341984728784,0.2265341984728784])
    #Al_ref=np.array([0.225,0.8,0.46,0.46,0.46])
    #Fe_ref=np.array([0.43,0.07,0.23,0.23,0.23])
    #Ca_ref=np.array([0.26,0.40,0.30,0.30,0.30])
    #Mg_ref=np.array([0.28,0.13,0.26,0.26,0.26])

    # *** mk graph ***
    error_x=(Al42_array/Si32_array)*np.sqrt((np.square(Al42_e/Al42_array)+np.square(Si32_e/Si32_array)))
    error_y=np.array([0.,0.]) # only black & white
    gr_al=ROOT.TGraphAsymmErrors(Nsample,Al42_array/Si32_array,Al_ref,error_x,error_x,error_y,error_y)
    gr_al_b=ROOT.TGraphAsymmErrors(Nsample-1,Al42_array[0]/Si32_array[0],Al_ref[0],error_x[0],error_x[0],error_y[0],error_y[0])
    gr_al_w=ROOT.TGraphAsymmErrors(Nsample-1,Al42_array[1]/Si32_array[1],Al_ref[1],error_x[1],error_x[1],error_y[1],error_y[1])
    fline_al_list=_fit(Al42_array/Si32_array,Al_ref)

    #Fe43    
    error_x=(Fe43_array/Si32_array)*np.sqrt((np.square(Fe43_e/Fe43_array)+np.square(Si32_e/Si32_array)))
    gr_fe=ROOT.TGraphAsymmErrors(Nsample,Fe43_array/Si32_array,Fe_ref,error_x,error_x,error_y,error_y)
    gr_fe_b=ROOT.TGraphAsymmErrors(Nsample-1,Fe43_array[0]/Si32_array[0],Fe_ref[0],error_x[0],error_x[0],error_y[0],error_y[0])
    gr_fe_w=ROOT.TGraphAsymmErrors(Nsample-1,Fe43_array[1]/Si32_array[1],Fe_ref[1],error_x[1],error_x[1],error_y[1],error_y[1])
    fline_fe_list=_fit(Fe43_array/Si32_array,Fe_ref)
    #Fe54
    #error_x=(Fe54_array/Si32_array)*np.sqrt((np.square(Fe54_e/Fe54_array)+np.square(Si32_e/Si32_array)))
    #gr_fe=ROOT.TGraphAsymmErrors(Nsample,Fe54_array/Si32_array,Fe_ref,error_x,error_x,error_y,error_y)
    #gr_fe_b=ROOT.TGraphAsymmErrors(Nsample-1,Fe54_array[0]/Si32_array[0],Fe_ref[0],error_x[0],error_x[0],error_y[0],error_y[0])
    #gr_fe_w=ROOT.TGraphAsymmErrors(Nsample-1,Fe54_array[1]/Si32_array[1],Fe_ref[1],error_x[1],error_x[1],error_y[1],error_y[1])
    #fline_fe_list=_fit(Fe54_array/Si32_array,Fe_ref)

    error_x=(Ca32_array/Si32_array)*np.sqrt((np.square(Ca32_e/Ca32_array)+np.square(Si32_e/Si32_array)))
    gr_ca=ROOT.TGraphAsymmErrors(Nsample,Ca32_array/Si32_array,Ca_ref,error_x,error_x,error_y,error_y)
    gr_ca_b=ROOT.TGraphAsymmErrors(Nsample-1,Ca32_array[0]/Si32_array[0],Ca_ref[0],error_x[0],error_x[0],error_y[0],error_y[0])
    gr_ca_w=ROOT.TGraphAsymmErrors(Nsample-1,Ca32_array[1]/Si32_array[1],Ca_ref[1],error_x[1],error_x[1],error_y[1],error_y[1])
    fline_ca_list=_fit(Ca32_array/Si32_array,Ca_ref)

    error_x=(Mg32_array/Si32_array)*np.sqrt((np.square(Mg32_e/Mg32_array)+np.square(Si32_e/Si32_array)))
    gr_mg=ROOT.TGraphAsymmErrors(Nsample,Mg32_array/Si32_array,Mg_ref,error_x,error_x,error_y,error_y)
    gr_mg_b=ROOT.TGraphAsymmErrors(Nsample-1,Mg32_array[0]/Si32_array[0],Mg_ref[0],error_x[0],error_x[0],error_y[0],error_y[0])
    gr_mg_w=ROOT.TGraphAsymmErrors(Nsample-1,Mg32_array[1]/Si32_array[1],Mg_ref[1],error_x[1],error_x[1],error_y[1],error_y[1])
    fline_mg_list=_fit(Mg32_array/Si32_array,Mg_ref)


    c1=ROOT.TCanvas("c1","c1",800,800)
    c1.cd()
    gr_al_b.SetTitle(";#muX/#muSi(3-2);X/Si Ref.")
    gr_al_b.GetXaxis().CenterTitle()
    gr_al_b.GetYaxis().CenterTitle()
    gr_al_b.GetXaxis().SetTitleSize(0.06)
    gr_al_b.GetYaxis().SetTitleSize(0.06)

    gr_al_b.SetLineColor(1)
    gr_al_b.SetMarkerColor(1)
    gr_al_b.SetMarkerSize(2)
    gr_al_w.SetLineColor(1)
    gr_al_w.SetMarkerColor(1)
    gr_al_w.SetMarkerStyle(22)
    gr_al_w.SetMarkerSize(2)
    fline_al_list[0].SetLineColor(1)

    gr_fe_b.SetLineColor(2)
    gr_fe_b.SetMarkerColor(2)
    gr_fe_b.SetMarkerSize(2)
    gr_fe_w.SetLineColor(2)
    gr_fe_w.SetMarkerColor(2)
    gr_fe_w.SetMarkerStyle(22)
    gr_fe_w.SetMarkerSize(2)
    fline_fe_list[0].SetLineColor(2)

    gr_ca_b.SetLineColor(ROOT.kSpring-1)
    gr_ca_b.SetMarkerColor(ROOT.kSpring-1)
    gr_ca_b.SetMarkerSize(2)
    gr_ca_w.SetLineColor(ROOT.kSpring-1)
    gr_ca_w.SetMarkerColor(ROOT.kSpring-1)
    gr_ca_w.SetMarkerStyle(22)
    gr_ca_w.SetMarkerSize(2)
    fline_ca_list[0].SetLineColor(ROOT.kSpring-1)

    gr_mg_b.SetLineColor(4)
    gr_mg_b.SetMarkerColor(4)
    gr_mg_b.SetMarkerSize(2)
    gr_mg_w.SetLineColor(4)
    gr_mg_w.SetMarkerColor(4)
    gr_mg_w.SetMarkerStyle(22)
    gr_mg_w.SetMarkerSize(2)
    fline_mg_list[0].SetLineColor(4)
     
    gr_al_b.GetXaxis().SetLimits(-0.01,1.0)
    gr_al_b.SetMinimum(-0.01);gr_al_b.SetMaximum(1.2)
     
    gr_al_b.Draw("ap")
    gr_fe_b.Draw("same p")
    gr_ca_b.Draw("same p")
    gr_mg_b.Draw("same p")
    gr_al_w.Draw("same p")
    gr_fe_w.Draw("same p")
    gr_ca_w.Draw("same p")
    gr_mg_w.Draw("same p")
    fline_al_list[0].Draw("same")
    fline_fe_list[0].Draw("same")
    fline_ca_list[0].Draw("same")
    fline_mg_list[0].Draw("same")
    
    leg = ROOT.TLegend(.35,.65,.75,.9)
    leg.SetFillColor(0)
    leg.SetLineColor(0)
    leg.SetBorderSize(0)
    leg.AddEntry(gr_al_b,"Al(4-2)","l")
    #leg.AddEntry(gr_fe_b,"Fe(4-3)","l")
    leg.AddEntry(gr_fe_b,"Fe(5-4)","l")
    leg.AddEntry(gr_ca_b,"Ca(3-2)","l")
    leg.AddEntry(gr_mg_b,"Mg(3-2)","l")
    leg.Draw("same")

    gr_al_b.Draw("same p")
    gr_fe_b.Draw("same p")
    gr_ca_b.Draw("same p")
    gr_mg_b.Draw("same p")
    gr_al_w.Draw("same p")
    gr_fe_w.Draw("same p")
    gr_ca_w.Draw("same p")
    gr_mg_w.Draw("same p")
    fline_al_list[0].Draw("same")
    fline_fe_list[0].Draw("same")
    fline_ca_list[0].Draw("same")
    fline_mg_list[0].Draw("same")
    c1.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_musr_combined_curve_fe43.pdf")

def plot_mixture():
    fint=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/terada_analysis/auxfile/intensity_sum.root","read")
    tree=fint.Get("tree")
    peak_list=["Fe#gamma","Si32", "Al43", "Al42", "Fe54", "Fe43", "Ca43", "Ca32", "Mg32", "Cu43", "O21", "O31", "O41"]
    n_peaks=len(peak_list)
    _temp_count_final, _temp_error=np.zeros((6,5,n_peaks)), np.zeros((6,5,n_peaks)) # only black & white

    for _i in range(tree.GetEntries()):
       tree.GetEntry(_i)
       _temp_count_final[tree.ch-1][tree.sample_ID][tree.peak_ID]=tree.count_final
       _temp_error[tree.ch-1][tree.sample_ID][tree.peak_ID]=tree.error_final

    # *** dispersion of detectors ***
    dis_error=_temp_count_final - np.average(_temp_count_final,axis=0)
    total_error=np.sqrt(np.square(_temp_error)+np.square(dis_error))
    
    sum_count = np.sum(_temp_count_final, axis=0)
    error = np.sqrt(np.sum(np.square(total_error), axis=0))

    # *** find X/Si from muX/muSi ***
    black_array=sum_count[0]/sum_count[0][1]
    white_array=sum_count[1]/sum_count[1][1]
    dew_array=sum_count[2]/sum_count[2][1]
    dewbar_array=sum_count[3]/sum_count[3][1]
    dewbar35_array=sum_count[4]/sum_count[4][1]

    black_e=black_array*np.sqrt(np.square(error[0]/sum_count[0]) + np.square(error[0][1]/sum_count[0][1]))
    white_e=white_array*np.sqrt(np.square(error[1]/sum_count[1]) + np.square(error[1][1]/sum_count[1][1]))
    dew_e=dew_array*np.sqrt(np.square(error[2]/sum_count[2]) + np.square(error[2][1]/sum_count[2][1]))
    dewbar_e=dewbar_array*np.sqrt(np.square(error[3]/sum_count[3]) + np.square(error[3][1]/sum_count[3][1]))
    dewbar35_e=dewbar35_array*np.sqrt(np.square(error[4]/sum_count[4]) + np.square(error[4][1]/sum_count[4][1]))

    # *** check sample (dew, dewbar, dewbar35) ***
    sample_list=[dew_array,dewbar_array,dewbar35_array]
    error_list=[dew_e,dewbar_e,dewbar35_e]
    color_list=[ROOT.kSpring-1,2,4]
    h1_list=[ROOT.TH1D("h1","h1",len(black_array),0,len(black_array)),ROOT.TH1D("h1","h1",len(black_array),0,len(black_array)),ROOT.TH1D("h1","h1",len(black_array),0,len(black_array))]
    name_list=["Front","Center","Back"]
    line_list=[ROOT.TLine(),ROOT.TLine(),ROOT.TLine()]

    c1 = ROOT.TCanvas("c1","c1",1000,800)
    c1.cd()
    h1=ROOT.TH1D("h1","h1",len(black_array),0,len(black_array))
    leg = ROOT.TLegend(.45,.7,.7,.90)
    leg.SetFillColor(0)
    leg.SetLineColor(0)
    leg.SetBorderSize(0)
    for isa, sample in enumerate(sample_list): #dew, dewbar, dewbar35      
       h1=h1_list[isa] 
       h1.SetDirectory(0)
       numerator=(sample-black_array)
       deniminator=(white_array-black_array)
       numerator_e=np.sqrt(np.square(error_list[isa])+np.square(black_e))
       deniminator_e=np.sqrt(np.square(white_e)+np.square(black_e))

       deniminator[deniminator==0]=1
       mix_1 = numerator/deniminator
       numerator[numerator==0]=1
       mix_1_e = mix_1*np.sqrt(np.square(numerator_e/numerator) + np.square(deniminator_e/deniminator))

       average=0.
       h1.Reset()
       for i, value in enumerate(mix_1):
          h1.GetXaxis().SetBinLabel(i+1,peak_list[i])
          h1.Fill(i,value)
          h1.SetBinError(i,mix_1_e[i])
          h1.SetMarkerColor(color_list[isa])
          h1.SetLineColor(color_list[isa])
          if i == 3 or i == 5 or i == 7 or i == 8: average+=value #Al42, Fe43, Ca32, Mg32
            
       h1.SetTitle(";;Ratio of White")
       if isa == 0: h1.Draw("ep")
       else: h1.Draw("ep same")
       leg.AddEntry(h1,name_list[isa],"l")
       line_list[isa]=ROOT.TLine(0,average/4.,len(black_array),average/4.) # 4 points
       line_list[isa].SetLineColor(color_list[isa])
       line_list[isa].SetLineStyle(10)
       line_list[isa].Draw("l same")
       print("average , {0}, {1:.2f} from white/NWA482/anorthositic crust".format(name_list[isa], average/4.))
    leg.Draw("same")
    c1.Print("/Users/chiu.i-huan/Desktop/temp_output/c_terada_musr_mixture_func.pdf") 

    # *** do chi-square goodness of fit test ***
    gr_list=[ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph()]
    gr4_list=[ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph()]
    for isa, sample in enumerate(sample_list):      
       x_list,y_list,x4_list,y4_list=[],[],[],[]
       chi2_min,Exp_value=100,0
       for ie in range(1,1000):# 0.1% ~ 99.9%          
          Exp,chi2,chi2_4=ie/1000.,0,0.
          for i, value in enumerate(zip(sample,black_array,white_array,error_list[isa],black_e,white_e)):# scan 13 points
              dew_V,black_V,white_V=value[0],value[1],value[2]
              dew_E,black_E,white_E=value[3],value[4],value[5]
              numerator=dew_V-(black_V+(white_V-black_V)*Exp)
              deniminator=math.sqrt(math.pow(dew_E,2)+math.pow(black_E,2)+math.pow(black_E*Exp,2)+math.pow(white_E*Exp,2))
              chi2+=math.pow(numerator/deniminator,2)
              if i == 3 or i == 5 or i == 7 or i == 8: chi2_4+=math.pow(numerator/deniminator,2) #Al42, Fe43, Ca32, Mg32
          x_list.append(Exp*100)
          y_list.append(chi2)
          x4_list.append(Exp*100)
          y4_list.append(chi2_4)
          if chi2_4 < chi2_min: 
             chi2_min=chi2_4
             Exp_value=Exp*100
       x,y,x4,y4=np.array(x_list),np.array(y_list),np.array(x4_list),np.array(y4_list)
       gr_list[isa]=ROOT.TGraph(len(x),x,y)
       gr_list[isa].SetLineColor(color_list[isa])
       gr_list[isa].SetMarkerColorAlpha(color_list[isa],0)
       gr_list[isa].SetTitle(";Weight of NWA482 [%];#chi2")
       gr_list[isa].GetXaxis().CenterTitle()
       gr_list[isa].GetYaxis().CenterTitle()
       gr4_list[isa]=ROOT.TGraph(len(x4),x4,y4)
       gr4_list[isa].SetLineColor(color_list[isa])
       gr4_list[isa].SetMarkerColorAlpha(color_list[isa],0)
       gr4_list[isa].SetTitle(";Weight of NWA482 [%];#chi2")
       gr4_list[isa].GetXaxis().CenterTitle()
       gr4_list[isa].GetYaxis().CenterTitle()
       print("chi-square test , {0}, chi2 = {1:.2f}, {2:.2f}% from white/NWA482/anorthositic crust".format(name_list[isa], chi2_min, Exp_value))
    c2 = ROOT.TCanvas("c2","c2",1000,800)
    c2.cd()
    gr_list[2].Draw()
    gr_list[1].Draw("SAME")
    gr_list[0].Draw("SAME")
    leg.Draw("same")
    #ROOT.gPad.SetLogy(1)
    c2.Print("/Users/chiu.i-huan/Desktop/temp_output/c_terada_mixture_chi2test.pdf") 
    c3 = ROOT.TCanvas("c3","c3",1000,800)
    c3.cd()
    gr4_list[2].Draw()
    gr4_list[1].Draw("SAME")
    gr4_list[0].Draw("SAME")
    leg.Draw("same")
    #ROOT.gPad.SetLogy(1)
    c3.Print("/Users/chiu.i-huan/Desktop/temp_output/c_terada_mixture_chi2test_4atoms.pdf") 

def plot_spec_nwa():
    cv_name = "NWA_blank"
    fint_nwa032=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Black/203086_beam.root","read");time_nwa032 = 35702; 
    fint_nwa482=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/White/203084_beam.root","read");time_nwa482 = 43368; 
    fint_dew=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/DEW12007/203079_beam.root","read");time_dew = 17367;    
    fint_dewbar=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/DEW12007_bar/203089_beam.root","read");time_dewb = 44846;           
    fint_dewbar35=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/DEW12007_bar_35MeV/203095_beam.root","read");time_dewb35 = 37233; 
    fint_bkg=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Blank_dsl20/203092_beam.root","read"); time_b = 10853;                 

    h_nwa032=fint_nwa032.Get("Energy")
    h_nwa482=fint_nwa482.Get("Energy")
    hb=fint_bkg.Get("Energy")

    h_nwa032.Scale(1./time_nwa032)
    h_nwa482.Scale(1./time_nwa482)
    hb.Scale(1./time_b)

    h_nwa032.SetTitle(";Energy [keV];Counts/sec./25 eV")
    h_nwa032.SetLineColorAlpha(2,0.7)
    h_nwa032.SetLineWidth(1)
    h_nwa032_low=h_nwa032.Clone()
    h_nwa032_high=h_nwa032.Clone()

    h_nwa482.SetTitle(";Energy [keV];Counts/sec./25 eV")
    h_nwa482.SetLineColorAlpha(4,0.7)
    h_nwa482.SetLineWidth(1)
    h_nwa482_low=h_nwa482.Clone()
    h_nwa482_high=h_nwa482.Clone()

    hb.SetTitle(";Energy [keV];Counts/sec./25 eV")
    hb.SetLineColorAlpha(1,0.7)
    hb.SetLineWidth(1)
    hb_low=hb.Clone()
    hb_high=hb.Clone()

    leg = ROOT.TLegend(.75,.65,.90,.90)
    leg.SetFillColor(0)
    leg.SetLineColor(0)
    leg.SetBorderSize(0)
    leg.AddEntry(h_nwa032,"NWA032","l") 
    leg.AddEntry(h_nwa482,"NWA482","l") 
    leg.AddEntry(hb,"blank","l") 

    c1=ROOT.TCanvas("c1","c1",2400,1200)
    c1.cd()
    h_nwa032.Draw("hist")
    h_nwa482.Draw("hist same")
    hb.Draw("hist same")
    leg.Draw("same")
    c1.Print("/Users/chiu.i-huan/Desktop/temp_output/MaPS_paper_long_es_{}.pdf".format(cv_name))

    leg2 = ROOT.TLegend(.75,.7,.90,.90)
    leg2.SetFillColor(0)
    leg2.SetLineColor(0)
    leg2.SetBorderSize(0)
    leg.AddEntry(h_nwa032,"NWA032","l") 
    leg.AddEntry(h_nwa482,"NWA482","l") 
    leg.AddEntry(hb,"blank","l") 

    c2=ROOT.TCanvas("c2","c2",1200,1000)
    c2.cd()
    ROOT.gPad.SetLogy(1)
    h_nwa032_low.GetXaxis().SetRangeUser(40,100)
    h_nwa032_low.Draw("hist")
    h_nwa482_low.Draw("hist same")
    hb_low.Draw("hist same")
    #leg2.Draw("same")
    c2.Print("/Users/chiu.i-huan/Desktop/temp_output/MaPS_paper_low_es_{}.pdf".format(cv_name))

    c3=ROOT.TCanvas("c1","c1",1200,1000)
    c3.cd()
    ROOT.gPad.SetLogy(1)
    h_nwa032_high.GetXaxis().SetRangeUser(130,180)
    h_nwa032_high.Draw("hist")
    h_nwa482_high.Draw("hist same")
    hb_high.Draw("hist same")
    #leg2.Draw("same")
    c3.Print("/Users/chiu.i-huan/Desktop/temp_output/MaPS_paper_high_es_{}.pdf".format(cv_name))

def plot_spec_dew():
    cv_name = "DEW"
    fint_nwa032=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Black/203086_beam.root","read");time_nwa032 = 35702; 
    fint_nwa482=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/White/203084_beam.root","read");time_nwa482 = 43368; 
    fint_dew=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/DEW12007/203079_beam.root","read");time_dew = 17367;    
    fint_dewbar=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/DEW12007_bar/203089_beam.root","read");time_dewb = 44846;           
    fint_dewbar35=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/DEW12007_bar_35MeV/203095_beam.root","read");time_dewb35 = 37233; 
    fint_bkg=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Blank_dsl20/203092_beam.root","read"); time_b = 10853;                 

    h_dew_f=fint_dewbar.Get("Energy")
    h_dew_c=fint_dewbar35.Get("Energy")
    h_dew_b=fint_dew.Get("Energy")
    hb=fint_bkg.Get("Energy")

    h_dew_f.Scale(1./time_dewb)
    h_dew_c.Scale(1./time_dewb35)
    h_dew_b.Scale(1./time_dew)
    hb.Scale(1./time_b)

    h_dew_f.SetTitle(";Energy [keV];Counts/sec./25 eV")
    h_dew_f.SetLineColorAlpha(2,0.9)
    h_dew_f.SetLineWidth(1)
    h_dew_f_low=h_dew_f.Clone()
    h_dew_f_high=h_dew_f.Clone()

    h_dew_c.SetTitle(";Energy [keV];Counts/sec./25 eV")
    h_dew_c.SetLineColorAlpha(4,0.7)
    h_dew_c.SetLineWidth(1)
    h_dew_c_low=h_dew_c.Clone()
    h_dew_c_high=h_dew_c.Clone()

    h_dew_b.SetTitle(";Energy [keV];Counts/sec./25 eV")
    h_dew_b.SetLineColorAlpha(ROOT.kSpring-1,1)
    h_dew_b.SetLineWidth(1)
    h_dew_b_low=h_dew_b.Clone()
    h_dew_b_high=h_dew_b.Clone()

    hb.SetTitle(";Energy [keV];Counts/sec./25 eV")
    hb.SetLineColorAlpha(1,0.7)
    hb.SetLineWidth(1)
    hb_low=hb.Clone()
    hb_high=hb.Clone()

    leg = ROOT.TLegend(.75,.65,.90,.90)
    leg.SetFillColor(0)
    leg.SetLineColor(0)
    leg.SetBorderSize(0)
    leg.AddEntry(h_dew_f,"Front","l") 
    leg.AddEntry(h_dew_c,"Center","l") 
    leg.AddEntry(h_dew_b,"Back","l") 
#    leg.AddEntry(hb,"blank","l") 

    c1=ROOT.TCanvas("c1","c1",2400,1200)
    c1.cd()
    h_dew_b.Draw("hist")
    h_dew_f.Draw("hist same")
    h_dew_c.Draw("hist same")
    #hb.Draw("hist same")
    leg.Draw("same")
    c1.Print("/Users/chiu.i-huan/Desktop/temp_output/MaPS_paper_long_es_{}.pdf".format(cv_name))

    c2=ROOT.TCanvas("c2","c2",1200,1000)
    c2.cd()
    ROOT.gPad.SetLogy(1)
    h_dew_b_low.GetXaxis().SetRangeUser(40,100)
    h_dew_b_low.Draw("hist")
    h_dew_f_low.Draw("hist same")
    h_dew_c_low.Draw("hist same")
    #hb_low.Draw("hist same")
    c2.Print("/Users/chiu.i-huan/Desktop/temp_output/MaPS_paper_low_es_{}.pdf".format(cv_name))

    c3=ROOT.TCanvas("c1","c1",1200,1000)
    c3.cd()
    ROOT.gPad.SetLogy(1)
    h_dew_b_high.GetXaxis().SetRangeUser(130,180)
    h_dew_b_high.Draw("hist")
    h_dew_f_high.Draw("hist same")
    h_dew_c_high.Draw("hist same")
    #hb_high.Draw("hist same")
    c3.Print("/Users/chiu.i-huan/Desktop/temp_output/MaPS_paper_high_es_{}.pdf".format(cv_name))
    
if __name__=="__main__":

#    plot_spec() # dew vs. blank
#    plot_eff()
    plot_curve()
#    plot_mixture()
#    plot_spec_nwa() # nwa vs. blank
#    plot_spec_dew() # dew, front, center, back
    
