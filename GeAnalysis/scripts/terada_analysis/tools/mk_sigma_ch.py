"""
The module to make correction function based on Geant4 simulation
"""
import sys,os,ROOT,ctypes
sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/terada_analysis/utils')
ROOT.gErrorIgnoreLevel = ROOT.kFatal
ROOT.gROOT.SetBatch(1)

def getsample():
    data_list, fit_list=[],[]

    data_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Al/203081_beam.root")
    fit_list.append([22,26])
    data_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Al/203081_beam.root")
    fit_list.append([65.5,67.5])
    data_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Fe/203082_beam.root")
    fit_list.append([92,94])
    data_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Fe/203082_beam.root")
    fit_list.append([124,128])
    data_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Ti/203080_beam.root")
    fit_list.append([65,68])
    data_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/DEW12007/203079_beam.root")
    fit_list.append([132,136])
    data_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/DEW12007/203079_beam.root")
    fit_list.append([74,78])
    data_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/DEW12007/203079_beam.root")
    fit_list.append([62,68])
    data_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/White/203084_beam.root")
    fit_list.append([32,35])
    data_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/White/203084_beam.root")
    fit_list.append([110,120])
    data_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Black/203086_beam.root")
    fit_list.append([130,135])
    data_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Black/203086_beam.root")
    fit_list.append([41,44])
    data_list.append("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Black/203086_beam.root")
    fit_list.append([32.5,34.5])
    
    return data_list, fit_list
    
def fit(_data,_range):
    graph_list=[ROOT.TGraphAsymmErrors() for i in range(6)]
    for idet in range(6):
       _g = graph_list[idet]
       _g.SetName("gr_ch{}".format(idet+1))
       for i in range(len(_data)):
          intf=ROOT.TFile(_data[i],"read")
          _tree=intf.Get("tree")

          par0,par1,par2,par3,par4 = map(ctypes.c_double, (0,0,0,0,0))
          _tree.Draw("energy >> h_data(8000,0,200)","detID == {}".format(idet+1),"")
          h_data=ROOT.gDirectory.Get("h_data")
          _down, _up=_range[i][0],_range[i][1]
          h_data.GetXaxis().SetRangeUser(_down, _up)
          peak = ROOT.TF1("peak","gaus", _down, _up)
          bkg = ROOT.TF1("bkg","pol1", _down, _up)
          h_data.Fit(peak,"Q")
          h_data.Fit(bkg,"Q")
          par0=peak.GetParameter(0)#constant
          par1=peak.GetParameter(1)#mu
          par2=peak.GetParameter(2)#sigma
          par3=bkg.GetParameter(0)
          par4=bkg.GetParameter(1)
          total = ROOT.TF1("total","gaus(0)+pol1(3)", _down, _up)
          total.SetParameters(par0,par1,par2,1,0)
          h_data.Fit(total,"Q")
          par0,par1,par2,par3,par4=total.GetParameter(0),total.GetParameter(1),abs(total.GetParameter(2)),total.GetParameter(3),total.GetParameter(4)
          peak.SetParameters(par0,par1,par2)
          bkg.SetParameters(par3,par4)

          c=ROOT.TCanvas("c{}_ch{}".format(i,idet),"c{}_ch{}".format(i,idet),1200,800)
          h_data.SetLineColor(1)
          peak.SetLineColor(4)
          bkg.SetLineColor(3)
          total.SetLineColor(2)
          h_data.SetMinimum(0)
          h_data.SetTitle(";Energy [keV];Counts/25 eV")
          h_data.GetXaxis().CenterTitle(); h_data.GetYaxis().CenterTitle();
          h_data.Draw("ep")
          peak.Draw("same")
          total.Draw("same")
          bkg.Draw("same")
          c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_check_sigma_sample{0}_ch{1}.png".format(i,idet+1))

          # make graph
          _g.SetPoint(i,par1,par2)
          _g.SetPointError(i,total.GetParError(1),total.GetParError(1),total.GetParError(2),total.GetParError(2))

    return graph_list
    
def plot(_graph):
    fout = ROOT.TFile("../auxfile/fline_sigma_ch.root","recreate")
    fout.cd()
    for ich in range(6):
       _g=_graph[ich]
       fline=ROOT.TF1("fline_ch{}".format(ich+1),"TMath::Sqrt([0]*[0]+[1]*[1]*x+[2]*[2]*x*x)",0,200)
       _g.Fit("fline_ch{}".format(ich+1),"qn")
       fline.Write()
       _g.Write()

       c=ROOT.TCanvas("c_sigma_ch{}".format(ich),"c_sigma_ch{}".format(ich),1200,800)
       _g.SetTitle(";Energy [keV];Sigma [keV]")
       _g.GetXaxis().CenterTitle(); _g.GetYaxis().CenterTitle();
       _g.SetMarkerColor(1)
       fline.SetLineColor(2)
       _g.Draw("ap")
       fline.Draw("same")
       c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_sigma_fit_ch{}.png".format(ich+1))

if __name__=="__main__":
    _data,_fitRange = getsample()
    _graph=fit(_data, _fitRange)
    plot(_graph)
    
