import os,sys,ROOT
from array import array
#sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/terada_analysis/utils')
#from func import fit
from ROOT import *
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

_path="/Users/chiu.i-huan/Desktop/geant4WS/geant4-xrayimage/macro/Terada_sim/G4_MCsample/0501/"
sample_list=["Output_white.root", "Output_black.root", "Output_dew.root", "Output_dew.root", "Output_dewbar35.root"]#dewbar is same with dew
_name=["eff_white","eff_black","eff_dew","eff_dewbar","eff_dewbar35"]

def getLatex(ch, x = 0.85, y = 0.85):
    _t = ROOT.TLatex()
    _t.SetNDC()
    _t.SetTextFont( 62 )
    _t.SetTextColor( 36 )
    _t.SetTextSize( 0.08 )
    _t.SetTextAlign( 12 )
    return _t

def mk_paper_plot():
    _peakaname=["Al43", "Fe54", "Ca43", "Mg32", "Si32", "Al42", "Fe43", "Cu43", "Fegamma", "O21", "O31", "Ca32", "O41"] #index for paper
    _range=[[22.6,23.6], [42.4,43.4], [54.6,54.8], [56.3,56.5], [76,77.4], [88.6,90], [92,93.6], [115,116.4], [125,126.8], [132.8,134.4], [158.4, 158.6], [159,160.7], [166.5,168.5]]

    for isap, isample in enumerate(sample_list):
       f1=ROOT.TFile(_path+isample,"read")
       tree=f1.Get("tree")
       h_base=f1.Get("h1_init_count")

       plot_list = []
       for i in range(6):
          tree.Draw("Hit_Energy*1000 >> h1_temp_{}_{}(800,0,200)".format(_name[isap],i+1),"Det_ID=={}".format(i+1),"")
          h1=ROOT.gDirectory.Get("h1_temp_{}_{}".format(_name[isap],i+1))
          h1.SetTitle(";Energy [keV]; Counts/0.25 keV")
          h1.GetXaxis().CenterTitle(); h1.GetYaxis().CenterTitle();
          h1.SetLineColorAlpha(i+1,0.9)
          _binwidth=h1.GetBinWidth(1) 

          #peak_name_list=["Al43", "Fe54", "Ca43", "Mg32", "Si32", "Al42", "Fe43", "Cu43", "Fegamma", "O21", "O31", "Ca32", "O41"] # name
          peak_name_list=["23.1", "42.9", "54.7", "56.4", "76.7", "89.3", "92.9", "115.7", "125", "133.6", "158.5", "159.6", "167.5"] # energy
          heff_out = ROOT.TH1D("h1_{}_{}".format(_name[isap],i+1),"eff for each peak ID",len(peak_name_list),0,len(peak_name_list))
          for _ipn, _peak_name in enumerate(peak_name_list): heff_out.GetXaxis().SetBinLabel(_ipn+1,_peak_name)
          for _j, ipeak in enumerate(_peakaname):
             myrange=_range[_j]
             mypeak=ipeak
             _bindown, _binup=h1.GetXaxis().FindBin(myrange[0]), h1.GetXaxis().FindBin(myrange[1])
             count=h1.Integral(_bindown,_binup)
             peak_base=h_base.GetBinContent(_j+1)
             heff_out.Fill(_j,count*1000/peak_base)

          _heff_out=ROOT.gDirectory.Get("h1_{}_{}".format(_name[isap],i+1))
          _heff_out.SetLineColorAlpha(i+1,0.9)
          _heff_out.SetMarkerColorAlpha(i+1,0.9)
          if i == 4: _heff_out.SetLineColorAlpha(ROOT.kOrange,0.9)
          plot_list.append(_heff_out)

       # == draw eff. plot for each samples
       c2=ROOT.TCanvas("c3_{}".format(_name[isap]),"c3_{}".format(_name[isap]),1200,1000)
       c2.cd()
       ROOT.gPad.SetLogy(0)
       leg = ROOT.TLegend(.75,.65,.92,.90)
       leg.SetFillColor(0)
       leg.SetLineColor(0)
       leg.SetBorderSize(0)
       for ih in range(len(plot_list)):
          if ih == 0:
             plot_list[ih].SetMinimum(0.0)
             plot_list[ih].SetMaximum(2.0)
             plot_list[ih].GetYaxis().SetNdivisions(5,8,1)
             plot_list[ih].SetTitle(";E [keV]; R^{E} (#times 10^{-3})")
             plot_list[ih].GetYaxis().SetLabelSize(0.050)
             plot_list[ih].GetYaxis().SetTitleSize(0.055)
             plot_list[ih].Draw("* hist L")
          else: plot_list[ih].Draw("* hist L same")
          leg.AddEntry(plot_list[ih],"CH{}".format(ih+1),"l")
       leg.Draw("same")
       c2.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_terada_MCSF_{}.pdf".format(_name[isap]))
   
def mk_input_file():
    fout = ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/terada_analysis/auxfile/output_eff_plots_sp.root","recreate")
    _peakaname=["Fegamma", "Si32", "Al43", "Al42", "Fe54", "Fe43", "Ca43", "Ca32", "Mg32", "Cu43", "O21", "O31", "O41"]#this index is based on the hist. of MC
    _range=[[125,126.8], [76,77.4], [22.6,23.6], [88.6,90], [42.4,43.4], [92,93.6], [54.6,54.8], [159,160.7], [56.3,56.5], [115,116.4], [132.8,134.4], [158.4, 158.6], [166.5,168.5]]

    for isap, isample in enumerate(sample_list):
       f1=ROOT.TFile(_path+isample,"read")
       tree=f1.Get("tree")
       h_base=f1.Get("h1_init_count")

       plot_list = []
       for i in range(6):
          tree.Draw("Hit_Energy*1000 >> h1_temp_{}_{}(800,0,200)".format(_name[isap],i+1),"Det_ID=={}".format(i+1),"")
          h1=ROOT.gDirectory.Get("h1_temp_{}_{}".format(_name[isap],i+1))
          h1.SetTitle(";Energy [keV]; Counts/0.25 keV")
          h1.GetXaxis().CenterTitle(); h1.GetYaxis().CenterTitle();
          h1.SetLineColorAlpha(i+1,0.9)
          _binwidth=h1.GetBinWidth(1) 

          peak_name_list=["Fe#gamma", "Si32", "Al43", "Al42", "Fe54", "Fe43", "Ca43", "Ca32", "Mg32", "Cu43", "O21", "O31", "O41"] # for root file
          #peak_name_list=["Al43", "Fe54", "Ca43", "Mg32", "Si32", "Al42", "Fe43", "Cu43", "Fegamma", "O21", "O31", "Ca32", "O41"] #name
          #peak_name_list=["23.1", "42.9", "54.7", "56.4", "76.7", "89.3", "92.9", "115.7", "125", "133.6", "158.5", "159.6", "167.5"] # for paper
          heff_out = ROOT.TH1D("h1_{}_{}".format(_name[isap],i+1),"eff for each peak ID",len(peak_name_list),0,len(peak_name_list))
          for _ipn, _peak_name in enumerate(peak_name_list): heff_out.GetXaxis().SetBinLabel(_ipn+1,_peak_name)
          for _j, ipeak in enumerate(_peakaname):
             myrange=_range[_j]
             mypeak=ipeak
             _bindown, _binup=h1.GetXaxis().FindBin(myrange[0]), h1.GetXaxis().FindBin(myrange[1])
             count=h1.Integral(_bindown,_binup)
             peak_base=h_base.GetBinContent(_j+1)
             heff_out.Fill(_j,count/peak_base)

          _heff_out=ROOT.gDirectory.Get("h1_{}_{}".format(_name[isap],i+1))
          _heff_out.SetLineColorAlpha(i+1,0.9)
          _heff_out.SetMarkerColorAlpha(i+1,0.9)
          if i == 4: _heff_out.SetLineColorAlpha(ROOT.kOrange,0.9)
          fout.cd()
          h1.Write()
          _heff_out.Write()
          plot_list.append(_heff_out)

       # == draw eff. plot for each samples
       c2=ROOT.TCanvas("c2_{}".format(_name[isap]),"c2_{}".format(_name[isap]),1200,1000)
       c2.cd()
       ROOT.gPad.SetLogy(0)
       leg = ROOT.TLegend(.75,.65,.92,.90)
       leg.SetFillColor(0)
       leg.SetLineColor(0)
       leg.SetBorderSize(0)
       for ih in range(len(plot_list)):
          if ih == 0:
             plot_list[ih].SetMinimum(0.0*10e-3)
             plot_list[ih].SetMaximum(2.0*10e-3)
             plot_list[ih].GetYaxis().SetNdivisions(5,8,1)
             plot_list[ih].SetTitle(";E [keV]; R^{E}")
             plot_list[ih].GetYaxis().SetLabelSize(0.050)
             plot_list[ih].GetYaxis().SetTitleSize(0.055)
             plot_list[ih].Draw("* hist L")
          else: plot_list[ih].Draw("* hist L same")
          leg.AddEntry(plot_list[ih],"CH{}".format(ih+1),"l")
       leg.Draw("same")
       c2.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_terada_MCSF_{}_forROOT.pdf".format(_name[isap]))

if __name__=="__main__":
   # for make root file

   #mk_input_file()
   mk_paper_plot()

