import sys, os, ROOT
from array import array
import numpy as np
ROOT.gErrorIgnoreLevel = ROOT.kFatal
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

momentum_range=[15,45]

def plot_pos(_data):
    gr,gr_sigma=ROOT.TGraph(), ROOT.TGraph()
    fint = ROOT.TFile(_data,"read")
    tint = fint.Get("tree")
    results=[]
    for _m in range(momentum_range[0],momentum_range[1]+1):
       tint.Draw("muSampleEndPolZ >> h1(500,0,5)","muInitpZ == {}".format(_m),"")
       h1=ROOT.gDirectory.Get("h1")
       results.append([h1.GetMean(), h1.GetStdDev()])
       print(h1.GetMean(), h1.GetStdDev())

    for ir, result in enumerate(results):
       gr.SetPoint(ir,momentum_range[0]+ir,result[0])
       gr_sigma.SetPoint(ir, momentum_range[0]+ir, result[0]+result[1])
#       print(ir, momentum_range[0]+ir, result[0]+result[1])

    results.reverse()
    for ir, result in enumerate(results):
       gr_sigma.SetPoint(len(results)+ir, momentum_range[1]-ir, result[0]-result[1])
       if ir == len(results)-1: gr_sigma.SetPoint(len(results)+ir+1, momentum_range[1]-ir, result[0]+result[1])
    gr.SetTitle(";Muon momentum [MeV/c]; Stop position [mm]")
    gr.SetMinimum()
    return [gr,gr_sigma] 

def osawa_ryugu():
    y=np.array([0.10,0.12,0.15,0.19,0.22,0.27,0.32,0.38,0.44,0.51,0.59,0.68,0.77,0.88,1.00,1.12,1.26,1.41,1.57,1.74,1.92,2.12,2.34,2.56,2.80,3.06,3.33,3.62,3.92,4.24,4.58])
    x=np.array(range(15,46),dtype=float)
    gr,gr_sigma=ROOT.TGraph(len(x),x,y), ROOT.TGraph()
    return [gr,gr_sigma]

def osawa_NWA482():
    y=np.array([0.06,0.07,0.09,0.11,0.13,0.16,0.19,0.22,0.26,0.30,0.35,0.40,0.45,0.52,0.58,0.66,0.74,0.82,0.92,1.02,1.13,1.24,1.37,1.50,1.64,1.79,1.95,2.12,2.30,2.48,2.68])
    x=np.array(range(15,46),dtype=float)
    gr,gr_sigma=ROOT.TGraph(len(x),x,y), ROOT.TGraph()
    return [gr,gr_sigma]

def mk_cv(_name_list,_gr_list):
    c=ROOT.TCanvas("c{}".format(1),"c{}".format(1),1200,1000)
    c.cd()
    leg = ROOT.TLegend(.20,.6,.42,.90)
    leg.SetFillColor(0)
    leg.SetLineColor(0)
    leg.SetBorderSize(0)
    gr_ori=ROOT.TGraphAsymmErrors()
    gr_ori.SetPoint(0,0,0)
    gr_ori.SetMinimum(0);gr_ori.SetMaximum(5)
    gr_ori.Draw()
    for i, (_name, _gr) in enumerate(zip(_name_list,_gr_list)):
       _gr[0].SetLineColor(i+1)
       _gr[0].SetMarkerColor(i+1)
       _gr[0].SetMarkerStyle(0)
       _gr[1].SetLineColorAlpha(0,0)
       _gr[1].SetFillColorAlpha(5,0.3)
       if i == 4: 
          _gr[0].SetLineColor(ROOT.kOrange) 
          _gr[0].SetMarkerColor(ROOT.kOrange)
       if i == 0: 
          _gr[0].SetMinimum(0);_gr[0].SetMaximum(5)
          _gr[0].Draw()
          leg.AddEntry(_gr[1],"1-#sigma","F") 
       else: _gr[0].Draw("SAME")
       _gr[1].Draw("F SAME")
       leg.AddEntry(_gr[0],"{}".format(_name),"l") 
    leg.Draw("same")

    c.Print("/Users/chiu.i-huan/Desktop/temp_output/cv_meteorite.pdf")
    fout = ROOT.TFile("/Users/chiu.i-huan/Desktop/temp_output/tarada_mc_stopmuon.root","recreate")
    fout.cd()
    for i,(_g,_n) in enumerate(zip(_gr_list,_name_list)):
       _g[0].SetName(_n)
       _g[1].SetName(_n+"_sigma")
       _g[0].Write()
       _g[1].Write()
    print("output : /Users/chiu.i-huan/Desktop/temp_output/tarada_mc_stopmuon.root")

if __name__=="__main__":

    name_list=["white","black","dew","Ryugu","Ryugu_Osawa","NWA482_Osawa"]
    gr_list=[]
    gr_list.append(plot_pos("./G4_MC_stop/Output_white.root"))
    gr_list.append(plot_pos("./G4_MC_stop/Output_black.root"))
    gr_list.append(plot_pos("./G4_MC_stop/Output_dew.root"))
    gr_list.append(plot_pos("./G4_MC_stop/Output_ryugu.root"))
    gr_list.append(osawa_ryugu())
    gr_list.append(osawa_NWA482())

    mk_cv(name_list, gr_list)
   
