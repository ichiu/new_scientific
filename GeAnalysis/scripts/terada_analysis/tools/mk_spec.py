import sys, os, ROOT
sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/terada_analysis/utils')
from array import array
from sample import GetSample
ROOT.gErrorIgnoreLevel = ROOT.kFatal
__location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
ROOT.gROOT.LoadMacro( __location__+'/AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()
#ROOT.gROOT.SetBatch(1)

def plot_spec(_data):
    index,maxbin=1,0
    xray_line=[[23.04,42.78],
    [54.65, 56.09, 76.42,88.91,92.49],
    [115.13],
    [156.29]]
    hist_list,time_list,color_list,name_list=[],[35702, 43368, 17367, 44846, 37233],[2,4,ROOT.kSpring, ROOT.kSpring-7, 6],[]
    for _name in _data:
       _path=_data[_name]
       f=ROOT.TFile(_path,"read")
       _tree=f.Get("tree")
       _tree.Draw("energy >> h{}(8000,0,200)".format(index),"","")
       htemp=ROOT.gDirectory.Get("h{}".format(index))
       htemp.SetDirectory(0); htemp.SetStats(0);
       htemp.Scale(1./time_list[index-1])
       if htemp.GetMaximum() > maxbin: maxbin = htemp.GetMaximum()
       htemp.SetTitle(";Energy [keV]; Counts/25 eV/sec.")
       hist_list.append(htemp)
       name_list.append(_name)
       index+=1

    leg=ROOT.TLegend(.35,.65,.55,.90)
    for i in range(4):
       _line=xray_line[i]
       c=ROOT.TCanvas("c{}".format(i),"c{}".format(i),1200,1000)
       _down,_up=0+i*50,50+i*50
       draw_option,_index = "",0
       for h1 in hist_list:
          h1.GetXaxis().SetRangeUser(_down, _up)
          h1.SetLineWidth(1)
          h1.SetLineColorAlpha(color_list[_index],0.9-0.1*_index)
          if i == 0: leg.AddEntry(h1,  name_list[_index], "l")
          h1.SetMaximum(maxbin*1.1)
          if _index != 0 : draw_option = " same"
          h1.Draw("hist "+draw_option)
          _index+=1
       for iline in _line:
          print(iline)
          line=ROOT.TLine(iline,0,iline,maxbin*1.1)
          line.SetLineWidth(1)
          line.SetLineColor(1)
          #line.Draw("same")
       leg.Draw("same")
       c.Print("/Users/chiu.i-huan/Desktop/plot_terada_spec{}.pdf".format(i))
    return 1

if __name__=="__main__":
    _data, _ = GetSample()
    flag_plot=plot_spec(_data)
    
