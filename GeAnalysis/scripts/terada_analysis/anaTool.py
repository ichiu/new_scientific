"""
This module has several parts for Terada analysis.
0. Get data list and fitting peaks
1. Fit J-PARC data to get intensity of the selected peaks.
2. Apply correction factor for each peaks.
3. Make final calibration curve
"""

import os,sys,ROOT,ctypes,argparse
sys.path.append('/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/terada_analysis/utils')
from ROOT import TMath
from sample import GetSample
from fit import Fit
from corr import Correction
from curve import mkCurve

ROOT.gROOT.SetBatch(1)

if __name__=="__main__":
   parser = argparse.ArgumentParser(description='Process some integers.')
   parser.add_argument( "-r", "--refit", type=bool, default=0, help="redo fitting")
   args = parser.parse_args()
   if args.refit: 
      _data, _peaks = GetSample()
      _outfile = Fit(_data, _peaks)
   else:
      _outfile = ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/terada_analysis/auxfile/fitting_result.root","read")
   _outfile_corr = Correction(args,_outfile)
   mkCurve(inputfile=_outfile_corr)
