"""
peaks fitting and correlation application
"""
import os,ROOT,ctypes
import numpy as np
from func import fit, fit_si32, fit_fe43, fit_ca32, fit_mg32, fit_al42, fit_al43
ROOT.gErrorIgnoreLevel = ROOT.kFatal
ROOT.gROOT.ProcessLine(
"struct RootStruct {\
  Char_t FileName[65];\
  Char_t PeakName[65];\
  Int_t  sample_ID;\
  Int_t  peak_ID;\
  Int_t  ch;\
  Double_t  peak;\
  Double_t  peak_e;\
  Double_t  count;\
  Double_t  eff;\
  Double_t  eff_e;\
  Double_t  sigma;\
  Double_t  sigma_e;\
  Double_t  error;\
  Double_t  cut;\
  Double_t  si32count;\
};"
)
from ROOT import RootStruct
struct = RootStruct()


def Fit(_data,_peaks):
    """
    loop data, ch, peaks
    _name : black, white, dew, dewbar, dewbar35
    idet+1 : 1~6
    _peakname : Fegamma, Si32, Al43, Al42, Fe54, Fe43, Ca43, Ca32, Ca32, Mg32, Cu43, O21, O31, O41
    -> make an output file to store all fitting results in "./fitting_result.root"
    """

    print("====================Original fit=================")
    fint_sigma=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/terada_analysis/auxfile/fline_sigma_ch.root","read")
    fline_sigma_list=[ fint_sigma.Get("fline_ch{}".format(i+1)) for i in range(6)]
 
    f_out=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/terada_analysis/auxfile/fitting_result.root","recreate")
    tout = ROOT.TTree('tree','tree')
    tout.SetDirectory(0)
    tout.Branch( 'FileName', ROOT.addressof( struct, 'FileName' ),  'FileName/C' )
    tout.Branch( 'PeakName', ROOT.addressof( struct, 'PeakName' ),  'PeakName/C' )
    tout.Branch( 'sample_ID', ROOT.addressof( struct, 'sample_ID' ),  'sample_ID/I' )
    tout.Branch( 'peak_ID', ROOT.addressof( struct, 'peak_ID' ),  'peak_ID/I' )
    tout.Branch( 'ch', ROOT.addressof( struct, 'ch' ),  'ch/I' )
    tout.Branch( 'peak', ROOT.addressof( struct, 'peak' ),  'peak/D' )
    tout.Branch( 'peak_e', ROOT.addressof( struct, 'peak_e' ),  'peak_e/D' )
    tout.Branch( 'count', ROOT.addressof( struct, 'count' ),  'count/D' )
    tout.Branch( 'eff', ROOT.addressof( struct, 'eff' ),  'eff/D' )
    tout.Branch( 'eff_e', ROOT.addressof( struct, 'eff_e' ),  'eff_e/D' )
    tout.Branch( 'sigma', ROOT.addressof( struct, 'sigma' ),  'sigma/D' )
    tout.Branch( 'sigma_e', ROOT.addressof( struct, 'sigma_e' ),  'sigma_e/D' )
    tout.Branch( 'error', ROOT.addressof( struct, 'error' ),  'error/D' )
    tout.Branch( 'cut', ROOT.addressof( struct, 'cut' ),  'cut/D' )
    tout.Branch( 'si32count', ROOT.addressof( struct, 'si32count' ),  'si32count/D' )

    struct.sample_ID=-1
    print_peak, print_count, print_eff="", "", ""
    print_peak+="\\begin{sidewaystable} \n\\centering \n\\small \n\\begin{tabular}{|l || c|c|c|c|c|c|c|c|c|c|c|c|} \n\\toprule \n"
    print_count+="\\begin{sidewaystable} \n\\centering \n\\small \n\\begin{tabular}{|l || c|c|c|c|c|c|c|c|c|c|c|} \n\\toprule \n"
    print_eff+="\\begin{sidewaystable} \n\\centering \n\\small \n\\begin{tabular}{|l || c|c|c|c|c|c|c|c|c|c|c|c|} \n\\toprule \n"
    for _name in _data:
       struct.sample_ID+=1
       _path=_data[_name]
       f=ROOT.TFile(_path,"read")
       _tree=f.Get("tree")     
       print_peak+="\multicolumn{1}{c}{} & \multicolumn{12}{c}{%s}\\\ \n\\hline \nName &  Si32&Al43&Al42&Fe54&Fe43&Ca43&Ca32&Mg32&Cu43&O21&O31&O41\\\ \n\\hline \n"%_name
       print_count+="\multicolumn{1}{c}{} & \multicolumn{11}{c}{%s}\\\ \n\\hline \nName &  Si32&Al43&Al42&Fe54&Fe43&Ca43&Ca32&Mg32&O21&O31&O41\\\ \n\\hline \n"%_name
       print_eff+="\multicolumn{1}{c}{} & \multicolumn{12}{c}{%s}\\\ \n\\hline \nEff. (X/Si32) &  Si32&Al43&Al42&Fe54&Fe43&Ca43&Ca32&Mg32&Cu43&O21&O31&O41\\\ \n\\hline \n"%_name
       for idet in range(6):
          _tree.Draw("energy_ori >> h_data(4000,0,200)","detID == {}".format(idet+1),"")
          h_data=ROOT.gDirectory.Get("h_data")
          h_data.SetLineColorAlpha(2,0.9)

          struct.peak_ID,CutforPeaks,Count_Si32=-1,0,[1,1]# reset for each peak loop
          print_peak+="CH{}".format(idet+1)
          print_count+="CH{}".format(idet+1)
          print_eff+="CH{}".format(idet+1)
          for _peakname in _peaks: 
             r_down, r_up = _peaks[_peakname][0], _peaks[_peakname][1]
             _htemp=h_data.Clone()

             # === fit ===
             if _peakname=="Si32" : f_peak, f_sigma, f_count, f_error = fit_si32(_htemp, r_down, r_up, _name, idet+1, _peakname, fline_sigma_list[idet])
             elif _peakname=="Mg32" : f_peak, f_sigma, f_count, f_error = fit_mg32(_htemp, r_down, r_up, _name, idet+1, _peakname, fline_sigma_list[idet])
             elif _peakname=="Ca32" : f_peak, f_sigma, f_count, f_error = fit_ca32(_htemp, r_down, r_up, _name, idet+1, _peakname, fline_sigma_list[idet])
             elif _peakname=="Al42" : f_peak, f_sigma, f_count, f_error = fit_al42(_htemp, r_down, r_up, _name, idet+1, _peakname, fline_sigma_list[idet])
             elif _peakname=="Al43" : f_peak, f_sigma, f_count, f_error = fit_al43(_htemp, r_down, r_up, _name, idet+1, _peakname, fline_sigma_list[idet])
             elif _peakname=="Fe43" : f_peak, f_sigma, f_count, f_error = fit_fe43(_htemp, r_down, r_up, _name, idet+1, _peakname, fline_sigma_list[idet])
             else: f_peak, f_sigma, f_count, f_error = fit(_htemp, r_down, r_up, _name, idet+1, _peakname, fline_sigma_list[idet])

             # === save info. to tree ===
             if _peakname == "Si32" : Count_Si32=[f_count,f_error.value];# 2nd peak
             struct.FileName=_name
             struct.PeakName=_peakname
             struct.peak_ID+=1
             struct.ch=idet+1
             struct.peak=f_peak[0]
             struct.peak_e=f_peak[1]
             struct.count=f_count
             struct.sigma=f_sigma[0]
             struct.sigma_e=f_sigma[1]
             struct.error=f_error.value
             struct.si32count=Count_Si32[0]
             struct.eff=f_count/struct.si32count# eff is not calculated for Fe-gamma

             # === specified fit result ===
             if _peakname == "Fegamma":# for 1st peak; eff. was not calculated
                struct.eff_e = 0; struct.eff = 0 
                CutforPeaks = f_count 
             if _peakname == "Ca43" or _peakname == "Ca32": struct.cut = CutforPeaks # gamma cut only for Ca
             else : struct.cut = 0
             if f_count == 0 : struct.eff_e = 0. # in the case of divide zero
             else: struct.eff_e=struct.eff*np.sqrt((np.square(f_error.value/f_count)+np.square(Count_Si32[1]/Count_Si32[0])))

             tout.Fill()

             # === print latex ===
             if _peakname != "Fegamma": 
                if struct.sigma > 0.5: print_peak+="&---$\pm$---"
                else: print_peak+="&{:.1f}$\pm${:.1f}".format(struct.peak, struct.sigma)
             if _peakname != "Fegamma" and _peakname != "Cu43": print_count+="&{:.1f}$\pm${:.1f}".format(struct.count, struct.error)    
             if _peakname != "Fegamma": print_eff+="&{:.2f}$\pm${:.2f}".format(struct.eff, struct.eff_e)    
          print_peak+="\\\ \n" 
          print_count+="\\\ \n"
          print_eff+="\\\ \n" 
       if struct.sample_ID+1 != len(_data):#not last
          print_peak+="\\midrule \n"
          print_count+="\\midrule \n"
          print_eff+="\\midrule \n"
       else: 
          print_peak+="\\bottomrule \n\\end{tabular}\n\\caption{\\label{tables:peak_result}\n}\n\\end{sidewaystable}"
          print_count+="\\bottomrule \n\\end{tabular}\n\\caption{\\label{tables:count_ori}\n}\n\\end{sidewaystable}"
          print_eff+="\\bottomrule \n\\end{tabular}\n\\caption{\\label{tables:eff_ori_result}\n}\n\\end{sidewaystable}"

    print("\n%=== Peaks (Table-1) ===")
    print(print_peak)
    print("\n%=== Counts (Table-2)===")
    print(print_count)
    print("\n%=== Eff. (Table-3) ===")
    print(print_eff)

    f_out.cd()
    tout.Write()
    return f_out
