"""
Define sample list by users.
"""

def GetSample():
    data_file, peaks_list=dict(),dict()
    data_file.update({"black":"/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Black/203086_beam.root"})
    data_file.update({"white":"/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/White/203084_beam.root"})
    data_file.update({"dew":"/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/DEW12007/203079_beam.root"})
    data_file.update({"dewbar":"/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/DEW12007_bar/203089_beam.root"})
    data_file.update({"dewbar35":"/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/DEW12007_bar_35MeV/203095_beam.root"})

#    data_file.update({"Al":"/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Al/203081_beam.root"})#spectial case for Al43, Al42
#    data_file.update({"Fe":"/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Fe/203082_beam.root"})#spectial case for Fe54, Fe43

    peaks_list.update({"Fegamma":[124,127]})
    peaks_list.update({"Si32":[74,79]})
    peaks_list.update({"Al43":[21,26]})
    peaks_list.update({"Al42":[86,92]})
    peaks_list.update({"Fe54":[42,44]})
    peaks_list.update({"Fe43":[91,95]})
    peaks_list.update({"Ca43":[54,55.6]})
    peaks_list.update({"Ca32":[153,160]})
    peaks_list.update({"Mg32":[52,57.5]})
    peaks_list.update({"Cu43":[113,119]})
    peaks_list.update({"O21":[131,135]})
    peaks_list.update({"O31":[157.5,161]})
    peaks_list.update({"O41":[165,168]})
    return data_file, peaks_list
