"""
Correlation applicarion based on MC (sample and detector)
and Branch addition for TTree
"""
import os,ROOT,ctypes
import numpy as np
ROOT.gROOT.ProcessLine(
"struct RootStruct_Corr {\
  Double_t  count_final;\
  Double_t  error_final;\
  Double_t  eff_final;\
};"
)
from ROOT import RootStruct_Corr
struct_corr = RootStruct_Corr()

correction_file_MC="/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/terada_analysis/auxfile/output_eff_plots_sp.root"# from G4 simulation
output_path="/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/scripts/terada_analysis/auxfile/intensity_sum.root"

def Correction(args, _fint):
    print("\n==================== Correction Sample =================")
    _tree=_fint.Get("tree") 
    f_temp=ROOT.TFile(correction_file_MC,"read")
    f_out=ROOT.TFile(output_path,"recreate")
    tout = _tree.CloneTree(0)
    tout.SetDirectory(0)
    tout.Branch( 'count_final', ROOT.addressof( struct_corr, 'count_final' ),  'count_final/D' )
    tout.Branch( 'error_final', ROOT.addressof( struct_corr, 'error_final' ),  'error_final/D' )
    tout.Branch( 'eff_final', ROOT.addressof( struct_corr, 'eff_final' ),  'eff_final/D' )

    loop_entries=_tree.GetEntries()
    print_count="\\begin{sidewaystable} \n\\centering \n\\small \n\\begin{tabular}{|l || c|c|c|c|c|c|c|c|c|c|c|} \n\\toprule \n"
    print_eff="\\begin{sidewaystable} \n\\centering \n\\small \n\\begin{tabular}{|l || c|c|c|c|c|c|c|c|c|c|c|} \n\\toprule \n"
    n_sample,n_ch,n_peaks=5,6,13
    for _i in range(loop_entries):
       _tree.GetEntry(_i)
       h1=f_temp.Get("h1_eff_{}_{}".format(_tree.FileName,_tree.ch))

       # === Scale-2 : specified energy region ===
       _ratio_Si32=h1.GetBinContent(2)
       _ratio_gamma=h1.GetBinContent(1)
       _ratio=h1.GetBinContent(_tree.peak_ID+1)# peak_ID is from 0
          
       # === Scale-3 : no scale ===
       #_ratio=1
       #_ratio_gamma=1
       #_ratio_Si32=1

       print("Name: {}, peak : {:.2f}, ratio : {:.2f}".format(_tree.PeakName,_tree.peak, _ratio))
       if _ratio_gamma == 0: _ratio_gamma =1 
       if _ratio == 0: _ratio =1 

       struct_corr.count_final=_tree.count/_ratio-_tree.cut*(0.172*_ratio/_ratio_gamma) # scale factor of gamma-ray
       struct_corr.error_final=_tree.error/_ratio # scale factor of error
       struct_corr.eff_final=struct_corr.count_final/(_tree.si32count/_ratio_Si32)
       tout.Fill()

       # === print latex ===
       if _i%(n_peaks*n_ch) == 0 : # for sample loop
          print_count+="\multicolumn{1}{c}{} & \multicolumn{11}{c}{%s}\\\ \n\\hline \nName &  Si32&Al43&Al42&Fe54&Fe43&Ca43&Ca32&Mg32&O21&O31&O41\\\ \n\\hline \n"%_tree.FileName
          print_eff+="\multicolumn{1}{c}{} & \multicolumn{11}{c}{%s}\\\ \n\\hline \nEff. (X/Si32) &  Si32&Al43&Al42&Fe54&Fe43&Ca43&Ca32&Mg32&O21&O31&O41\\\ \n\\hline \n"%_tree.FileName
       if _i%n_peaks == 0 : # for new channel loop
          print_count+="CH{}".format(_tree.ch)
          print_eff+="CH{}".format(_tree.ch)
          peak_index=0
       if _tree.PeakName != "Fegamma" and _tree.PeakName != "Cu43": # for peak loop 
          print_count+="&{:.1f}$\pm${:.1f}".format(struct_corr.count_final/43368., struct_corr.error_final/43368.)# for proceeding
          #print_count+="&{:.1f}".format(struct_corr.count_final)
          print_eff+="&{:.2f}$\pm${:.2f}".format(struct_corr.eff_final, _tree.eff_e)
          peak_index+=1
       if (_i+1)%n_peaks == 0 : # check the next event
          print_count+="\\\ \n"
          print_eff+="\\\ \n"
       if  (_i+1)%(n_peaks*n_ch) == 0:
         if _tree.sample_ID+1 !=5: #_tree.sample_ID+1 is 1,2,3,4,5
            print_count+="\\midrule \n"
            print_eff+="\\midrule \n"
         else:
            print_count+="\\bottomrule \n\\end{tabular}\n\\caption{\\label{tables:count_result}\n}\n\\end{sidewaystable}"
            print_eff+="\\bottomrule \n\\end{tabular}\n\\caption{\\label{tables:eff_corr_result}\n}\n\\end{sidewaystable}"
    print("\n%=== Corr. Counts (Table-4)===")
    print(print_count)
    print("\n%=== Corr. Eff. (Table-5)===")
    print(print_eff)
       
    f_out.cd()
    tout.Write()
    return f_out

