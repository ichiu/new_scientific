"""
peaks fitting and correlation application
"""
import os,ROOT,ctypes
import numpy as np
from ROOT import TMath
ROOT.gErrorIgnoreLevel = ROOT.kFatal
ROOT.gROOT.LoadMacro('../AtlasStyle/AtlasStyle.C')
ROOT.SetAtlasStyle()

def fit(_h,_down,_up,_n1,_n2,_n3, fline_sigma):
    """
    Normal fit
    _n1="sample" _n2="ch" _n3="peak"
    """
    n_bins_b=_h.GetXaxis().GetNbins()
    bad_fit_thre, bad_flag=0.7, False
    par0,par1,par2,par3,par4 = map(ctypes.c_double, (0,0,0,0,0))
    _h.GetXaxis().SetRangeUser(_down,_up)
    peak = ROOT.TF1("peak","gaus", _down, _up)
    bkg = ROOT.TF1("bkg","pol1", _down, _up)
    _h.Fit(peak,"Q")
    _h.Fit(bkg,"Q")
    par0=peak.GetParameter(0)
    par1=peak.GetParameter(1)
    par2=peak.GetParameter(2)
    par3=bkg.GetParameter(0)
    par4=bkg.GetParameter(1)

    total = ROOT.TF1("total","gaus(0)+pol1(3)", _down, _up) 
    total.SetParameters(par0,par1,par2,1,0)
    _h.Fit(total,"Q")     
    if abs(total.GetParameter(2)) > bad_fit_thre: bad_flag=True
    total.SetParLimits(0,0,100000)
    total.FixParameter(2,fline_sigma.Eval(par1))#TODO not fixed sigma
    _h.Fit(total,"Q") #TODO re-fit
    par0,par1,par2,par3,par4=total.GetParameter(0),total.GetParameter(1),abs(total.GetParameter(2)),total.GetParameter(3),total.GetParameter(4)
    peak.SetParameters(par0,par1,par2)
    bkg.SetParameters(par3,par4)

    # === integral and find error ===
    _binwidth, _count, _error=_h.GetBinWidth(1), 0, ctypes.c_double(0)
    _bindown, _binup=_h.GetXaxis().FindBin(par1-3*par2), _h.GetXaxis().FindBin(par1+3*par2)
    if ((peak.Integral(par1-3*par2,par1+3*par2)/_binwidth) < _h.Integral(_bindown,_binup)): # good fitting
       _count=peak.Integral(par1-3*par2,par1+3*par2)/_binwidth
    _sum_temp=_h.IntegralAndError(_bindown,_binup,_error,"error")

    # === print ===
    print("{0:5} CH{1:<3} {2:7} | Peak : {3:.2f} | Sigma : {4:.2f} | Intensity : {5:.1f} | Error : {6:.1f}".format(_n1,_n2,_n3,par1,par2,_count,_error.value))

    # === make plot ===
    c=ROOT.TCanvas("c{}_{}_{}".format(_n1,_n2,_n3),"c{}_{}_{}".format(_n1,_n2,_n3),1200,1000)
    pad1,pad2 = ROOT.TPad("pad1","pad1",0,0.3,1,1), ROOT.TPad("pad2","pad2",0,0.03,1,0.3)
    pad1.SetBottomMargin(0.0)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(0.4)
#    pad2.SetGridy()
    pad2.Draw();pad1.Draw();

    pad1.cd()
    _h.SetLineColor(1)
    #peak.SetLineWidth(1)
    #total.SetLineWidth(1)
    #bkg.SetLineWidth(1)
    peak.SetLineColor(4)
    bkg.SetLineColor(3)
    total.SetLineColor(2)
    _h.SetMinimum((_h.GetMaximum()/25.)*(-0.9))
    _h.SetTitle(";Energy [keV];Counts/{} eV".format(int((200*1000)/n_bins_b)))
    _h.GetXaxis().CenterTitle(); _h.GetYaxis().CenterTitle();
    _h.Draw("ep")
    peak.Draw("same")
    total.Draw("same")
    bkg.Draw("same")

    pad2.cd()
    h_residual=_h.Clone()
#    h_residual.Add(total,-1)
    for i in range(_h.GetXaxis().GetNbins()):
       _xbin,_xmean,_xerror=_h.GetBinContent(i+1), _h.GetBinCenter(i+1),_h.GetBinError(i+1)
       h_residual.SetBinContent(i+1, _xbin/total.Eval(_xmean))
       if _xbin == 0 or total.Eval(_xmean) == 0: h_residual.SetBinError(i+1,0)
       #else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin) + np.square(_error.value/total.Eval(_xmean))))
       else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin)))
    h_residual.SetTitle(";Energy [keV];Data/Model")
    h_residual.GetXaxis().SetTitleSize(35);
    h_residual.GetXaxis().SetTitleFont(43);
    h_residual.GetXaxis().SetTitleOffset(4.);
    h_residual.GetXaxis().SetLabelFont(43);
    h_residual.GetXaxis().SetLabelSize(30);
    h_residual.GetYaxis().SetNdivisions(505);
    h_residual.GetYaxis().SetTitleSize(25);
    h_residual.GetYaxis().SetTitleFont(43);
    h_residual.GetYaxis().SetTitleOffset(1.55);
    h_residual.GetYaxis().SetLabelFont(43);
    h_residual.GetYaxis().SetLabelSize(30);
    h_residual.SetMinimum(0.2)
    h_residual.SetMaximum(2.2)
    h_residual.Draw("p")
    line = ROOT.TLine(_down,1,_up,1)
    line.SetLineColorAlpha(ROOT.kRed, 0.3)
    line.SetLineWidth(2)
    line.Draw("same")

    c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_fit_peak_{2}_ch{1}_{0}.pdf".format(_n1,_n2,_n3))# take a long time

    if bad_flag: return [0,0],[0,0],0,ctypes.c_double(0)
    return [par1,total.GetParError(1)], [abs(par2),total.GetParError(2)], _count, _error #peak , sigma , count, error 

def fit_si32(_h,_down,_up,_n1,_n2,_n3, fline_sigma):
    """
    Si32 fit: two peaks
    Mg42 (76)+Si32(76.7)
    _n1="sample" _n2="ch" _n3="peak"
    """

    n_bins_b=_h.GetXaxis().GetNbins()
    par0,par1,par2,par3,par4,par5,par6,par7,par8,par9,par10 = map(ctypes.c_double, (0,0,0,0,0,0,0,0,0,0,0))
    _h.GetXaxis().SetRangeUser(_down,_up)
    peak_mg = ROOT.TF1("peak_mg","gaus", _down, _up)
    peak_si = ROOT.TF1("peak_si","gaus", _down, _up)
    peak_si_sub = ROOT.TF1("peak_si_sub","gaus", _down, _up)
    bkg = ROOT.TF1("bkg","pol1", _down, _up)

    # === get initial par. ===
    peak_mg.FixParameter(1,75.9)
    peak_mg.FixParameter(2,fline_sigma.Eval(75.7))
    peak_si.FixParameter(2,fline_sigma.Eval(76.5))
    peak_si_sub.FixParameter(2,fline_sigma.Eval(77))
    _h.Fit(peak_mg,"Q","",75.7,76)
    _h.Fit(peak_si,"Q","",76,77.5)
    _h.Fit(peak_si_sub,"Q","",76.8,77.5)
    _h.Fit(bkg,"Q","",_down,_up)
    par0=peak_mg.GetParameter(0)
    par1=peak_mg.GetParameter(1)
    par2=peak_mg.GetParameter(2)
    par3=peak_si.GetParameter(0)
    par4=peak_si.GetParameter(1)
    par5=peak_si.GetParameter(2)
    par6=peak_si_sub.GetParameter(0)
    par7=peak_si_sub.GetParameter(1)
    par8=peak_si_sub.GetParameter(2)
    par9=bkg.GetParameter(0)
    par10=bkg.GetParameter(1)

    total = ROOT.TF1("total","gaus(0)+[3]*TMath::Exp(-0.5*pow(((x-[4])/[5]),2))+([3]*0.5)*TMath::Exp(-0.5*pow(((x-[6])/[7]),2))+pol1(8)", _down, _up) 
    total.SetParameters(par0,par1,par2,par3,par4,par5,par6,par7,par8,par9)
    total.SetParLimits(0,0,par3)#constant
    total.SetParLimits(3,0,par3*2)
    total.SetParLimits(1,75.7,76)#mu
    total.SetParLimits(4,76.5,76.8)
    total.SetParLimits(6,76.8,77.5)
    total.FixParameter(2,fline_sigma.Eval((75.7+76.2)/2.))#sigma
    total.FixParameter(5,fline_sigma.Eval((76.5+76.8)/2.))
    total.FixParameter(7,fline_sigma.Eval((76.8+77.5)/2.))
    _h.Fit(total,"Q") #fit with fixed sigma
    par0=total.GetParameter(0)
    par1=total.GetParameter(1)
    par2=total.GetParameter(2)
    par3=total.GetParameter(3)
    par4=total.GetParameter(4)
    par5=total.GetParameter(5)
    par6=total.GetParameter(6)
    par7=total.GetParameter(7)
    par8=total.GetParameter(8)
    par9=total.GetParameter(9)

    peak_mg.SetParameters(par0,par1,par2)
    peak_si.SetParameters(par3,par4,par5)
    peak_si_sub.SetParameters(par3*0.5,par6,par7)
    bkg.SetParameters(par8,par9)

    # === integral and find error ===
    _binwidth, _count, _error=_h.GetBinWidth(1), 0, ctypes.c_double(0)
    _bindown, _binup=_h.GetXaxis().FindBin(par4-3*par5), _h.GetXaxis().FindBin(par4+3*par5)
    _count=peak_si.Integral(par4-3*par5,par4+3*par5)/_binwidth
    _count+=peak_si_sub.Integral(par6-3*par7,par6+3*par7)/_binwidth
    _sum_temp=_h.IntegralAndError(_bindown,_binup,_error,"error")

    # === make plot ===
    c=ROOT.TCanvas("c{}_{}_{}".format(_n1,_n2,_n3),"c{}_{}_{}".format(_n1,_n2,_n3),1200,1000)
    pad1,pad2 = ROOT.TPad("pad1","pad1",0,0.3,1,1), ROOT.TPad("pad2","pad2",0,0.03,1,0.3)
    pad1.SetBottomMargin(0.0)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(0.4)
#    pad2.SetGridy()
    pad2.Draw();pad1.Draw();

    pad1.cd()
    _h.SetLineColor(1)
    #peak_mg.SetLineWidth(1)
    #peak_si.SetLineWidth(1)
    #peak_si_sub.SetLineWidth(1)
    #total.SetLineWidth(1)
    #bkg.SetLineWidth(1)
    peak_mg.SetLineColor(ROOT.kOrange)
    peak_si.SetLineColor(4)
    peak_si_sub.SetLineColor(4)
    bkg.SetLineColor(3)
    total.SetLineColor(2)
    _h.SetMinimum((_h.GetMaximum()/25.)*(-0.9))
    _h.SetTitle(";Energy [keV];Counts/{} eV".format(int((200*1000)/n_bins_b)))
    _h.GetXaxis().CenterTitle(); _h.GetYaxis().CenterTitle();
    _h.Draw("ep")
    peak_mg.Draw("same")
    peak_si.Draw("same")
    peak_si_sub.Draw("same")
    total.Draw("same")
    bkg.Draw("same")

    pad2.cd()
    h_residual=_h.Clone()
#    h_residual.Add(total,-1)
    for i in range(_h.GetXaxis().GetNbins()):
       _xbin,_xmean,_xerror=_h.GetBinContent(i+1), _h.GetBinCenter(i+1),_h.GetBinError(i+1)
       h_residual.SetBinContent(i+1, _xbin/total.Eval(_xmean))
       if _xbin == 0 or total.Eval(_xmean) == 0: h_residual.SetBinError(i+1,0)
       #else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin) + np.square(_error.value/total.Eval(_xmean))))
       else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin)))
    h_residual.SetTitle(";Energy [keV];Data/Model")
    h_residual.GetXaxis().SetTitleSize(35);
    h_residual.GetXaxis().SetTitleFont(43);
    h_residual.GetXaxis().SetTitleOffset(4.);
    h_residual.GetXaxis().SetLabelFont(43);
    h_residual.GetXaxis().SetLabelSize(30);
    h_residual.GetYaxis().SetNdivisions(505);
    h_residual.GetYaxis().SetTitleSize(25);
    h_residual.GetYaxis().SetTitleFont(43);
    h_residual.GetYaxis().SetTitleOffset(1.55);
    h_residual.GetYaxis().SetLabelFont(43);
    h_residual.GetYaxis().SetLabelSize(30);
    h_residual.SetMinimum(0.2)
    h_residual.SetMaximum(2.2)
    h_residual.Draw("p")
    line = ROOT.TLine(_down,1,_up,1)
    line.SetLineColorAlpha(ROOT.kRed, 0.3)
    line.SetLineWidth(2)
    line.Draw("same")

    c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_fit_peak_{2}_ch{1}_{0}.pdf".format(_n1,_n2,_n3))# take a long time

    # === print ===
    print("{0:5} CH{1:<3} {2:7} | Peak Si : {3:.2f} | Sigma : {4:.2f} | Intensity : {5:.1f} | Error : {6:.1f}".format(_n1,_n2,_n3,par4,par5,_count,_error.value))

    return [par4,total.GetParError(4)], [abs(par5),total.GetParError(5)], _count, _error #main peak , sigma , count, error 


def fit_fe43(_h,_down,_up,_n1,_n2,_n3, fline_sigma):
    """
    Fe43 fit: two peaks with 3 gaus
    Fe43 + unknown
    _n1="sample" _n2="ch" _n3="peak"
    """
    fe43_peaks=[[92.772 , 93.087],[92.704 , 93.020],[92.704 , 93.010],[92.684 , 92.994],[92.689 , 93.042],[92.652 , 92.946]]
    n_bins_b=_h.GetXaxis().GetNbins()
    par0,par1,par2,par3,par4,par5,par6,par7,par8,par9,par10 = map(ctypes.c_double, (0,0,0,0,0,0,0,0,0,0,0))
    _h.GetXaxis().SetRangeUser(_down,_up)
    peak_fe1 = ROOT.TF1("peak_fe1","gaus", _down, _up)
    peak_fe2 = ROOT.TF1("peak_fe2","gaus", _down, _up)
    peak_un = ROOT.TF1("peak_un","gaus", _down, _up)
    bkg = ROOT.TF1("bkg","pol1", _down, _up)

    # === get initial par. ===
    peak_fe1.FixParameter(2,fline_sigma.Eval((_down+_up)/2))#sigma
    peak_fe2.FixParameter(2,fline_sigma.Eval((_down+_up)/2))
    peak_un.FixParameter(2,fline_sigma.Eval((_down+_up)/2))
    peak_fe1.SetParLimits(0,0,1000)#constant
    peak_fe2.SetParLimits(0,0,1000)
    peak_un.SetParLimits(0,0,1000)
    _h.Fit(peak_fe1,"Q","",92.3,93.2)
    _h.Fit(peak_fe2,"Q","",92.3,93.2)
    _h.Fit(peak_un,"Q","",93.6,94.3)
    _h.Fit(bkg,"Q","",_down,_up)
    par0=peak_fe1.GetParameter(0)
    par1=peak_fe1.GetParameter(1)
    par2=peak_fe1.GetParameter(2)
    par3=peak_fe2.GetParameter(0)
    par4=peak_fe2.GetParameter(1)
    par5=peak_fe2.GetParameter(2)
    par6=peak_un.GetParameter(0)
    par7=peak_un.GetParameter(1)
    par8=peak_un.GetParameter(2)
    par9=bkg.GetParameter(0)
    par10=bkg.GetParameter(1)

    total = ROOT.TF1("total","[0]*TMath::Exp(-0.5*pow(((x-[1])/[2]),2))+[0]*TMath::Exp(-0.5*pow(((x-[3])/[4]),2))+gaus(5)+pol1(8)", _down, _up)
    total.SetParameters(par0,par1,par2,par4,par5,par6,par7,par8,par9,par10)
    total.SetParLimits(0,0,par0*2)#constant
    total.SetParLimits(5,0,par0*2)
    total.FixParameter(1,fe43_peaks[_n2-1][0])#mu
    total.FixParameter(3,fe43_peaks[_n2-1][1])
    total.SetParLimits(6,93.7,94.4)
    total.FixParameter(2,fline_sigma.Eval(fe43_peaks[_n2-1][0]))#sigma
    total.FixParameter(4,fline_sigma.Eval(fe43_peaks[_n2-1][1]))
#    if _n1 == "white" and _n2 == 6:
#       total.SetParLimits(2,fline_sigma.Eval(10),fline_sigma.Eval(200))#sigma
#       total.SetParLimits(4,fline_sigma.Eval(10),fline_sigma.Eval(200))
    total.FixParameter(7,fline_sigma.Eval((93.7+94.2)/2.))
    _h.Fit(total,"Q") #fit with fixed sigma
    par0=total.GetParameter(0)
    par1=total.GetParameter(1)
    par2=total.GetParameter(2)
    par3=total.GetParameter(3)
    par4=total.GetParameter(4)
    par5=total.GetParameter(5)
    par6=total.GetParameter(6)
    par7=total.GetParameter(7)
    par8=total.GetParameter(8)
    par9=total.GetParameter(9)

    peak_fe1.SetParameters(par0,par1,par2)
    peak_fe2.SetParameters(par0,par3,par4)
    peak_un.SetParameters(par5,par6,par7)
    bkg.SetParameters(par8,par9)
 
    # === integral and find error ===
    _binwidth, _count, _error=_h.GetBinWidth(1), 0, ctypes.c_double(0)
    _bindown, _binup=_h.GetXaxis().FindBin(par1-3*par2), _h.GetXaxis().FindBin(par1+3*par2)
    _count=peak_fe1.Integral(par1-3*par2,par1+3*par2)/_binwidth
    _count+=peak_fe2.Integral(par3-3*par4,par3+3*par4)/_binwidth
    _sum_temp=_h.IntegralAndError(_bindown,_binup,_error,"error")

    # === make plot ===
    c=ROOT.TCanvas("c{}_{}_{}".format(_n1,_n2,_n3),"c{}_{}_{}".format(_n1,_n2,_n3),1200,1000)
    pad1,pad2 = ROOT.TPad("pad1","pad1",0,0.3,1,1), ROOT.TPad("pad2","pad2",0,0.03,1,0.3)
    pad1.SetBottomMargin(0.0)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(0.4)
#    pad2.SetGridy()
    pad2.Draw();pad1.Draw();

    pad1.cd()
    _h.SetLineColor(1)
    peak_fe1.SetLineColor(4)
    peak_fe2.SetLineColor(4)
    peak_un.SetLineColor(ROOT.kOrange)
    bkg.SetLineColor(3)
    total.SetLineColor(2)
    _h.SetMinimum((_h.GetMaximum()/25.)*(-0.9))
    _h.SetTitle(";Energy [keV];Counts/{} eV".format(int((200*1000)/n_bins_b)))
    _h.GetXaxis().CenterTitle(); _h.GetYaxis().CenterTitle();
    _h.Draw("ep")
    peak_fe1.Draw("same")
    peak_fe2.Draw("same")
    peak_un.Draw("same")
    total.Draw("same")
    bkg.Draw("same")

    pad2.cd()
    h_residual=_h.Clone()
#    h_residual.Add(total,-1)
    for i in range(_h.GetXaxis().GetNbins()):
       _xbin,_xmean,_xerror=_h.GetBinContent(i+1), _h.GetBinCenter(i+1),_h.GetBinError(i+1)
       h_residual.SetBinContent(i+1, _xbin/total.Eval(_xmean))
       if _xbin == 0 or total.Eval(_xmean) == 0: h_residual.SetBinError(i+1,0)
       #else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin) + np.square(_error.value/total.Eval(_xmean))))
       else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin)))
    h_residual.SetTitle(";Energy [keV];Data/Model")
    h_residual.GetXaxis().SetTitleSize(35);
    h_residual.GetXaxis().SetTitleFont(43);
    h_residual.GetXaxis().SetTitleOffset(4.);
    h_residual.GetXaxis().SetLabelFont(43);
    h_residual.GetXaxis().SetLabelSize(30);
    h_residual.GetYaxis().SetNdivisions(505);
    h_residual.GetYaxis().SetTitleSize(25);
    h_residual.GetYaxis().SetTitleFont(43);
    h_residual.GetYaxis().SetTitleOffset(1.55);
    h_residual.GetYaxis().SetLabelFont(43);
    h_residual.GetYaxis().SetLabelSize(30);
    h_residual.SetMinimum(0.2)
    h_residual.SetMaximum(2.2)
    h_residual.Draw("p")
    line = ROOT.TLine(_down,1,_up,1)
    line.SetLineColorAlpha(ROOT.kRed, 0.3)
    line.SetLineWidth(2)
    line.Draw("same")

    c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_fit_peak_{2}_ch{1}_{0}.pdf".format(_n1,_n2,_n3))# take a long time

    # === print ===
    print("{0:5} CH{1:<3} {2:7} | Peak : {3:.2f} | Sigma : {4:.2f} | Intensity : {5:.1f} | Error : {6:.1f}".format(_n1,_n2,_n3,par1,par2,_count,_error.value))
    #TODO check peak centers
    #print("Peak-1 : {0:.3f} | Peak-2 : {1:.3f}".format(par1,par3))

    return [par1,total.GetParError(1)], [abs(par2),total.GetParError(2)], _count, _error #peak , sigma , count, error 

def fit_mg32(_h,_down,_up,_n1,_n2,_n3, fline_sigma):
    """
    Mg32 fit: 4 peaks
    Fe95(52.6) + Cu54(53.2) + Ca43&Fe(54.7) + Mg32(56.1)
    _n1="sample" _n2="ch" _n3="peak"
    """
    n_bins_b=_h.GetXaxis().GetNbins()
    par0,par1,par2, par3,par4,par5, par6,par7,par8, par9,par10,par11, par12,par13 = map(ctypes.c_double, (0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    _h.GetXaxis().SetRangeUser(_down,_up)
    peak_fe = ROOT.TF1("peak_fe","gaus", _down, _up)
    peak_cu = ROOT.TF1("peak_cu","gaus", _down, _up)
    peak_ca = ROOT.TF1("peak_ca","gaus", _down, _up)
    peak_mg = ROOT.TF1("peak_mg","gaus", _down, _up)
    bkg = ROOT.TF1("bkg","pol1", _down, _up)

    # === get initial par. ===
    peak_fe.FixParameter(2,fline_sigma.Eval(55))
    peak_cu.FixParameter(2,fline_sigma.Eval(55))
    peak_ca.FixParameter(2,fline_sigma.Eval(55))
    peak_mg.FixParameter(2,fline_sigma.Eval(55))
    _h.Fit(peak_fe,"Q","",52,53)
    _h.Fit(peak_cu,"Q","",53,54)
    _h.Fit(peak_ca,"Q","",54.5,55.5)
    _h.Fit(peak_mg,"Q","",56,57)
    _h.Fit(bkg,"Q","",_down,_up)
    par0=peak_fe.GetParameter(0)
    par1=peak_fe.GetParameter(1)
    par2=peak_fe.GetParameter(2)
    par3=peak_cu.GetParameter(0)
    par4=peak_cu.GetParameter(1)
    par5=peak_cu.GetParameter(2)
    par6=peak_ca.GetParameter(0)
    par7=peak_ca.GetParameter(1)
    par8=peak_ca.GetParameter(2)
    par9=peak_mg.GetParameter(0)
    par10=peak_mg.GetParameter(1)
    par11=peak_mg.GetParameter(2)
    par12=bkg.GetParameter(0)
    par13=bkg.GetParameter(1)
    par=np.array([par0,par1,par2,par3,par4,par5,par6,par7,par8,par9,par10,par11,par12,par13])

    f_fe="[0]*TMath::Exp(-0.5*pow(((x-[1])/[2]),2))"
    f_cu="[3]*TMath::Exp(-0.5*pow(((x-[4])/[5]),2))"
    f_ca="[6]*TMath::Exp(-0.5*pow(((x-[7])/[8]),2))"
    f_mg="[9]*TMath::Exp(-0.5*pow(((x-[10])/[11]),2))"
    total = ROOT.TF1("total","{}+{}+{}+{}+pol1(12)".format(f_fe,f_cu,f_ca,f_mg), _down, _up) 
    total.SetParameters(par)
    total.SetParLimits(0,0,par6)#constant
    total.SetParLimits(3,0,par6)
    total.SetParLimits(6,0,par6*2)
    total.SetParLimits(9,0,par6)
    total.SetParLimits(1,52,53)#mu
    total.SetParLimits(4,53,53.8)
    total.SetParLimits(7,54.3,55.2)
    total.SetParLimits(10,56,56.8)
    total.FixParameter(2,fline_sigma.Eval((52+53)/2.))#sigma
    total.FixParameter(5,fline_sigma.Eval((53+53.8)/2.))
    total.FixParameter(8,fline_sigma.Eval((54.3+55.2)/2.))
    total.FixParameter(11,fline_sigma.Eval((56+56.8)/2.))
    _h.Fit(total,"Q") #fit
    par0,par1,par2=total.GetParameter(0),total.GetParameter(1),abs(total.GetParameter(2))
    par3,par4,par5=total.GetParameter(3),total.GetParameter(4),abs(total.GetParameter(5))
    par6,par7,par8=total.GetParameter(6),total.GetParameter(7),abs(total.GetParameter(8))
    par9,par10,par11=total.GetParameter(9),total.GetParameter(10),abs(total.GetParameter(11))
    par12,par13=total.GetParameter(12),total.GetParameter(13)
    peak_fe.SetParameters(par0,par1,par2)
    peak_cu.SetParameters(par3,par4,par5)
    peak_ca.SetParameters(par6,par7,par8)
    peak_mg.SetParameters(par9,par10,par11)#Mg32 (target)
    bkg.SetParameters(par12,par13)

    # === integral and find error ===
    _binwidth, _count, _error=_h.GetBinWidth(1), 0, ctypes.c_double(0)
    _bindown, _binup=_h.GetXaxis().FindBin(par10-3*par11), _h.GetXaxis().FindBin(par10+3*par11)
    _count=peak_mg.Integral(par10-3*par11,par10+3*par11)/_binwidth
    _sum_temp=_h.IntegralAndError(_bindown,_binup,_error,"error")

    # === make plot ===
    c=ROOT.TCanvas("c{}_{}_{}".format(_n1,_n2,_n3),"c{}_{}_{}".format(_n1,_n2,_n3),1200,1000)
    pad1,pad2 = ROOT.TPad("pad1","pad1",0,0.3,1,1), ROOT.TPad("pad2","pad2",0,0.03,1,0.3)
    pad1.SetBottomMargin(0.0)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(0.4)
#    pad2.SetGridy()
    pad2.Draw();pad1.Draw();

    pad1.cd()
    _h.SetLineColor(1)
    #peak_fe.SetLineWidth(1)
    #peak_cu.SetLineWidth(1)
    #peak_ca.SetLineWidth(1)
    #peak_mg.SetLineWidth(1)
    #total.SetLineWidth(1)
    #bkg.SetLineWidth(1)
    peak_fe.SetLineColor(ROOT.kOrange)
    peak_cu.SetLineColor(ROOT.kPink+1)
    peak_ca.SetLineColor(ROOT.kViolet-9)
    peak_mg.SetLineColor(4)
    bkg.SetLineColor(3)
    total.SetLineColor(2)
    _h.SetMinimum((_h.GetMaximum()/25.)*(-0.9))
    _h.SetTitle(";Energy [keV];Counts/{} eV".format(int((200*1000)/n_bins_b)))
    _h.GetXaxis().CenterTitle(); _h.GetYaxis().CenterTitle();
    _h.Draw("ep")
    peak_fe.Draw("same")
    peak_cu.Draw("same")
    peak_ca.Draw("same")
    peak_mg.Draw("same")
    total.Draw("same")
    bkg.Draw("same")

    pad2.cd()
    h_residual=_h.Clone()
#    h_residual.Add(total,-1)
    for i in range(_h.GetXaxis().GetNbins()):
       _xbin,_xmean,_xerror=_h.GetBinContent(i+1), _h.GetBinCenter(i+1),_h.GetBinError(i+1)
       h_residual.SetBinContent(i+1, _xbin/total.Eval(_xmean))
       if _xbin == 0 or total.Eval(_xmean) == 0: h_residual.SetBinError(i+1,0)
       #else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin) + np.square(_error.value/total.Eval(_xmean))))
       else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin)))
    h_residual.SetTitle(";Energy [keV];Data/Model")
    h_residual.GetXaxis().SetTitleSize(35);
    h_residual.GetXaxis().SetTitleFont(43);
    h_residual.GetXaxis().SetTitleOffset(4.);
    h_residual.GetXaxis().SetLabelFont(43);
    h_residual.GetXaxis().SetLabelSize(30);
    h_residual.GetYaxis().SetNdivisions(505);
    h_residual.GetYaxis().SetTitleSize(25);
    h_residual.GetYaxis().SetTitleFont(43);
    h_residual.GetYaxis().SetTitleOffset(1.55);
    h_residual.GetYaxis().SetLabelFont(43);
    h_residual.GetYaxis().SetLabelSize(30);
    h_residual.SetMinimum(0.2)
    h_residual.SetMaximum(2.2)
    h_residual.Draw("p")
    line = ROOT.TLine(_down,1,_up,1)
    line.SetLineColorAlpha(ROOT.kRed, 0.3)
    line.SetLineWidth(2)
    line.Draw("same")

    c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_fit_peak_{2}_ch{1}_{0}.pdf".format(_n1,_n2,_n3))# take a long time

    # === print ===
    print("{0:5} CH{1:<3} {2:7} | Peak : {3:.2f} | Sigma : {4:.2f} | Intensity : {5:.1f} | Error : {6:.1f}".format(_n1,_n2,_n3,par10,par11,_count,_error.value))

    return [par10,total.GetParError(10)], [abs(par11),total.GetParError(11)], _count, _error #peak , sigma , count, error 


def fit_ca32(_h,_down,_up,_n1,_n2,_n3, fline_sigma):
    """
    unknown + Ca32&Fe-gamma + O31&Fe63 + unknown2
    _n1="sample" _n2="ch" _n3="peak"
    """
    n_bins_b=_h.GetXaxis().GetNbins()
    par0,par1,par2,par3,par4,par5,par6,par7,par8,par9,par10,par11,par12,par13 = map(ctypes.c_double, (0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    _h.GetXaxis().SetRangeUser(_down,_up)
    peak_un = ROOT.TF1("peak_un","gaus", _down, _up)
    peak_ca = ROOT.TF1("peak_ca","gaus", _down, _up)
    peak_o = ROOT.TF1("peak_o","gaus", _down, _up)
    bkg = ROOT.TF1("bkg","pol1", _down, _up)
    peak_un2 = ROOT.TF1("peak_un","gaus", _down, _up)

    # === get initial par. ===
    peak_un.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
    peak_ca.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
    peak_o.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
    peak_un2.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
#    peak_un.SetParLimits(0,0,10000)#constant
#    peak_ca.SetParLimits(0,0,10000)
#    peak_o.SetParLimits(0,0,10000)
    peak_un2.SetParLimits(0,0,10000)
    _h.Fit(peak_un,"Q","",155,156)
    _h.Fit(peak_ca,"Q","",156,157.5)
    _h.Fit(peak_o,"Q","",157.5,159)
    _h.Fit(peak_un2,"Q","",155.9,156.3)
    _h.Fit(bkg,"Q","",_down,_up)
    par0=peak_un.GetParameter(0)
    par1=peak_un.GetParameter(1)
    par2=peak_un.GetParameter(2)
    par3=peak_ca.GetParameter(0)
    par4=peak_ca.GetParameter(1)
    par5=peak_ca.GetParameter(2)
    par6=peak_o.GetParameter(0)
    par7=peak_o.GetParameter(1)
    par8=peak_o.GetParameter(2)
    par9=bkg.GetParameter(0)
    par10=bkg.GetParameter(1)
    par11=peak_un2.GetParameter(0)
    par12=peak_un2.GetParameter(1)
    par13=peak_un2.GetParameter(2)
    par=np.array([par0,par1,par2,par3,par4,par5,par6,par7,par8,par9,par10,par11,par12,par13])

    f_un="[0]*TMath::Exp(-0.5*pow(((x-[1])/[2]),2))"
    f_ca="[3]*TMath::Exp(-0.5*pow(((x-[4])/[5]),2))"
    f_o="[6]*TMath::Exp(-0.5*pow(((x-[7])/[8]),2))"
    f_un2="[11]*TMath::Exp(-0.5*pow(((x-[12])/[13]),2))"
    total = ROOT.TF1("total","{0}+{1}+{2}+pol1(9)+{3}".format(f_un,f_ca,f_o,f_un2), _down, _up) 
    total.SetParameters(par)
    total.SetParLimits(0,0,par6)#constant
    total.SetParLimits(3,0,par6)
    total.SetParLimits(6,0,par6*2)
    total.SetParLimits(11,0,par6*2)
    total.SetParLimits(1,155,155.8)#mu
    total.SetParLimits(4,156.2,157.2)
    total.SetParLimits(7,158,159)
    total.SetParLimits(12,155.9,156.3)
    total.FixParameter(2,fline_sigma.Eval((155+155.8)/2.))#sigma
    total.FixParameter(5,fline_sigma.Eval((156.2+157.2)/2.))
    total.FixParameter(8,fline_sigma.Eval((158+159)/2.))
    total.FixParameter(13,fline_sigma.Eval((155.9+156.3)/2.))
    _h.Fit(total,"Q") #fit
    par0,par1,par2=total.GetParameter(0),total.GetParameter(1),abs(total.GetParameter(2))
    par3,par4,par5=total.GetParameter(3),total.GetParameter(4),abs(total.GetParameter(5))
    par6,par7,par8=total.GetParameter(6),total.GetParameter(7),abs(total.GetParameter(8))
    par9,par10=total.GetParameter(9),total.GetParameter(10)
    par11,par12,par13=total.GetParameter(11),total.GetParameter(12),abs(total.GetParameter(13))
    peak_un.SetParameters(par0,par1,par2)
    peak_ca.SetParameters(par3,par4,par5)
    peak_o.SetParameters(par6,par7,par8)
    bkg.SetParameters(par9,par10)
    peak_un2.SetParameters(par11,par12,par13)

    # === integral and find error ===
    _binwidth, _count, _error=_h.GetBinWidth(1), 0, ctypes.c_double(0)
    _bindown, _binup=_h.GetXaxis().FindBin(par4-3*par5), _h.GetXaxis().FindBin(par4+3*par5)
    _count=peak_ca.Integral(par4-3*par5,par4+3*par5)/_binwidth
    _sum_temp=_h.IntegralAndError(_bindown,_binup,_error,"error")

    # === make plot ===
    c=ROOT.TCanvas("c{}_{}_{}".format(_n1,_n2,_n3),"c{}_{}_{}".format(_n1,_n2,_n3),1200,1000)
    pad1,pad2 = ROOT.TPad("pad1","pad1",0,0.3,1,1), ROOT.TPad("pad2","pad2",0,0.03,1,0.3)
    pad1.SetBottomMargin(0.0)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(0.4)
#    pad2.SetGridy()
    pad2.Draw();pad1.Draw();

    pad1.cd()
    _h.SetLineColor(1)
    #peak_un.SetLineWidth(1)
    #peak_ca.SetLineWidth(1)
    #peak_o.SetLineWidth(1)
    #total.SetLineWidth(1)
    #bkg.SetLineWidth(1)
    peak_un.SetLineColor(6)
    peak_ca.SetLineColor(4)
    peak_un2.SetLineColor(6)
    peak_o.SetLineColor(ROOT.kOrange)
    bkg.SetLineColor(3)
    total.SetLineColor(2)
    _h.SetMinimum((_h.GetMaximum()/25.)*(-0.9))
    _h.SetTitle(";Energy [keV];Counts/{} eV".format(int((200*1000)/n_bins_b)))
    _h.GetXaxis().CenterTitle(); _h.GetYaxis().CenterTitle();
    _h.Draw("ep")
    peak_un.Draw("same")
    peak_un2.Draw("same")
    peak_ca.Draw("same")
    peak_o.Draw("same")
    total.Draw("same")
    bkg.Draw("same")

    pad2.cd()
    h_residual=_h.Clone()
#    h_residual.Add(total,-1)
    for i in range(_h.GetXaxis().GetNbins()):
       _xbin,_xmean,_xerror=_h.GetBinContent(i+1), _h.GetBinCenter(i+1),_h.GetBinError(i+1)
       h_residual.SetBinContent(i+1, _xbin/total.Eval(_xmean))
       if _xbin == 0 or total.Eval(_xmean) == 0: h_residual.SetBinError(i+1,0)
       #else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin) + np.square(_error.value/total.Eval(_xmean))))
       else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin)))
    h_residual.SetTitle(";Energy [keV];Data/Model")
    h_residual.GetXaxis().SetTitleSize(35);
    h_residual.GetXaxis().SetTitleFont(43);
    h_residual.GetXaxis().SetTitleOffset(4.);
    h_residual.GetXaxis().SetLabelFont(43);
    h_residual.GetXaxis().SetLabelSize(30);
    h_residual.GetYaxis().SetNdivisions(505);
    h_residual.GetYaxis().SetTitleSize(25);
    h_residual.GetYaxis().SetTitleFont(43);
    h_residual.GetYaxis().SetTitleOffset(1.55);
    h_residual.GetYaxis().SetLabelFont(43);
    h_residual.GetYaxis().SetLabelSize(30);
    h_residual.SetMinimum(0.2)
    h_residual.SetMaximum(2.2)
    h_residual.Draw("p")
    line = ROOT.TLine(_down,1,_up,1)
    line.SetLineColorAlpha(ROOT.kRed, 0.3)
    line.SetLineWidth(2)
    line.Draw("same")

    c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_fit_peak_{2}_ch{1}_{0}.pdf".format(_n1,_n2,_n3))# take a long time

    # === print ===
    print("{0:5} CH{1:<3} {2:7} | Peak : {3:.2f} | Sigma : {4:.2f} | Intensity : {5:.1f} | Error : {6:.1f}".format(_n1,_n2,_n3,par4,par5,_count,_error.value))

    return [par4,total.GetParError(4)], [abs(par5),total.GetParError(5)], _count, _error #peak , sigma , count, error 


def fit_al42(_h,_down,_up,_n1,_n2,_n3, fline_sigma):
    """
    three step for Al42:
    1. 89 keV from Fe (expect it based on 93 keV from Fe)
    2. 89 keV from Al window (expect it based on 66 keV counts of blank data)
    3. fix 1. and 2. source to fit : unknown + tail + Al42 + unknown + unknown + Al42_sub + unknown
    _n1="sample" _n2="ch" _n3="peak"
    """
    # ****************************** (Step 1)
    rate_89keV_Fe=[45.9/(2119.2/2), 44.8/(2743.4/2), 77.9/(3245.8/2), 36.6/(1972.9/2), 49.4/(2209.2/2), 43.3/(2865.9/2)]# one gaus, based on Stardard Fe sample
    peak_89keV_Fe=[89.18,89.11,89.10,89.11,89.11,89.05]# fix center
    _hFe=_h.Clone()
    n_bins_b=_hFe.GetXaxis().GetNbins()

    my_fe43_peaks=[[92.772 , 93.087],[92.704 , 93.020],[92.704 , 93.010],[92.684 , 92.994],[92.689 , 93.042],[92.652 , 92.946]]
    par0_t,par1_t,par2_t,par3_t,par4_t,par5_t,par6_t,par7_t,par8_t,par9_t,par10_t = map(ctypes.c_double, (0,0,0,0,0,0,0,0,0,0,0))
    _hFe.GetXaxis().SetRangeUser(91,95)#find Fe43
    peak_fe1 = ROOT.TF1("peak_fe1","gaus", 91, 95)
    peak_fe2 = ROOT.TF1("peak_fe2","gaus", 91, 95)
    peak_un = ROOT.TF1("peak_un","gaus", 91, 95)
    bkg = ROOT.TF1("bkg","pol1", 91, 95)

    peak_fe1.FixParameter(2,fline_sigma.Eval((91+95)/2))#sigma
    peak_fe2.FixParameter(2,fline_sigma.Eval((91+95)/2))
    peak_un.FixParameter(2,fline_sigma.Eval((91+95)/2))
    peak_fe1.SetParLimits(0,0,1000)#constant
    peak_fe2.SetParLimits(0,0,1000)
    peak_un.SetParLimits(0,0,1000)
    _hFe.Fit(peak_fe1,"Q","",92.3,93.2)
    _hFe.Fit(peak_fe2,"Q","",92.3,93.2)
    _hFe.Fit(peak_un,"Q","",93.6,94.3)
    _hFe.Fit(bkg,"Q","",91,95)
    par0_t=peak_fe1.GetParameter(0)
    par1_t=peak_fe1.GetParameter(1)
    par2_t=peak_fe1.GetParameter(2)
    par3_t=peak_fe2.GetParameter(0)
    par4_t=peak_fe2.GetParameter(1)
    par5_t=peak_fe2.GetParameter(2)
    par6_t=peak_un.GetParameter(0)
    par7_t=peak_un.GetParameter(1)
    par8_t=peak_un.GetParameter(2)
    par9_t=bkg.GetParameter(0)
    par10_t=bkg.GetParameter(1)

    total = ROOT.TF1("total","[0]*TMath::Exp(-0.5*pow(((x-[1])/[2]),2))+[0]*TMath::Exp(-0.5*pow(((x-[3])/[4]),2))+gaus(5)+pol1(8)", _down, _up)
    total.SetParameters(par0_t,par1_t,par2_t,par4_t,par5_t,par6_t,par7_t,par8_t,par9_t,par10_t)
    total.SetParLimits(0,0,par0_t*2)#constant
    total.SetParLimits(5,0,par0_t*2)
    total.FixParameter(1,my_fe43_peaks[_n2-1][0])#mu
    total.FixParameter(3,my_fe43_peaks[_n2-1][1])
    total.SetParLimits(6,93.7,94.4)
    total.FixParameter(2,fline_sigma.Eval(my_fe43_peaks[_n2-1][0]))#sigma
    total.FixParameter(4,fline_sigma.Eval(my_fe43_peaks[_n2-1][1]))
    total.FixParameter(7,fline_sigma.Eval((93.7+94.2)/2.))
    _hFe.Fit(total,"Q") #fit with fixed sigma
    par0_t=total.GetParameter(0)
    par1_t=total.GetParameter(1)
    par2_t=total.GetParameter(2)
    par3_t=total.GetParameter(3)
    par4_t=total.GetParameter(4)
    par5_t=total.GetParameter(5)
    par6_t=total.GetParameter(6)
    par7_t=total.GetParameter(7)
    par8_t=total.GetParameter(8)
    par9_t=total.GetParameter(9)

    peak_fe1.SetParameters(par0_t,par1_t,par2_t)
    peak_fe2.SetParameters(par0_t,par3_t,par4_t)
    peak_un.SetParameters(par5_t,par6_t,par7_t)
    bkg.SetParameters(par8_t,par9_t)
 
    c=ROOT.TCanvas("c{}_{}_{}_addfe".format(_n1,_n2,_n3),"c{}_{}_{}_addfe".format(_n1,_n2,_n3),1200,1000)
    pad1,pad2 = ROOT.TPad("pad1","pad1",0,0.3,1,1), ROOT.TPad("pad2","pad2",0,0.03,1,0.3)
    pad1.SetBottomMargin(0.0)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(0.4)
    pad2.Draw();pad1.Draw();

    pad1.cd()
    _hFe.SetLineColor(1)
    peak_fe1.SetLineColor(4)
    peak_fe2.SetLineColor(4)
    peak_un.SetLineColor(ROOT.kOrange)
    bkg.SetLineColor(3)
    total.SetLineColor(2)
    _hFe.SetMinimum((_hFe.GetMaximum()/25.)*(-0.9))
    _hFe.SetTitle(";Energy [keV];Counts/{} eV".format(int((200*1000)/n_bins_b)))
    _hFe.GetXaxis().CenterTitle(); _hFe.GetYaxis().CenterTitle();
    _hFe.Draw("ep")
    peak_fe1.Draw("same")
    peak_fe2.Draw("same")
    peak_un.Draw("same")
    total.Draw("same")
    bkg.Draw("same")

    pad2.cd()
    h_residual=_hFe.Clone()
    for i in range(_hFe.GetXaxis().GetNbins()):
       _xbin,_xmean,_xerror=_hFe.GetBinContent(i+1), _hFe.GetBinCenter(i+1),_hFe.GetBinError(i+1)
       h_residual.SetBinContent(i+1, _xbin/total.Eval(_xmean))
       if _xbin == 0 or total.Eval(_xmean) == 0: h_residual.SetBinError(i+1,0)
       #else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin) + np.square(_error.value/total.Eval(_xmean))))
       else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin)))
    h_residual.SetTitle(";Energy [keV];Data/Model")
    h_residual.GetXaxis().SetTitleSize(35);
    h_residual.GetXaxis().SetTitleFont(43);
    h_residual.GetXaxis().SetTitleOffset(4.);
    h_residual.GetXaxis().SetLabelFont(43);
    h_residual.GetXaxis().SetLabelSize(30);
    h_residual.GetYaxis().SetNdivisions(505);
    h_residual.GetYaxis().SetTitleSize(25);
    h_residual.GetYaxis().SetTitleFont(43);
    h_residual.GetYaxis().SetTitleOffset(1.55);
    h_residual.GetYaxis().SetLabelFont(43);
    h_residual.GetYaxis().SetLabelSize(30);
    h_residual.SetMinimum(0.2)
    h_residual.SetMaximum(2.2)
    h_residual.Draw("p")
    line = ROOT.TLine(_down,1,_up,1)
    line.SetLineColorAlpha(ROOT.kRed, 0.3)
    line.SetLineWidth(2)
    line.Draw("same")
#    c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_fit_peak_{2}_ch{1}_{0}_addfe43.pdf".format(_n1,_n2,_n3))# take a long time

    constant_fe84=rate_89keV_Fe[_n2-1]*par0_t
    e_peak_fe84=peak_89keV_Fe[_n2-1]
    sigma_fe84=fline_sigma.Eval(e_peak_fe84)

    # ****************************** (Step 2)
    blank_file=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Blank_dsl20/203092_beam.root","read")
    my_secs=1801*60 
    if _n1 == "dewbar35":
       blank_file=ROOT.TFile("/Users/chiu.i-huan/Desktop/new_scientific/GeAnalysis/data/JPARC_2021Apri_Terada/Blank_35MeV/203094_beam.root","read")
       my_secs=1800*60
    time_dict= {"black":35702,"white":43368,"dew":17367,"dewbar":44846,"dewbar35":37233}
    _tree_b = blank_file.Get("tree")
    _tree_b.Draw("energy_ori >> h_blank(120,64,70)","detID == {}".format(_n2),"")
    h_blank=ROOT.gDirectory.Get("h_blank")
    al32_peaks=[6.61767e+01, 6.61053e+01, 6.61260e+01, 6.61142e+01, 6.61356e+01, 6.60866e+01]
    par0_blank,par1_blank,par2_blank,par3_blank,par4_blank=map(ctypes.c_double, (0,0,0,0,0))
    peak_al32 = ROOT.TF1("peak_al32","gaus", 64, 70)
    peak_bkg_blank = ROOT.TF1("peak_bkg_blank","pol1", 64, 70)
    peak_al32.FixParameter(2,fline_sigma.Eval(66))
    peak_al32.SetParLimits(0,0,10000)
    h_blank.Fit(peak_al32,"Q","",65.7,66.5)
    h_blank.Fit(peak_bkg_blank,"Q","",64,70)
    par0_blank=peak_al32.GetParameter(0)
    par1_blank=peak_al32.GetParameter(1)
    par2_blank=peak_al32.GetParameter(2)
    par3_blank=peak_bkg_blank.GetParameter(0)
    par4_blank=peak_bkg_blank.GetParameter(1)
    total = ROOT.TF1("total","gaus(0)+pol1(3)", 64, 70) 
    total.SetParameters(par0_blank,par1_blank,par2_blank,par3_blank,par4_blank)
    total.SetParLimits(0,0,par0_blank*2)#constant
    total.SetParLimits(1,67,67.8)#mu
    total.FixParameter(2,fline_sigma.Eval(66.))#sigma
    h_blank.Fit(total,"Q") #fit
    par0_blank,par1_blank,par2_blank=total.GetParameter(0),total.GetParameter(1),abs(total.GetParameter(2))
    par3_blank,par4_blank=total.GetParameter(3),total.GetParameter(4)
    peak_al32.SetParameters(par0_blank,par1_blank,par2_blank)
    peak_bkg_blank.SetParameters(par3_blank,par4_blank)
    _binwidth, _count_al32=h_blank.GetBinWidth(1), 0
    _count_al32=peak_al32.Integral(par1_blank-3*par2_blank,par1_blank+3*par2_blank)/_binwidth

    c=ROOT.TCanvas("c{}_{}_{}_blank".format(_n1,_n2,_n3),"c{}_{}_{}_blank".format(_n1,_n2,_n3),1200,1000)
    pad1,pad2 = ROOT.TPad("pad1","pad1",0,0.3,1,1), ROOT.TPad("pad2","pad2",0,0.03,1,0.3)
    pad1.SetBottomMargin(0.0)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(0.4)
    pad2.Draw();pad1.Draw();
    pad1.cd()
    h_blank.SetLineColor(1)
    peak_al32.SetLineColor(4)
    bkg.SetLineColor(3)
    total.SetLineColor(2)
    h_blank.SetMinimum((h_blank.GetMaximum()/25.)*(-0.9))
    h_blank.SetTitle(";Energy [keV];Counts")
    h_blank.GetXaxis().CenterTitle(); h_blank.GetYaxis().CenterTitle();
    h_blank.Draw("ep")
    peak_al32.Draw("same")
    total.Draw("same")
    bkg.Draw("same")
    pad2.cd()
    h_residual=h_blank.Clone()
    for i in range(h_blank.GetXaxis().GetNbins()):
       _xbin,_xmean,_xerror=h_blank.GetBinContent(i+1), h_blank.GetBinCenter(i+1),h_blank.GetBinError(i+1)
       h_residual.SetBinContent(i+1, _xbin/total.Eval(_xmean))
       if _xbin == 0 or total.Eval(_xmean) == 0: h_residual.SetBinError(i+1,0)
       else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin)))
    h_residual.SetTitle(";Energy [keV];Data/Model")
    h_residual.GetXaxis().SetTitleSize(35);
    h_residual.GetXaxis().SetTitleFont(43);
    h_residual.GetXaxis().SetTitleOffset(4.);
    h_residual.GetXaxis().SetLabelFont(43);
    h_residual.GetXaxis().SetLabelSize(30);
    h_residual.GetYaxis().SetNdivisions(505);
    h_residual.GetYaxis().SetTitleSize(25);
    h_residual.GetYaxis().SetTitleFont(43);
    h_residual.GetYaxis().SetTitleOffset(1.55);
    h_residual.GetYaxis().SetLabelFont(43);
    h_residual.GetYaxis().SetLabelSize(30);
    h_residual.SetMinimum(0.2)
    h_residual.SetMaximum(2.2)
    h_residual.Draw("p")
    line = ROOT.TLine(_down,1,_up,1)
    line.SetLineColorAlpha(ROOT.kRed, 0.3)
    line.SetLineWidth(2)
    line.Draw("same")
#    c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_fit_peak_{2}_ch{1}_{0}_blank.pdf".format(_n1,_n2,_n3))# take a long time

    _rate_89_66=[6.91156e+01/3.32200e+02,7.56629e+01/3.71410e+02,8.35320e+01/4.26155e+02,7.05506e+01/3.41635e+02,6.86964e+01/2.87192e+02,6.65576e+01/3.14040e+02]
    _count_al32_window=_count_al32*(time_dict[_n1]/my_secs)
    _count_al89_window=_rate_89_66[_n2-1]*_count_al32_window

    # ****************************** (Step 3)
    n_bins_b=_h.GetXaxis().GetNbins()
    al42_peaks=[[89.178 , 89.452],[89.114 , 89.344],[89.099 , 89.381],[89.107 , 89.367],[89.106 , 89.438],[89.053 , 89.349]]
    par0,par1,par2,par3,par4,par5,par6,par7,par8,par9,par10,par11,par12,par13,par14,par15,par16,par17,par18,par19 = map(ctypes.c_double, (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    _h.GetXaxis().SetRangeUser(_down,_up)
    peak_un1 = ROOT.TF1("peak_un1","gaus", _down, _up)
    peak_tail = ROOT.TF1("peak_tail","gaus", _down, _up)
    peak_al = ROOT.TF1("peak_al","gaus", _down, _up)
    peak_al_sub = ROOT.TF1("peak_al_sub","gaus", _down, _up)
    peak_un2 = ROOT.TF1("peak_un2","gaus", _down, _up)
    bkg = ROOT.TF1("bkg","pol1", _down, _up)
    peak_fe84 = ROOT.TF1("peak_fe84","gaus", _down, _up)

    # === get initial par. ===
    peak_un1.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
    peak_tail.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
    peak_al.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
    peak_al_sub.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
    peak_un2.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
    peak_fe84.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
    peak_un1.SetParLimits(0,0,10000)#constant
    peak_tail.SetParLimits(0,0,10000)
    peak_al.SetParLimits(0,0,10000)
    peak_al_sub.SetParLimits(0,0,10000)
    peak_un2.SetParLimits(0,0,10000)
    peak_fe84.SetParLimits(0,0,10000)
    _h.Fit(peak_un1,"Q","",86.5,87.8)
    _h.Fit(peak_tail,"Q","",87.8,88.6)
    _h.Fit(peak_al,"Q","",88.5,89.2)
    _h.Fit(peak_al_sub,"Q","",89.2,89.7)
    _h.Fit(peak_un2,"Q","",90.5,91.5)
    _h.Fit(peak_fe84,"Q","",89.4,89.7)
    _h.Fit(bkg,"Q","",_down,_up)
    par0=peak_un1.GetParameter(0)
    par1=peak_un1.GetParameter(1)
    par2=peak_un1.GetParameter(2)
    par11=peak_tail.GetParameter(0)
    par12=peak_tail.GetParameter(1)
    par13=peak_tail.GetParameter(2)
    par3=peak_al.GetParameter(0)
    par4=peak_al.GetParameter(1)
    par5=peak_al.GetParameter(2)
    par14=peak_al_sub.GetParameter(0)
    par15=peak_al_sub.GetParameter(1)
    par16=peak_al_sub.GetParameter(2)
    par6=peak_un2.GetParameter(0)
    par7=peak_un2.GetParameter(1)
    par8=peak_un2.GetParameter(2)
    par17=peak_fe84.GetParameter(0)
    par18=peak_fe84.GetParameter(1)
    par19=peak_fe84.GetParameter(2)
    par9=bkg.GetParameter(0)
    par10=bkg.GetParameter(1)
    par=np.array([par0,par1,par2,par3,par4,par5,par6,par7,par8,par9,par10,par11,par12,par13,par15,par16,par17,par18,par19])#par14 is no used (fix high)

    f_un1="[0]*TMath::Exp(-0.5*pow(((x-[1])/[2]),2))"
    f_al="[3]*TMath::Exp(-0.5*pow(((x-[4])/[5]),2))"
    f_al_sub="([3]/2.)*TMath::Exp(-0.5*pow(((x-[14])/[15]),2))"
    f_tail="[11]*TMath::Exp(-0.5*pow(((x-[12])/[13]),2))"
    f_un2="[6]*TMath::Exp(-0.5*pow(((x-[7])/[8]),2))"
    f_fe84="[16]*TMath::Exp(-0.5*pow(((x-[17])/[18]),2))"
    total = ROOT.TF1("total","{0}+{1}+{2}+{4}+pol1(9)+{3}+{5}".format(f_un1,f_al,f_un2,f_tail,f_al_sub,f_fe84), _down, _up) 
    total.SetParameters(par)
    total.SetParLimits(0,0,par3)#constant
    total.SetParLimits(3,0,par3*2)
    total.SetParLimits(6,0,par3)
    total.SetParLimits(11,0,par3)
    total.FixParameter(16,constant_fe84)
    total.SetParLimits(1,86.8,87.6)#mu
    total.FixParameter(4,al42_peaks[_n2-1][0])
    total.FixParameter(14,al42_peaks[_n2-1][1])
    total.SetParLimits(7,90.4,91.4)
    total.SetParLimits(12,88,88.5)
    total.FixParameter(17,e_peak_fe84)
    total.FixParameter(2,fline_sigma.Eval((86.8+87.6)/2.))#sigma
    total.FixParameter(5,fline_sigma.Eval(al42_peaks[_n2-1][0]))
    total.FixParameter(8,fline_sigma.Eval((90.4+91.4)/2.))
    total.FixParameter(13,fline_sigma.Eval((88+88.5)/2.))
    total.FixParameter(15,fline_sigma.Eval(al42_peaks[_n2-1][1]))
    total.FixParameter(18,sigma_fe84)
    _h.Fit(total,"Q") #fit
    par0,par1,par2=total.GetParameter(0),total.GetParameter(1),abs(total.GetParameter(2))
    par3,par4,par5=total.GetParameter(3),total.GetParameter(4),abs(total.GetParameter(5))
    par6,par7,par8=total.GetParameter(6),total.GetParameter(7),abs(total.GetParameter(8))
    par11,par12,par13=total.GetParameter(11),total.GetParameter(12),abs(total.GetParameter(13))
    par16,par17,par18=total.GetParameter(16),total.GetParameter(17),abs(total.GetParameter(18))
    par14,par15=total.GetParameter(14),abs(total.GetParameter(15))
    par9,par10=total.GetParameter(9),total.GetParameter(10)
    peak_un1.SetParameters(par0,par1,par2)
    peak_tail.SetParameters(par11,par12,par13)
    peak_al.SetParameters(par3,par4,par5)
    peak_al_sub.SetParameters((par3/3.),par14,par15)
    peak_un2.SetParameters(par6,par7,par8)
    peak_fe84.SetParameters(par16,par17,par18)
    bkg.SetParameters(par9,par10)

    # === integral and find error ===
    _binwidth, _count, _error=_h.GetBinWidth(1), 0, ctypes.c_double(0)
    _bindown, _binup=_h.GetXaxis().FindBin(par4-3*par5), _h.GetXaxis().FindBin(par4+3*par5)
    _count=peak_al.Integral(par4-3*par5,par4+3*par5)/_binwidth
    _count+=peak_al_sub.Integral(par14-3*par15,par14+3*par15)/_binwidth
    _sum_temp=_h.IntegralAndError(_bindown,_binup,_error,"error")

    # === al43 from window ===
    rate=_count_al89_window/(_count)
    peak_al_win = ROOT.TF1("peak_al_win","gaus", _down, _up)
    peak_al_sub_win = ROOT.TF1("peak_al_sub_win","gaus", _down, _up)
    peak_al_win.SetParameters(par3*rate,par4,par5)
    peak_al_sub_win.SetParameters((par3/3.)*rate,par14,par15)

    # === make plot ===
    c=ROOT.TCanvas("c{}_{}_{}".format(_n1,_n2,_n3),"c{}_{}_{}".format(_n1,_n2,_n3),1200,1000)
    pad1,pad2 = ROOT.TPad("pad1","pad1",0,0.3,1,1), ROOT.TPad("pad2","pad2",0,0.03,1,0.3)
    pad1.SetBottomMargin(0.0)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(0.4)
#    pad2.SetGridy()
    pad2.Draw();pad1.Draw();

    pad1.cd()
    _h.SetLineColor(1)
    #peak_un1.SetLineWidth(1)
    #peak_al.SetLineWidth(1)
    #peak_un2.SetLineWidth(1)
    #total.SetLineWidth(1)
    #bkg.SetLineWidth(1)
    peak_un1.SetLineColor(6)
    peak_al.SetLineColor(4)
    peak_al_sub.SetLineColor(4)
    peak_tail.SetLineColor(7)
    peak_un2.SetLineColor(ROOT.kOrange)
    peak_fe84.SetLineColor(ROOT.kViolet)
    peak_al_win.SetLineColor(ROOT.kGray)
    peak_al_sub_win.SetLineColor(ROOT.kGray)
    bkg.SetLineColor(3)
    total.SetLineColor(2)
    _h.SetMinimum((_h.GetMaximum()/25.)*(-0.9))
    _h.SetTitle(";Energy [keV];Counts/{} eV".format(int((200*1000)/n_bins_b)))
    _h.GetXaxis().CenterTitle(); _h.GetYaxis().CenterTitle();
    _h.Draw("ep")
    peak_un1.Draw("same")
    peak_tail.Draw("same")
    peak_al.Draw("same")
    peak_al_sub.Draw("same")
    peak_un2.Draw("same")
    peak_fe84.Draw("same")
    total.Draw("same")
    bkg.Draw("same")
    peak_al_win.Draw("same")
    peak_al_sub_win.Draw("same")
 
    pad2.cd()
    h_residual=_h.Clone()
#    h_residual.Add(total,-1)
    for i in range(_h.GetXaxis().GetNbins()):
       _xbin,_xmean,_xerror=_h.GetBinContent(i+1), _h.GetBinCenter(i+1),_h.GetBinError(i+1)
       h_residual.SetBinContent(i+1, _xbin/total.Eval(_xmean))
       if _xbin == 0 or total.Eval(_xmean) == 0: h_residual.SetBinError(i+1,0)
       #else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin) + np.square(_error.value/total.Eval(_xmean))))
       else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin)))
    h_residual.SetTitle(";Energy [keV];Data/Model")
    h_residual.GetXaxis().SetTitleSize(35);
    h_residual.GetXaxis().SetTitleFont(43);
    h_residual.GetXaxis().SetTitleOffset(4.);
    h_residual.GetXaxis().SetLabelFont(43);
    h_residual.GetXaxis().SetLabelSize(30);
    h_residual.GetYaxis().SetNdivisions(505);
    h_residual.GetYaxis().SetTitleSize(25);
    h_residual.GetYaxis().SetTitleFont(43);
    h_residual.GetYaxis().SetTitleOffset(1.55);
    h_residual.GetYaxis().SetLabelFont(43);
    h_residual.GetYaxis().SetLabelSize(30);
    h_residual.SetMinimum(0.2)
    h_residual.SetMaximum(2.2)
    h_residual.Draw("p")
    line = ROOT.TLine(_down,1,_up,1)
    line.SetLineColorAlpha(ROOT.kRed, 0.3)
    line.SetLineWidth(2)
    line.Draw("same")

    c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_fit_peak_{2}_ch{1}_{0}.pdf".format(_n1,_n2,_n3))# take a long time

    # === print ===
    print("{0:5} CH{1:<3} {2:7} | Peak : {3:.2f} | Sigma : {4:.2f} | Intensity : {5:.1f} | Error : {6:.1f}".format(_n1,_n2,_n3,par4,par5,_count,_error.value))

    return [par4,total.GetParError(4)], [abs(par5),total.GetParError(5)], _count, _error #peak , sigma , count, error 

def fit_al43(_h,_down,_up,_n1,_n2,_n3, fline_sigma):
    """
    Al43 + unknown + O32 + unknow
    _n1="sample" _n2="ch" _n3="peak"
    """
    n_bins_b=_h.GetXaxis().GetNbins()
    par0,par1,par2,par3,par4,par5,par6,par7,par8,par9,par10,par11,par12,par13,par14,par15,par16 = map(ctypes.c_double, (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
    _h.GetXaxis().SetRangeUser(_down,_up)
    peak_al43 = ROOT.TF1("peak_al43","gaus", _down, _up)
    peak_un1 = ROOT.TF1("peak_un1","gaus", _down, _up)
    peak_o32 = ROOT.TF1("peak_o32","gaus", _down, _up)
    peak_un2 = ROOT.TF1("peak_un2","gaus", _down, _up)
    peak_un3 = ROOT.TF1("peak_un3","gaus", _down, _up)
    bkg = ROOT.TF1("bkg","pol1", _down, _up)

    # === get initial par. ===
    peak_al43.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
    peak_un1.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
    peak_o32.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
    peak_un2.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
    peak_un3.FixParameter(2,fline_sigma.Eval((_down+_up)/2.))
    peak_al43.SetParLimits(0,0,1000)#constant
    peak_un1.SetParLimits(0,0,1000)
    peak_o32.SetParLimits(0,0,100000)
    peak_un2.SetParLimits(0,0,1000)
    peak_un3.SetParLimits(0,0,1000)
    _h.Fit(peak_al43,"Q","",22.5,23.5)
    _h.Fit(peak_un1,"Q","",22.4,22.8)
    _h.Fit(peak_o32,"Q","",24.5,25.2)
    _h.Fit(peak_un2,"Q","",23.6,24.5)
    _h.Fit(peak_un3,"Q","",25.2,25.5)
    _h.Fit(bkg,"Q","",_down,_up)
    par0=peak_al43.GetParameter(0)
    par1=peak_al43.GetParameter(1)
    par2=peak_al43.GetParameter(2)
    par3=peak_un1.GetParameter(0)
    par4=peak_un1.GetParameter(1)
    par5=peak_un1.GetParameter(2)
    par6=peak_o32.GetParameter(0)
    par7=peak_o32.GetParameter(1)
    par8=peak_o32.GetParameter(2)
    par9=peak_un2.GetParameter(0)
    par10=peak_un2.GetParameter(1)
    par11=peak_un2.GetParameter(2)
    par12=bkg.GetParameter(0)
    par13=bkg.GetParameter(1)
    par14=peak_un3.GetParameter(0)
    par15=peak_un3.GetParameter(1)
    par16=peak_un3.GetParameter(2)
    par=np.array([par0,par1,par2,par3,par4,par5,par6,par7,par8,par9,par10,par11,par12,par13,par14,par15,par16])

    f_al43="[0]*TMath::Exp(-0.5*pow(((x-[1])/[2]),2))"
    f_un1="[3]*TMath::Exp(-0.5*pow(((x-[4])/[5]),2))"
    f_o32="[6]*TMath::Exp(-0.5*pow(((x-[7])/[8]),2))"
    f_un2="[9]*TMath::Exp(-0.5*pow(((x-[10])/[11]),2))"
    f_un3="[14]*TMath::Exp(-0.5*pow(((x-[15])/[16]),2))"
    total = ROOT.TF1("total","{0}+{1}+{2}+{3}+pol1(12)+{4}".format(f_al43,f_un1,f_o32,f_un2,f_un3), _down, _up) 
    total.SetParameters(par)
    total.SetParLimits(0,0,par0*2)#constant
    total.SetParLimits(3,0,par3*2)
    total.SetParLimits(6,0,par6*2)
    total.SetParLimits(9,0,par9*2)
    total.SetParLimits(14,0,par14*2)
    total.SetParLimits(1,22.5,23.4)#mu
    total.SetParLimits(4,22.4,22.8)
    total.SetParLimits(7,24.5,25.2)
    total.SetParLimits(10,23.6,24.5)
    total.SetParLimits(15,25.2,25.5)
    total.FixParameter(2,fline_sigma.Eval(23.15))#sigma
    total.FixParameter(5,fline_sigma.Eval(23.15))
    total.FixParameter(8,fline_sigma.Eval(24))
    total.FixParameter(11,fline_sigma.Eval(24))
    total.FixParameter(16,fline_sigma.Eval(25))
    _h.Fit(total,"Q") #fit
    par0,par1,par2=total.GetParameter(0),total.GetParameter(1),abs(total.GetParameter(2))
    par3,par4,par5=total.GetParameter(3),total.GetParameter(4),abs(total.GetParameter(5))
    par6,par7,par8=total.GetParameter(6),total.GetParameter(7),abs(total.GetParameter(8))
    par9,par10,par11=total.GetParameter(9),total.GetParameter(10),abs(total.GetParameter(11))
    par12,par13=total.GetParameter(12),total.GetParameter(13)
    par14,par15,par16=total.GetParameter(14),total.GetParameter(15),abs(total.GetParameter(16))
    peak_al43.SetParameters(par0,par1,par2)
    peak_un1.SetParameters(par3,par4,par5)
    peak_o32.SetParameters(par6,par7,par8)
    peak_un2.SetParameters(par9,par10,par11)
    peak_un3.SetParameters(par14,par15,par16)
    bkg.SetParameters(par12,par13)

    # === integral and find error ===
    _binwidth, _count, _error=_h.GetBinWidth(1), 0, ctypes.c_double(0)
    _bindown, _binup=_h.GetXaxis().FindBin(par1-3*par2), _h.GetXaxis().FindBin(par1+3*par2)
    _count=peak_al43.Integral(par1-3*par2,par1+3*par2)/_binwidth
    _sum_temp=_h.IntegralAndError(_bindown,_binup,_error,"error")

    # === make plot ===
    c=ROOT.TCanvas("c{}_{}_{}".format(_n1,_n2,_n3),"c{}_{}_{}".format(_n1,_n2,_n3),1200,1000)
    pad1,pad2 = ROOT.TPad("pad1","pad1",0,0.3,1,1), ROOT.TPad("pad2","pad2",0,0.03,1,0.3)
    pad1.SetBottomMargin(0.0)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(0.4)
    pad2.Draw();pad1.Draw();

    pad1.cd()
    _h.SetLineColor(1)
    peak_al43.SetLineColor(4)
    peak_un1.SetLineColor(7)
    peak_un2.SetLineColor(8)
    peak_un3.SetLineColor(ROOT.kOrange)
    peak_o32.SetLineColor(ROOT.kOrange)
    bkg.SetLineColor(3)
    total.SetLineColor(2)
    _h.SetMinimum((_h.GetMaximum()/25.)*(-0.9))
    _h.SetTitle(";Energy [keV];Counts/{} eV".format(int((200*1000)/n_bins_b)))
    _h.GetXaxis().CenterTitle(); _h.GetYaxis().CenterTitle();
    _h.Draw("ep")
    peak_al43.Draw("same")
    peak_un1.Draw("same")
    peak_o32.Draw("same")
    peak_un2.Draw("same")
    peak_un3.Draw("same")
    total.Draw("same")
    bkg.Draw("same")
 
    pad2.cd()
    h_residual=_h.Clone()
#    h_residual.Add(total,-1)
    for i in range(_h.GetXaxis().GetNbins()):
       _xbin,_xmean,_xerror=_h.GetBinContent(i+1), _h.GetBinCenter(i+1),_h.GetBinError(i+1)
       h_residual.SetBinContent(i+1, _xbin/total.Eval(_xmean))
       if _xbin == 0 or total.Eval(_xmean) == 0: h_residual.SetBinError(i+1,0)
       #else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin) + np.square(_error.value/total.Eval(_xmean))))
       else: h_residual.SetBinError(i+1, _xbin/total.Eval(_xmean)*np.sqrt(np.square(_xerror/_xbin)))
    h_residual.SetTitle(";Energy [keV];Data/Model")
    h_residual.GetXaxis().SetTitleSize(35);
    h_residual.GetXaxis().SetTitleFont(43);
    h_residual.GetXaxis().SetTitleOffset(4.);
    h_residual.GetXaxis().SetLabelFont(43);
    h_residual.GetXaxis().SetLabelSize(30);
    h_residual.GetYaxis().SetNdivisions(505);
    h_residual.GetYaxis().SetTitleSize(25);
    h_residual.GetYaxis().SetTitleFont(43);
    h_residual.GetYaxis().SetTitleOffset(1.55);
    h_residual.GetYaxis().SetLabelFont(43);
    h_residual.GetYaxis().SetLabelSize(30);
    h_residual.SetMinimum(0.2)
    h_residual.SetMaximum(2.2)
    h_residual.Draw("p")
    line = ROOT.TLine(_down,1,_up,1)
    line.SetLineColorAlpha(ROOT.kRed, 0.3)
    line.SetLineWidth(2)
    line.Draw("same")

    c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_fit_peak_{2}_ch{1}_{0}.pdf".format(_n1,_n2,_n3))# take a long time

    # === print ===
    print("{0:5} CH{1:<3} {2:7} | Peak : {3:.2f} | Sigma : {4:.2f} | Intensity : {5:.1f} | Error : {6:.1f}".format(_n1,_n2,_n3,par1,par2,_count,_error.value))

    return [par1,total.GetParError(1)], [abs(par2),total.GetParError(2)], _count, _error #peak , sigma , count, error 
