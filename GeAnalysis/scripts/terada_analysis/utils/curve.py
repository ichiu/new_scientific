"""
 The module to make calibration curve.
"""
import os, ROOT, ctypes
import numpy as np

ROOT.gErrorIgnoreLevel = ROOT.kFatal
ROOT.gROOT.LoadMacro('../AtlasStyle/AtlasStyle.C')

class mkCurve():
      def __init__(self,
                  inputfile=None
                  ):
          self.inputfile=inputfile
          self.tree=self.inputfile.Get("tree")
          self.peak_list=["Fegamma","Si32", "Al43", "Al42", "Fe54", "Fe43", "Ca43", "Ca32", "Mg32", "Cu43", "O21", "O31", "O41"]
          self.n_peaks=len(self.peak_list)

          self._temp_peak,self._temp_peak_e,self._temp_sigma,self._temp_sigma_e=np.zeros((6,5,self.n_peaks)), np.zeros((6,5,self.n_peaks)),np.zeros((6,5,self.n_peaks)), np.zeros((6,5,self.n_peaks))
          self._temp_count,self._temp_error, self._temp_count_final, self._temp_error_final=np.zeros((6,5,self.n_peaks)), np.zeros((6,5,self.n_peaks)), np.zeros((6,5,self.n_peaks)), np.zeros((6,5,self.n_peaks))
          self._temp_eff, self._temp_eff_e, self._temp_eff_final=np.zeros((6,5,self.n_peaks)),np.zeros((6,5,self.n_peaks)),np.zeros((6,5,self.n_peaks))#6chs, 5 samples, 13 peaks
          self._getpeaks()

          self._check_peak_6ch("Counts_final", self._temp_count_final, self._temp_error_final)
          self._check_peak_6ch("Counts", self._temp_count, self._temp_error)
          self._check_peak_6ch("Peaks", self._temp_peak, self._temp_peak_e)
          self._check_peak_6ch("Sigma", self._temp_sigma, self._temp_sigma_e)
          self._check_peak_6ch("Eff", self._temp_eff, self._temp_eff_e)
          self._check_peak_6ch("Eff_final", self._temp_eff_final, self._temp_eff_e)

          self._mkgraph_6ch("ori",self._temp_eff)
          self._mkgraph_6ch("corr",self._temp_eff_final)

          # sum of 6 chs
          self.nplot_list,self.fline_list=[],[]
          self.sum_count, self.sqsum_error=self._sumCH()
          self._mkgraph()
          self._mkele_com()

      def _getpeaks(self):
          for _i in range(self.tree.GetEntries()):
             self.tree.GetEntry(_i)
             self._temp_count_final[self.tree.ch-1][self.tree.sample_ID][self.tree.peak_ID]=self.tree.count_final
             self._temp_error_final[self.tree.ch-1][self.tree.sample_ID][self.tree.peak_ID]=self.tree.error_final
             self._temp_count[self.tree.ch-1][self.tree.sample_ID][self.tree.peak_ID]=self.tree.count
             self._temp_error[self.tree.ch-1][self.tree.sample_ID][self.tree.peak_ID]=self.tree.error
             self._temp_peak[self.tree.ch-1][self.tree.sample_ID][self.tree.peak_ID]=self.tree.peak
             self._temp_peak_e[self.tree.ch-1][self.tree.sample_ID][self.tree.peak_ID]=self.tree.peak_e
             self._temp_sigma[self.tree.ch-1][self.tree.sample_ID][self.tree.peak_ID]=self.tree.sigma
             self._temp_sigma_e[self.tree.ch-1][self.tree.sample_ID][self.tree.peak_ID]=self.tree.sigma_e
             self._temp_eff[self.tree.ch-1][self.tree.sample_ID][self.tree.peak_ID]=self.tree.eff
             self._temp_eff_final[self.tree.ch-1][self.tree.sample_ID][self.tree.peak_ID]=self.tree.eff_final
             self._temp_eff_e[self.tree.ch-1][self.tree.sample_ID][self.tree.peak_ID]=self.tree.eff_e

      def _fit(self,X_array,Y_array):
          fit_points=[np.array([0,X_array[0],X_array[1]]),np.array([0,Y_array[0],Y_array[1]])]
          _temp_g=ROOT.TGraph(3,fit_points[0],fit_points[1])
          _fit_g=_temp_g.Clone()
          fline=ROOT.TF1("fline","pol1",-0.1,8)
          fline.FixParameter(0,0)#TODO fix zero point
          _fit_g.Fit("fline","qn")
          return [fline,_temp_g]# return TF1 and TGraph

      def _check_peak_6ch(self,_name,value,error):#peaks
          _temp_list=[ROOT.TH1F("h{}{}".format(_name,i),"h{}{}".format(_name,i),1,0,1) for i in range(5)]
          for i in range(5):_temp_list[i].SetMarkerColor(i+1)
          leg = ROOT.TLegend(.8,.85,.95,.95)
          leg.SetFillColor(0)
          leg.SetLineColor(0)
          leg.SetBorderSize(0)
          leg.AddEntry(_temp_list[0],"Black","p")
          leg.AddEntry(_temp_list[1],"White","p")
          leg.AddEntry(_temp_list[2],"DEW","p")
          leg.AddEntry(_temp_list[3],"DEWbar","p")
          leg.AddEntry(_temp_list[4],"DEWbar35","p")

          cv_list=[ROOT.TCanvas("cv{}{}".format(_name,i),"cv{}{}".format(_name,i), 1000,800) for i in range(self.n_peaks)]
          for _ipeaks in range(self.n_peaks):
             cv_list[_ipeaks].cd()
             _hist_list = [ROOT.TH1F("hist_{}".format(i),"hist_{}".format(i),7,0,7) for i in range(5)]
             _maxbin,_minbin=np.amax(value[:,:,_ipeaks]), np.amin(value[:,:,_ipeaks])
             for isample in range(5):#samples
                _hist_list[isample].SetLineColorAlpha(isample+1,0.5)
                _hist_list[isample].SetMarkerColorAlpha(isample+1,0.5)
                sum_bin,n_bin = [0,0], 0
                for ibin in range(6):#chs
                   _hist_list[isample].SetBinError(ibin+1,error[ibin,isample,_ipeaks])
                   _hist_list[isample].SetBinContent(ibin+1,value[ibin,isample,_ipeaks])
                   if value[ibin,isample,_ipeaks] != 0:
                      sum_bin[0]+= value[ibin,isample,_ipeaks]
                      sum_bin[1]+= np.square(error[ibin,isample,_ipeaks])
                      n_bin+=1
                if n_bin == 0: n_bin=1 # for Eff. Fe-gamma
                _hist_list[isample].SetBinContent(7,sum_bin[0]/n_bin)
                _hist_list[isample].SetBinError(7,np.sqrt(sum_bin[1])/n_bin)
                if isample == 0: 
                   _hist_list[isample].SetMaximum(_maxbin*1.2)
                   _hist_list[isample].SetMinimum(_minbin*0.8)
                   _hist_list[isample].SetTitle(";;{} {}".format(self.peak_list[_ipeaks],_name,))
                   for i in range(1,7): _hist_list[isample].GetXaxis().SetBinLabel(i, "ch{}".format(i))
                   _hist_list[isample].GetXaxis().SetBinLabel(7, "Sum")
                   _hist_list[isample].Draw("e1")
                   _hist_list[isample].Draw("hist same][")
                else: _hist_list[isample].Draw("e1 SAME"); _hist_list[isample].Draw("hist same][")
                # === draw line (TODO) ===
                #sum_line=ROOT.TLine(0,sum_bin[0]/n_bin,6,sum_bin[0]/n_bin)
                #sum_line.SetLineWidth(1)
                #sum_line.SetLineColor(isample+1)
                #sum_line.Draw("same")
             leg.Draw("same")
             cv_list[_ipeaks].SaveAs("/Users/chiu.i-huan/Desktop/temp_output/Peak_{}_{}_6ch.pdf".format(_name,self.peak_list[_ipeaks]))

      def _mkgraph_6ch(self,_outname,_array):       
          cv_list=["Al","Fe","Ca","Al_ref","Fe_ref","Ca_ref","Ca_ref2","Mg_ref","O","O2"]
          x_peakname=["Al42","Fe43","Ca32","Al42","Fe43","Ca32","Ca43","Mg32","O21","O21"]
          y_peakname=["Al43","Fe54","Ca43","Al_ref","Fe_ref","Ca_ref","Ca_ref","Mg_ref","O31","O41"]
          xy_limit=[[0.4,0.5],[1.4,0.8],[0.6,1.],[0.3,1.],[1.2,0.6],[0.9,0.6],[0.95,0.6],[0.5,0.5],[5.5,1.8],[5.5,1.6]]
          #terada value
          #ref_dict={"Al_ref":np.array([0.225,0.8,0.46,0.46,0.46]),"Fe_ref":np.array([0.43,0.07,0.23,0.23,0.23]),"Ca_ref":np.array([0.26,0.40,0.30,0.30,0.30]),"Mg_ref":np.array([0.28,0.13,0.26,0.26,0.26])}
          #from paper
          ref_dict={"Al_ref":np.array([0.22138347760312999,0.7598975203768764,0.44751891335024385,0.44751891335024385,0.44751891335024385]),"Fe_ref":np.array([0.8556488650061658,0.13513692742839553,0.45254817705772626,0.45254817705772626,0.45254817705772626]),"Ca_ref":np.array([0.3728378706921842,0.5970949816409656,0.4260001612484084,0.4260001612484084,0.4260001612484084]),"Mg_ref":np.array([0.24387843829585934,0.11357561582036725,0.2265341984728784,0.2265341984728784,0.2265341984728784])}
          color_list=[1,2,3,4,ROOT.kOrange,6]

          for _it in range(len(cv_list)):
             outname,peak_x,peak_y,x_max,y_max,plot_list=cv_list[_it],x_peakname[_it],y_peakname[_it],xy_limit[_it][0],xy_limit[_it][1],[]
             peak_index_1=self.peak_list.index(peak_x)
             if "_ref" in outname: 
                for i in range(6): plot_list.append(self._fit(_array[i,:,peak_index_1],ref_dict[peak_y])) 
             else: 
                peak_index_2=self.peak_list.index(peak_y)
                for i in range(6): plot_list.append(self._fit(_array[i,:,peak_index_1],_array[i,:,peak_index_2])) # mk calibration line for 6 ch
   
             c2=ROOT.TCanvas("cv_temp{}".format(outname),"cv_temp{}".format(outname),1000,800); c2.cd()
             leg = ROOT.TLegend(.18,.75,.32,.90); leg.SetFillColor(0); leg.SetLineColor(0); leg.SetBorderSize(0);
             for i in range(len(plot_list)):
                plot_list[i][0].SetLineWidth(1)
                plot_list[i][0].SetLineColorAlpha(color_list[i],0.9)
                plot_list[i][1].SetLineColorAlpha(color_list[i],0.65)
                plot_list[i][1].SetMarkerColorAlpha(color_list[i],0.65)
                plot_list[i][1].GetXaxis().SetLimits(0,x_max)
                plot_list[i][1].SetMinimum(0); plot_list[i][1].SetMaximum(y_max) 
                leg.AddEntry(plot_list[i][0], "Ch{}".format(i+1),"l")
                if i == 0:
                   if "_ref" in peak_y: plot_list[i][1].SetTitle(";{}/Si32;{}/Si".format(peak_x,peak_y))
                   else: plot_list[i][1].SetTitle(";{}/Si32;{}/Si32".format(peak_x,peak_y))
                   plot_list[i][1].Draw("ap") 
                else: plot_list[i][1].Draw("same p")
                plot_list[i][0].Draw("same")
             leg.Draw("same")
             c2.Print("/Users/chiu.i-huan/Desktop/temp_output/calibration_curve_6ch_{}_{}.pdf".format(outname,_outname))

      def _sumCH(self):
          #self._temp_count_final[0,:,:]=0.; self._temp_error[0,:,:]=0.# you can skip ch1 by this
          #self._temp_count_final[1,:,:]=0.; self._temp_error[1,:,:]=0.# you can skip ch by this
          #self._temp_count_final[2,:,:]=0.; self._temp_error[2,:,:]=0.# you can skip ch by this
          #self._temp_count_final[4,:,:]=0.; self._temp_error[4,:,:]=0.# you can skip ch by this
          #self._temp_count_final[5,:,:]=0.; self._temp_error[5,:,:]=0. # you can skip ch by this

          _sum = np.sum(self._temp_count_final, axis=0) # summing 6 ch
          dis_error=self._temp_count_final - np.average(self._temp_count_final,axis=0) # NOTE: Added discrepancy of each CH to the uncertainty 
          total_error=np.sqrt(np.square(self._temp_error_final)+np.square(dis_error))
          _error = np.sqrt(np.sum(np.square(total_error), axis=0))
          return _sum, _error
      
      def _mkgraph(self):
          Nsample=self.sum_count[:,1].size
          Si32_array,Si32_e=self.sum_count[:,1],self.sqsum_error[:,1]
          Al43_array,Al43_e=self.sum_count[:,2],self.sqsum_error[:,2]
          Al42_array,Al42_e=self.sum_count[:,3],self.sqsum_error[:,3]
          Fe54_array,Fe54_e=self.sum_count[:,4],self.sqsum_error[:,4]
          Fe43_array,Fe43_e=self.sum_count[:,5],self.sqsum_error[:,5]
          Ca43_array,Ca43_e=self.sum_count[:,6],self.sqsum_error[:,6]
          Ca32_array,Ca32_e=self.sum_count[:,7],self.sqsum_error[:,7]
          Mg32_array,Mg32_e=self.sum_count[:,8],self.sqsum_error[:,8]
          O21_array,O21_e=self.sum_count[:,10],self.sqsum_error[:,10]
          O31_array,O31_e=self.sum_count[:,11],self.sqsum_error[:,11]
          O41_array,O41_e=self.sum_count[:,12],self.sqsum_error[:,12]
          Al_ref=np.array([0.22138347760312999,0.7598975203768764,0.44751891335024385,0.44751891335024385,0.44751891335024385])
          Fe_ref=np.array([0.8556488650061658,0.13513692742839553,0.45254817705772626,0.45254817705772626,0.45254817705772626])
          Ca_ref=np.array([0.3728378706921842,0.5970949816409656,0.4260001612484084,0.4260001612484084,0.4260001612484084])
          Mg_ref=np.array([0.24387843829585934,0.11357561582036725,0.2265341984728784,0.2265341984728784,0.2265341984728784])

          x_limit,title_list,cali_name=[],[],[]

          error_x=(Al42_array/Si32_array)*np.sqrt((np.square(Al42_e/Al42_array)+np.square(Si32_e/Si32_array)))
          error_y=(Al43_array/Si32_array)*np.sqrt((np.square(Al43_e/Al43_array)+np.square(Si32_e/Si32_array)))
          self.nplot_list.append(ROOT.TGraphAsymmErrors(Nsample,Al42_array/Si32_array,Al43_array/Si32_array,error_x,error_x,error_y,error_y))
          self.fline_list.append(self._fit(Al42_array/Si32_array, Al43_array/Si32_array))
          title_list.append(";Al(4-2)/Si(3-2); Al(4-3)/Si(3-2)")
          cali_name.append("Al42Al43")#0 (number for fline_list)
          x_limit.append(0.2)

          error_x=(Fe43_array/Si32_array)*np.sqrt((np.square(Fe43_e/Fe43_array)+np.square(Si32_e/Si32_array)))
          error_y=(Fe54_array/Si32_array)*np.sqrt((np.square(Fe54_e/Fe54_array)+np.square(Si32_e/Si32_array)))
          self.nplot_list.append(ROOT.TGraphAsymmErrors(Nsample,Fe43_array/Si32_array,Fe54_array/Si32_array,error_x,error_x,error_y,error_y))
          self.fline_list.append(self._fit(Fe43_array/Si32_array, Fe54_array/Si32_array))
          title_list.append(";Fe(4-3)/Si(3-2); Fe(5-4)/Si(3-2)")
          cali_name.append("Fe43Fe54")#1 
          x_limit.append(1)

          error_x=(Ca32_array/Si32_array)*np.sqrt((np.square(Ca32_e/Ca32_array)+np.square(Si32_e/Si32_array)))
          error_y=(Ca43_array/Si32_array)*np.sqrt((np.square(Ca43_e/Ca43_array)+np.square(Si32_e/Si32_array)))
          self.nplot_list.append(ROOT.TGraphAsymmErrors(Nsample,Ca32_array/Si32_array,Ca43_array/Si32_array,error_x,error_x,error_y,error_y))
          self.fline_list.append(self._fit(Ca32_array/Si32_array, Ca43_array/Si32_array))
          title_list.append(";Ca(3-2)/Si(3-2); Ca(4-3)/Si(3-2)")
          cali_name.append("Ca32Ca43")#2
          x_limit.append(0.9)

          error_x=(Al42_array/Si32_array)*np.sqrt((np.square(Al42_e/Al42_array)+np.square(Si32_e/Si32_array)))
          error_y=np.array([0.,0.,0.,0.,0.])
          self.nplot_list.append(ROOT.TGraphAsymmErrors(Nsample,Al42_array/Si32_array,Al_ref,error_x,error_x,error_y,error_y))
          self.fline_list.append(self._fit(Al42_array/Si32_array,Al_ref))
          title_list.append(";Al(4-2)/Si(3-2); Al/Si ref.")
          cali_name.append("Al42Alref")#3
          x_limit.append(0.2)

          error_x=(Al43_array/Si32_array)*np.sqrt((np.square(Al43_e/Al43_array)+np.square(Si32_e/Si32_array)))
          error_y=np.array([0.,0.,0.,0.,0.])
          self.nplot_list.append(ROOT.TGraphAsymmErrors(Nsample,Al43_array/Si32_array,Al_ref,error_x,error_x,error_y,error_y))
          self.fline_list.append(self._fit(Al43_array/Si32_array,Al_ref))
          title_list.append(";Al(4-3)/Si(3-2); Al/Si ref.")
          cali_name.append("Al43Alref")#4
          x_limit.append(0.8)

          error_x=(Fe43_array/Si32_array)*np.sqrt((np.square(Fe43_e/Fe43_array)+np.square(Si32_e/Si32_array)))
          error_y=np.array([0.,0.,0.,0.,0.])
          self.nplot_list.append(ROOT.TGraphAsymmErrors(Nsample,Fe43_array/Si32_array,Fe_ref,error_x,error_x,error_y,error_y))
          self.fline_list.append(self._fit(Fe43_array/Si32_array,Fe_ref))
          title_list.append(";Fe(4-3)/Si(3-2); Fe/Si ref.")
          cali_name.append("Fe43Feref")#5
          x_limit.append(0.9)

          error_x=(Ca32_array/Si32_array)*np.sqrt((np.square(Ca32_e/Ca32_array)+np.square(Si32_e/Si32_array)))
          error_y=np.array([0.,0.,0.,0.,0.])
          self.nplot_list.append(ROOT.TGraphAsymmErrors(Nsample,Ca32_array/Si32_array,Ca_ref,error_x,error_x,error_y,error_y))
          self.fline_list.append(self._fit(Ca32_array/Si32_array,Ca_ref))
          title_list.append(";Ca(3-2)/Si(3-2); Ca/Si ref.")
          cali_name.append("Ca32Caref")#6
          x_limit.append(0.9)

          error_x=(Mg32_array/Si32_array)*np.sqrt((np.square(Mg32_e/Mg32_array)+np.square(Si32_e/Si32_array)))
          error_y=np.array([0.,0.,0.,0.,0.])
          self.nplot_list.append(ROOT.TGraphAsymmErrors(Nsample,Mg32_array/Si32_array,Mg_ref,error_x,error_x,error_y,error_y))
          self.fline_list.append(self._fit(Mg32_array/Si32_array,Mg_ref))
          title_list.append(";Mg(3-2)/Si(3-2); Mg/Si ref.")
          cali_name.append("Mg32Mgref")#7
          x_limit.append(0.4)

          error_x=(O21_array/Si32_array)*np.sqrt((np.square(O21_e/O21_array)+np.square(Si32_e/Si32_array)))
          error_y=(O31_array/Si32_array)*np.sqrt((np.square(O31_e/O31_array)+np.square(Si32_e/Si32_array)))
          self.nplot_list.append(ROOT.TGraphAsymmErrors(Nsample,O21_array/Si32_array,O31_array/Si32_array,error_x,error_x,error_y,error_y))
          self.fline_list.append(self._fit(O21_array/Si32_array, O31_array/Si32_array))
          title_list.append(";O(2-1)/Si(3-2); O(3-1)/Si(3-2)")
          cali_name.append("O21O31")#8
          x_limit.append(5.5)

          error_x=(O21_array/Si32_array)*np.sqrt((np.square(O21_e/O21_array)+np.square(Si32_e/Si32_array)))
          error_y=(O41_array/Si32_array)*np.sqrt((np.square(O41_e/O41_array)+np.square(Si32_e/Si32_array)))
          self.nplot_list.append(ROOT.TGraphAsymmErrors(Nsample,O21_array/Si32_array,O41_array/Si32_array,error_x,error_x,error_y,error_y))
          self.fline_list.append(self._fit(O21_array/Si32_array, O41_array/Si32_array))
          title_list.append(";O(2-1)/Si(3-2); O(4-1)/Si(3-2)")
          cali_name.append("O21O41")#9
          x_limit.append(5.5)

          error_x=(O31_array/O21_array)*np.sqrt((np.square(O31_e/O31_array)+np.square(O21_e/O21_array)))
          error_y=(O41_array/O21_array)*np.sqrt((np.square(O41_e/O41_array)+np.square(O21_e/O21_array)))
          self.nplot_list.append(ROOT.TGraphAsymmErrors(Nsample,O31_array/O21_array,O41_array/O21_array,error_x,error_x,error_y,error_y))
          self.fline_list.append(self._fit(O31_array/O21_array, O41_array/O21_array))
          title_list.append(";O(3-1)/O(2-1); O(4-1)/O(2-1)")
          cali_name.append("O31O41")#10
          x_limit.append(0.3)

          error_x=(Ca43_array/Si32_array)*np.sqrt((np.square(Ca43_e/Ca43_array)+np.square(Si32_e/Si32_array)))
          error_y=np.array([0.,0.,0.,0.,0.])
          self.nplot_list.append(ROOT.TGraphAsymmErrors(Nsample,Ca43_array/Si32_array,Ca_ref,error_x,error_x,error_y,error_y))
          self.fline_list.append(self._fit(Ca43_array/Si32_array,Ca_ref))
          title_list.append(";Ca(4-3)/Si(3-2); Ca/Si ref.")
          cali_name.append("Ca43Caref")#11
          x_limit.append(0.8)

          error_x=(Fe54_array/Si32_array)*np.sqrt((np.square(Fe54_e/Fe54_array)+np.square(Si32_e/Si32_array)))
          error_y=np.array([0.,0.,0.,0.,0.])
          self.nplot_list.append(ROOT.TGraphAsymmErrors(Nsample,Fe54_array/Si32_array,Fe_ref,error_x,error_x,error_y,error_y))
          self.fline_list.append(self._fit(Fe54_array/Si32_array,Fe_ref))
          title_list.append(";Fe(5-4)/Si(3-2); Fe/Si ref.")
          cali_name.append("Fe54Feref")#12
          x_limit.append(0.9)

          # === calibration curve ===
          ROOT.SetAtlasStyle()
          print_latex_para="\n"
          for i in range(len(self.nplot_list)):
             c = ROOT.TCanvas("c{}".format(i),"c{}".format(i),1000,800)
             self.nplot_list[i].GetXaxis().SetLimits(0,x_limit[i]) 
             self.nplot_list[i].SetMinimum(0) 
             self.nplot_list[i].SetMarkerSize(1)
             self.nplot_list[i].SetMarkerColor(4)
             #self.nplot_list[i].SetLineColor(4)
             self.nplot_list[i].SetTitle(title_list[i])
             self.nplot_list[i].GetXaxis().CenterTitle()
             self.nplot_list[i].GetYaxis().CenterTitle()
             self.fline_list[i][0].SetLineColor(2)
             self.fline_list[i][1].SetMarkerSize(1)
             self.fline_list[i][1].SetMarkerColor(ROOT.kSpring-1)
             #self.fline_list[i][1].SetLineColor(ROOT.kSpring-1)
             self.nplot_list[i].Draw("ap")
             self.fline_list[i][0].Draw("same")
             self.fline_list[i][1].Draw("same p")
             c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/calibration_curve_{0}_{1}.pdf".format(i,cali_name[i]))

             # === print latex ===
             title_list[i]=title_list[i].replace("; "," vs.")
             title_list[i]=title_list[i].replace(";","")
             print_latex_para+="{0}&{1:.2f}$\pm${2:.2f}&{3:.2f}$\pm${4:.2f}\\\ \n".format(title_list[i],self.fline_list[i][0].GetParameter(0), self.fline_list[i][0].GetParError(0), self.fline_list[i][0].GetParameter(1), self.fline_list[i][0].GetParError(1))
          print(print_latex_para)


          # === plot comparisons ===
          print_latex_fe, print_latex_al, print_latex_ca, print_latex_mg="\n","\n","\n","\n"
          point_name=["black","white","dew","dewbar","dewbar35"]
          print(" === Final dew ana. results === ")

          fline_Al=self.fline_list[3][0]#Al42 -> check numbers from line 185
          #TODO (use Fe43 for paper, Fe54 for check)
          fline_Fe=self.fline_list[5][0]#Fe43
          fline_Mg=self.fline_list[7][0]#Mg32
          fline_Ca=self.fline_list[6][0]#Ca32
          #fline_Fe=self.fline_list[12][0]#Fe54

          cv = ROOT.TCanvas("cv","cv",1000,800)
          error_y=(Al42_array/Si32_array)*np.sqrt((np.square(Al42_e/Al42_array)+np.square(Si32_e/Si32_array)))
          error_x=(Fe43_array/Si32_array)*np.sqrt((np.square(Fe43_e/Fe43_array)+np.square(Si32_e/Si32_array)))

          _n_point=len(Fe43_array)
          _graph = ROOT.TGraphAsymmErrors(_n_point,Fe43_array/Si32_array, Al42_array/Si32_array, error_x, error_x, error_y, error_y)
          leg = ROOT.TLegend(.60,.7,.9,.90)
          leg.SetFillColor(0)
          leg.SetLineColor(0)
          leg.SetBorderSize(0)
          _graph.SetTitle(";#muFe43/#muSi32;#muAl42/#muSi32")
          _graph.GetXaxis().SetLimits(0,1)
          _graph.GetYaxis().SetRangeUser(0,0.25)
          _graph.Draw("ap")
          x,y = map(ctypes.c_double, (0,0))
          for i in range(_n_point):
             _graph.GetPoint(i+1,x,y)
             _m = ROOT.TMarker(x,y, 20)
             _m.SetMarkerColor(i+1)
             _m.Paint()
          cv.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/cv_fe_vs_al.pdf")

          cv2 = ROOT.TCanvas("cv2","cv2",1000,800)          
          _graph2 = ROOT.TGraphAsymmErrors()
          _graph2.SetTitle(";Fe/Si;Al/Si")
          for ip in range(_n_point):
             #TODO
             _x,_y=Fe43_array[ip]/Si32_array[ip], Al42_array[ip]/Si32_array[ip]
             _xe,_ye=(Fe43_array[ip]/Si32_array[ip])*np.sqrt((np.square(Fe43_e[ip]/Fe43_array[ip])+np.square(Si32_e[ip]/Si32_array[ip]))),(Al42_array[ip]/Si32_array[ip])*np.sqrt((np.square(Al42_e[ip]/Al42_array[ip])+np.square(Si32_e[ip]/Si32_array[ip])))
             #_x,_y=Fe54_array[ip]/Si32_array[ip], Al42_array[ip]/Si32_array[ip]
             #_xe,_ye=(Fe54_array[ip]/Si32_array[ip])*np.sqrt((np.square(Fe54_e[ip]/Fe54_array[ip])+np.square(Si32_e[ip]/Si32_array[ip]))),(Al42_array[ip]/Si32_array[ip])*np.sqrt((np.square(Al42_e[ip]/Al42_array[ip])+np.square(Si32_e[ip]/Si32_array[ip])))
             _graph2.SetPoint(ip,fline_Fe.Eval(_x),fline_Al.Eval(_y))
             _graph2.SetPointError(ip,fline_Fe.Eval(_xe),fline_Fe.Eval(_xe),fline_Al.Eval(_ye),fline_Al.Eval(_ye))
             print_latex_fe+="{0:15}: Fe/Si : {1:.2f} $\pm$ {2:.4f} \n".format(point_name[ip],fline_Fe.Eval(_x),fline_Fe.Eval(_xe))
             print_latex_al+="{0:15}: Al/Si : {1:.2f} $\pm$ {2:.4f} \n".format(point_name[ip],fline_Al.Eval(_y),fline_Al.Eval(_ye))
#             print("{0:15}:".format(point_name[ip]),"Fe/Si : {:.2f} $\pm$ {:.2f}".format(fline_Fe.Eval(_x),fline_Fe.Eval(_xe))," Al/Si : {:.2f} $\pm$ {:.2f}".format(fline_Al.Eval(_y),fline_Al.Eval(_ye)) )
          _graph2.GetXaxis().SetLimits(0,0.45)
          _graph2.GetYaxis().SetRangeUser(0,1)
          _graph2.Draw("ap")
          cv2.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/cv_fe_vs_al_2.pdf")

          cvmg = ROOT.TCanvas("cvmg","cvmg",1000,800)
          error_y=(Mg32_array/Si32_array)*np.sqrt((np.square(Mg32_e/Mg32_array)+np.square(Si32_e/Si32_array)))
          error_x=(Fe43_array/Si32_array)*np.sqrt((np.square(Fe43_e/Fe43_array)+np.square(Si32_e/Si32_array)))

          _n_point=len(Fe43_array)
          _graph = ROOT.TGraphAsymmErrors(_n_point,Fe43_array/Si32_array, Mg32_array/Si32_array, error_x, error_x, error_y, error_y)
          leg = ROOT.TLegend(.60,.7,.9,.90)
          leg.SetFillColor(0)
          leg.SetLineColor(0)
          leg.SetBorderSize(0)
          _graph.SetTitle(";#muFe43/#muSi32;#muMg32/#muSi32")
          _graph.GetXaxis().SetLimits(0,0.6)
          _graph.GetYaxis().SetRangeUser(0,0.35)
          _graph.Draw("ap")
          x,y = map(ctypes.c_double, (0,0))
          for i in range(_n_point):
             _graph.GetPoint(i+1,x,y)
             _m = ROOT.TMarker(x,y, 20)
             _m.SetMarkerColor(i+1)
             _m.Paint()
          cvmg.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/cv_fe_vs_mg.pdf")

          cvmg2 = ROOT.TCanvas("cvmg2","cvmg2",1000,800)          
          _graph2 = ROOT.TGraphAsymmErrors()
          _graph2.SetTitle(";Fe/Si;Mg/Si")
          for ip in range(_n_point):
             _x,_y=Fe43_array[ip]/Si32_array[ip], Mg32_array[ip]/Si32_array[ip]
             _xe,_ye=(Fe43_array[ip]/Si32_array[ip])*np.sqrt((np.square(Fe43_e[ip]/Fe43_array[ip])+np.square(Si32_e[ip]/Si32_array[ip]))),(Mg32_array[ip]/Si32_array[ip])*np.sqrt((np.square(Mg32_e[ip]/Mg32_array[ip])+np.square(Si32_e[ip]/Si32_array[ip])))
             _graph2.SetPoint(ip,fline_Fe.Eval(_x),fline_Mg.Eval(_y))
             _graph2.SetPointError(ip,fline_Fe.Eval(_xe),fline_Fe.Eval(_xe),fline_Mg.Eval(_ye),fline_Mg.Eval(_ye))
             print_latex_mg+="{0:15}: Mg/Si : {1:.2f} $\pm$ {2:.4f} \n".format(point_name[ip],fline_Mg.Eval(_y),fline_Mg.Eval(_ye))
#             print("{0:15}:".format(point_name[ip]), "Fe/Si : {:.2f} $\pm$ {:.2f}".format(fline_Fe.Eval(_x),fline_Fe.Eval(_xe))," Mg/Si : {:.2f} $\pm$ {:.2f}".format(fline_Mg.Eval(_y),fline_Mg.Eval(_ye)) )
          _graph2.GetXaxis().SetLimits(0,0.45)
          _graph2.GetYaxis().SetRangeUser(0,0.35)
          _graph2.Draw("ap")
          cvmg2.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/cv_fe_vs_mg_2.pdf")

          cvmg3 = ROOT.TCanvas("cvmg3","cvmg3",1000,800)          
          _graph3 = ROOT.TGraphAsymmErrors()
          _graph3.SetTitle(";Fe/Si;Ca/Si")
          for ip in range(_n_point):
             _x,_y=Fe43_array[ip]/Si32_array[ip], Ca32_array[ip]/Si32_array[ip]
             _xe,_ye=(Fe43_array[ip]/Si32_array[ip])*np.sqrt((np.square(Fe43_e[ip]/Fe43_array[ip])+np.square(Si32_e[ip]/Si32_array[ip]))),(Ca32_array[ip]/Si32_array[ip])*np.sqrt((np.square(Ca32_e[ip]/Ca32_array[ip])+np.square(Si32_e[ip]/Si32_array[ip])))
             _graph3.SetPoint(ip,fline_Fe.Eval(_x),fline_Ca.Eval(_y))
             _graph3.SetPointError(ip,fline_Fe.Eval(_xe),fline_Fe.Eval(_xe),fline_Ca.Eval(_ye),fline_Ca.Eval(_ye))
             print_latex_ca+="{0:15}: Ca/Si : {1:.2f} $\pm$ {2:.4f} \n".format(point_name[ip],fline_Ca.Eval(_y),fline_Ca.Eval(_ye))
#             print("{0:15}:".format(point_name[ip]),"Fe/Si : {:.2f} $\pm$ {:.2f}".format(fline_Fe.Eval(_x),fline_Fe.Eval(_xe))," Ca/Si : {:.2f} $\pm$ {:.2f}".format(fline_Ca.Eval(_y),fline_Ca.Eval(_ye)) )
          _graph3.GetXaxis().SetLimits(0,0.45)
          _graph3.GetYaxis().SetRangeUser(0.23,0.45)
          _graph3.Draw("ap")
          cvmg3.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/cv_fe_vs_ca_2.pdf")
          print(print_latex_mg)
          print(print_latex_al)
          print(print_latex_fe)
          print(print_latex_ca)
          

      def _mkele_com(self):
          name_list=["Si32", "Al43", "Al42", "Fe54", "Fe43", "Ca43", "Ca32", "Mg32", "Cu43", "O21", "O31", "O41"]
          h0 = ROOT.TH1F("sum", "sum",len(name_list),0,len(name_list))  
          h1 = ROOT.TH1F("b", "b",len(name_list),0,len(name_list))  
          h2 = ROOT.TH1F("w", "w",len(name_list),0,len(name_list))  
          h3 = ROOT.TH1F("d", "d",len(name_list),0,len(name_list))  
          h4 = ROOT.TH1F("db", "db",len(name_list),0,len(name_list))  
          h5 = ROOT.TH1F("d35", "d35",len(name_list),0,len(name_list))
          h0.SetTitle(";;X/Si(3-2)")
          h0.GetYaxis().CenterTitle()
          h0.SetMaximum(5); h0.SetMinimum(0);
 
          for i in range(len(name_list)):
             h0.GetXaxis().SetBinLabel(i+1, name_list[i])
             h1.Fill(i,self.sum_count[0,i+1]/self.sum_count[0,1])
             h2.Fill(i,self.sum_count[1,i+1]/self.sum_count[1,1])
             h3.Fill(i,self.sum_count[2,i+1]/self.sum_count[2,1])
             h4.Fill(i,self.sum_count[3,i+1]/self.sum_count[3,1])
             h5.Fill(i,self.sum_count[4,i+1]/self.sum_count[4,1])
          h1.SetMarkerColor(4);h1.SetLineColor(4);
          h2.SetMarkerColor(2);h2.SetLineColor(2);
          h3.SetMarkerColor(3);h3.SetLineColor(3);
          h4.SetMarkerColor(8);h4.SetLineColor(8);
          h5.SetMarkerColor(ROOT.kOrange);h5.SetLineColor(ROOT.kOrange);
          c=ROOT.TCanvas("c","c",1200,800)
          h0.Draw("     hist lp")
          h1.Draw("same hist lp")
          h2.Draw("same hist lp")
          h3.Draw("same hist lp")
          h4.Draw("same hist lp")
          h5.Draw("same hist lp")
          leg = ROOT.TLegend(.25,.7,.5,.90)
          leg.SetFillColor(0)
          leg.SetLineColor(0)
          leg.SetBorderSize(0)
          leg.AddEntry(h1,"Black","p")
          leg.AddEntry(h2,"White","p")
          leg.AddEntry(h3,"DEW","p")
          leg.AddEntry(h4,"DEWbar","p")
          leg.AddEntry(h5,"DEWbar35","p")
          leg.Draw("same")
          c.SaveAs("/Users/chiu.i-huan/Desktop/temp_output/c_com_ele.pdf")

          # === print latex ===
          #name_list=["Si32", "Al43", "Al42", "Fe54", "Fe43", "Ca43", "Ca32", "Mg32"]# list for print
          name_list=["Si32", "Al43", "Al42", "Fe54", "Fe43", "Ca43", "Ca32", "Mg32", "Cu43", "O21", "O31", "O41"]# for proceeding
          print_latex_sum = "Sum : Si32, Al43, Al42, Fe54, Fe43, Ca43, Ca32, Mg32, Cu43, O21, O31, O41"
          print_latex = "Ratio : Si32, Al43, Al42, Fe54, Fe43, Ca43, Ca32, Mg32, Cu43, O21, O31, O41"
          print_latex += "\\\ \n"
          print_latex_sum += "\\\ \n"
          print_latex += "Black"
          print_latex_sum += "Black"
          print(" === Normalized intensities ===")
          print("black:")
          for i in range(len(name_list)): 
             _rate=self.sum_count[0,i+1]/self.sum_count[0,1]
             _error=_rate*np.sqrt(np.square(self.sqsum_error[0,i+1]/self.sum_count[0,i+1])+np.square(self.sqsum_error[0,1]/self.sum_count[0,1]))
             print_latex += "&{:.3f}$\pm${:.4f}".format(_rate,_error)
             print_latex_sum += "&{:.1f}$\pm${:.1f}".format(self.sum_count[0,i+1]/35702.,self.sqsum_error[0,i+1]/35702.)
             #print_latex_sum += "&{:.1f}".format(self.sum_count[0,i+1])
             print("{}, {:.2f}, {:.2f}".format(name_list[i], _rate,_error))
          print_latex += "\\\ \n"
          print_latex_sum += "\\\ \n"
          print_latex += "White"
          print_latex_sum += "White"
          print("white:")
          for i in range(len(name_list)): 
             _rate=self.sum_count[1,i+1]/self.sum_count[1,1]
             _error=_rate*np.sqrt(np.square(self.sqsum_error[1,i+1]/self.sum_count[1,i+1])+np.square(self.sqsum_error[1,1]/self.sum_count[1,1]))
             print_latex += "&{:.3f}$\pm${:.4f}".format(_rate,_error)
             print_latex_sum += "&{:.1f}$\pm${:.1f}".format(self.sum_count[1,i+1]/43368.,self.sqsum_error[1,i+1]/43368.)# for proceeding
             #print_latex_sum += "&{:.1f}".format(self.sum_count[1,i+1])
             #print("{}, {:.1f}, {:.1f}".format(name_list[i], self.sum_count[1,i+1]/43368.,self.sqsum_error[1,i+1]/43368.))
             print("{}, {:.2f}, {:.2f}".format(name_list[i], _rate,_error))
          print_latex += "\\\ \n"
          print_latex_sum += "\\\ \n"
          print_latex += "DEW"
          print_latex_sum += "DEW"
          print("dew:")
          for i in range(len(name_list)): 
             _rate=self.sum_count[2,i+1]/self.sum_count[2,1]
             _error=_rate*np.sqrt(np.square(self.sqsum_error[2,i+1]/self.sum_count[2,i+1])+np.square(self.sqsum_error[2,1]/self.sum_count[2,1]))
             print_latex += "&{:.3f}$\pm${:.4f}".format(_rate,_error)
             print_latex_sum += "&{:.1f}$\pm${:.1f}".format(self.sum_count[2,i+1]/17367.,self.sqsum_error[2,i+1]/17367.)
             #print_latex_sum += "&{:.1f}".format(self.sum_count[2,i+1])
             #print("{}, {:.1f}, {:.1f}".format(name_list[i], self.sum_count[2,i+1]/17367.,self.sqsum_error[2,i+1]/17367.))
             print("{}, {:.2f}, {:.2f}".format(name_list[i], _rate,_error))
          print_latex += "\\\ \n"
          print_latex_sum += "\\\ \n"
          print_latex += "DEW bar"
          print_latex_sum += "DEW bar"
          print("dewbar:")
          for i in range(len(name_list)): 
             _rate=self.sum_count[3,i+1]/self.sum_count[3,1]
             _error=_rate*np.sqrt(np.square(self.sqsum_error[3,i+1]/self.sum_count[3,i+1])+np.square(self.sqsum_error[3,1]/self.sum_count[3,1]))
             print_latex += "&{:.3f}$\pm${:.4f}".format(_rate,_error)
             print_latex_sum += "&{:.1f}$\pm${:.1f}".format(self.sum_count[3,i+1]/44846.,self.sqsum_error[3,i+1]/44846.)
             #print_latex_sum += "&{:.1f}".format(self.sum_count[3,i+1])
             #print("{}, {:.1f}, {:.1f}".format(name_list[i], self.sum_count[3,i+1]/44846.,self.sqsum_error[3,i+1]/44846.))
             print("{}, {:.2f}, {:.2f}".format(name_list[i], _rate,_error))
          print_latex += "\\\ \n"
          print_latex_sum += "\\\ \n"
          print_latex += "DEW bar 35"
          print_latex_sum += "DEW bar 35"
          print("dewbar35:")
          for i in range(len(name_list)): 
             _rate=self.sum_count[4,i+1]/self.sum_count[4,1]
             _error=_rate*np.sqrt(np.square(self.sqsum_error[4,i+1]/self.sum_count[4,i+1])+np.square(self.sqsum_error[4,1]/self.sum_count[4,1]))
             print_latex += "&{:.3f}$\pm${:.4f}".format(_rate,_error)
             print_latex_sum += "&{:.1f}$\pm${:.1f}".format(self.sum_count[4,i+1]/37233.,self.sqsum_error[4,i+1]/37233.)
             #print_latex_sum += "&{:.1f}".format(self.sum_count[4,i+1])
             #print("{}, {:.1f}, {:.1f}".format(name_list[i], self.sum_count[4,i+1]/37233.,self.sqsum_error[4,i+1]/37233.))
             print("{}, {:.2f}, {:.2f}".format(name_list[i], _rate,_error))
          print_latex += "\\\ \n"
          print_latex_sum += "\\\ \n"
          print(print_latex_sum)
          print(print_latex)
    
 
